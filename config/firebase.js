'use strict'

const Env = use('Env')

module.exports = {
  credential: {
    type: 'service_account',
    project_id: Env.get('FIREBASE_PROJECT_ID'),
    private_key_id: Env.get('FIREBASE_PRIVATE_KEY_ID'),
    private_key: Env.get('FIREBASE_PRIVATE_KEY'),
    client_email: Env.get('FIREBASE_CLIENT_EMAIL'),
    client_id: Env.get('FIREBASE_CLIENT_ID'),
    auth_uri: 'https://accounts.google.com/o/oauth2/auth',
    token_uri: 'https://accounts.google.com/o/oauth2/token',
    auth_provider_x509_cert_url: 'https://www.googleapis.com/oauth2/v1/certs',
    client_x509_cert_url: Env.get('FIREBASE_CLIENT_X509_CERT_URL')
  },
  databaseURL: Env.get('FIREBASE_DB_URL')
}

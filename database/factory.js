'use strict'

const Factory = use('Factory')
const moment = require('moment')

Factory.blueprint('App/Models/User', (faker, i, data) => {
  return {
    name: _checkValue(data.name, faker.name()),
    email: _checkValue(data.email, faker.email()),
    password: _checkValue(data.password, faker.word()),
    role: _checkValue(data.role, faker.pickone(['ADMIN', 'MANAGER', 'MONITOR'])),
    photo_url: _checkValue(data.photo_url, faker.avatar()),
    company_id: _checkValue(data.company_id, null),
    verification_code: _checkValue(data.verification_code, null)
  }
})

Factory.blueprint('App/Models/Company', (faker, i, data) => {
  return {
    name: _checkValue(data.name, faker.name()),
    trading_name: _checkValue(data.trading_name, faker.name()),
    cnpj: _checkValue(data.cnpj, faker.cnpj()),
    photo_url: _checkValue(data.photo_url, faker.avatar()),
    type: _checkValue(data.type, faker.pickone(['PF', 'PJ'])),
    email: _checkValue(data.email, faker.email()),
    cpf: _checkValue(data.cpf, faker.cpf()),
    rural_inscription: _checkValue(data.rural_inscription, faker.word()),
    zip_code: _checkValue(data.zip_code, faker.zip({plusfour: true})),
    city: _checkValue(data.city, faker.city()),
    state: _checkValue(data.state, faker.state()),
    district: _checkValue(data.district, faker.province()),
    street: _checkValue(data.street, faker.street()),
    number: _checkValue(data.number, faker.integer({min: 1, max: 99999})),
    complement: _checkValue(data.complement, faker.sentence()),
    primary_phone: _checkValue(data.primary_phone, faker.phone()),
    secondary_phone: _checkValue(data.secondary_phone, faker.phone()),
    observation: _checkValue(data.observation, faker.paragraph())
  }
})

Factory.blueprint('App/Models/Farm', (faker, i, data) => {
  return {
    name: _checkValue(data.name, faker.name()),
    hectares: _checkValue(data.hectares, faker.floating({min: 1, max: 100, fixed: 2})),
    company_id: _checkValue(data.company_id, 1)
  }
})

Factory.blueprint('App/Models/Provider', (faker, i, data) => {
  return {
    name: _checkValue(data.name, faker.name()),
    trading_name: _checkValue(data.trading_name, faker.name()),
    cnpj: _checkValue(data.cnpj, faker.cnpj()),
    photo_url: _checkValue(data.photo_url, faker.avatar()),
    type: _checkValue(data.type, faker.pickone(['PF', 'PJ'])),
    email: _checkValue(data.email, faker.email()),
    cpf: _checkValue(data.cpf, faker.cpf()),
    rural_inscription: _checkValue(data.rural_inscription, faker.word()),
    zip_code: _checkValue(data.zip_code, faker.zip({plusfour: true})),
    city: _checkValue(data.city, faker.city()),
    state: _checkValue(data.state, faker.state()),
    district: _checkValue(data.district, faker.province()),
    street: _checkValue(data.street, faker.street()),
    number: _checkValue(data.number, faker.integer({min: 1, max: 99999})),
    complement: _checkValue(data.complement, faker.sentence()),
    primary_phone: _checkValue(data.primary_phone, faker.phone()),
    secondary_phone: _checkValue(data.secondary_phone, faker.phone()),
    observation: _checkValue(data.observation, faker.paragraph()),
    employees_are_clt: _checkValue(data.employees_are_clt, faker.bool())
  }
})

Factory.blueprint('App/Models/Machine', (faker, i, data) => {
  return {
    machineCategory_id: _checkValue(data.machineCategory_id, null),
    machineType_id: _checkValue(data.machineType_id, null),
    provider_id: _checkValue(data.provider_id, null),
    brand:  _checkValue(data.brand, faker.word()),
    model: _checkValue(data.model, faker.word()),
    year: _checkValue(data.year, parseInt(faker.year())),
    description: _checkValue(data.description, faker.paragraph()),
    harvester_brand: _checkValue(data.harvester_brand, faker.pickone(['JOHN_DEERE', 'VALTRA', 'NEW_HOLLAND', 'CASE', 'MASSEY_FERGUSON'])),
    harvester_soy: _checkValue(data.harvester_soy, faker.bool()),
    harvester_soy_brand: _checkValue(data.harvester_soy_brand, faker.word()),
    harvester_soy_model: _checkValue(data.harvester_soy_model, faker.pickone(['DRAPER', 'CARACOL'])),
    harvester_soy_feet: _checkValue(data.harvester_soy_feet, faker.integer({min: 1, max: 80})),
    harvester_corn: _checkValue(data.harvester_corn, faker.bool()),
    harvester_corn_brand: _checkValue(data.harvester_corn_brand, faker.word()),
    harvester_corn_lines: _checkValue(data.harvester_corn_lines, faker.integer({min: 1, max: 1000})),
    harvester_corn_spacing: _checkValue(data.harvester_corn_spacing, faker.integer({min: 1, max: 1000})),
    harvester_corn_feet: _checkValue(data.harvester_corn_feet, faker.integer({min: 1, max: 80})),
    harvester_motor_type: _checkValue(data.harvester_motor_type, faker.pickone(['ROTOR', 'CILINDRO']))
  }
})

Factory.blueprint('App/Models/MachineCategory', (faker, i, data) => {
  return {
    name: _checkValue(data.name, faker.name()),
    fields: _checkValue(data.fields, faker.pickone(['HARVESTER', 'COMMON']))
  }
})

Factory.blueprint('App/Models/MachineType', (faker, i, data) => {
  return {
    name: _checkValue(data.name, faker.name()),
    machineCategory_id: _checkValue(data.machineCategory_id, null)
  }
})

Factory.blueprint('App/Models/Plot', (faker, i, data) => {
  return {
    name: _checkValue(data.name, faker.name()),
    coordinates: _checkValue(data.coordinates, [faker.coordinates(), faker.coordinates(), faker.coordinates()]),
    farm_id: _checkValue(data.farm_id, null),
    code: _checkValue(data.code, faker.word())
  }
})

Factory.blueprint('App/Models/MachineCategory', (faker, i, data) => {
  return {
    name: _checkValue(data.name, faker.name())
  }
})

Factory.blueprint('App/Models/Demand', (faker, i, data) => {
  let dates = _dateRange(faker)
  return {
    name: _checkValue(data.name, faker.name()),
    start_date: _checkValue(data.start_date, dates.date),
    end_date: _checkValue(data.end_date, dates.endDate),
    company_id: _checkValue(data.company_id, null),
    farm_id: _checkValue(data.farm_id, null),
    contracts: _checkValue(data.contracts, [faker.word(), faker.word(), faker.word()]),
    status: _checkValue(data.status, faker.pickone(['REQUESTED', 'IN_ANALYSIS', 'IN_PROGRESS', 'COMPLETED'])),
    monitor_id: _checkValue(data.monitor_id, null),
    hectares: _checkValue(data.hectares, faker.floating({min: 0, max: 1000, fixed: 2})),
  }
})

Factory.blueprint('App/Models/Report', (faker, i, data) => {
  let dates = _dateRange(faker)
  return {
    machine_id: _checkValue(data.machine_id, null),
    plot_id: _checkValue(data.plot_id, null),
    demand_id: _checkValue(data.demand_id, null),
    engine_hourmeter: _isDefined(data.engine_hourmeter, faker.floating({min: 0, max: 1000, fixed: 2})),
    rotor_hourmeter: _isDefined(data.rotor_hourmeter, faker.floating({min: 0, max: 1000, fixed: 2})),
    fuel: _isDefined(data.fuel, faker.floating({min: 0, max: 1000, fixed: 2})),
    chopper_losses_1: _isDefined(data.chopper_losses_1, faker.floating({min: 0, max: 1000, fixed: 2})),
    chopper_losses_2: _isDefined(data.chopper_losses_2, faker.floating({min: 0, max: 1000, fixed: 2})),
    chopper_losses_3: _isDefined(data.chopper_losses_3, faker.floating({min: 0, max: 1000, fixed: 2})),
    chopper_losses_4: _isDefined(data.chopper_losses_4, faker.floating({min: 0, max: 1000, fixed: 2})),
    platform_losses_1: _isDefined(data.platform_losses_1, faker.floating({min: 0, max: 1000, fixed: 2})),
    platform_losses_2: _isDefined(data.platform_losses_2, faker.floating({min: 0, max: 1000, fixed: 2})),
    platform_losses_3: _isDefined(data.platform_losses_3, faker.floating({min: 0, max: 1000, fixed: 2})),
    platform_losses_4: _isDefined(data.platform_losses_4, faker.floating({min: 0, max: 1000, fixed: 2})),
    hectares_harvested: _isDefined(data.hectares_harvested, faker.floating({min: 0, max: 1000, fixed: 2})),
    comments: _checkValue(data.comments, faker.paragraph()),
    date: _checkValue(data.date, dates.date)
  }
})

Factory.blueprint('App/Models/DemandMachine', (faker, i, data) => {
  return {
    demand_id: _checkValue(data.demand_id, null),
    machine_id: _checkValue(data.machine_id, null),
    machine_code: _checkValue(data.machine_code, faker.word())
  }
})

Factory.blueprint('App/Models/MachinePhoto', (faker, i, data) => {
  return {
    machine_id: _checkValue(data.machine_id, null),
    photo_url: _checkValue(data.photo_url, faker.avatar()),
    featured: _checkValue(data.featured, faker.bool())
  }
})

Factory.blueprint('App/Models/DemandObservation', (faker, i, data) => {
  return {
    demand_id: _checkValue(data.demand_id, null),
    date: _checkValue(data.date, moment(faker.date()).format('YYYY-MM-DD')),
    observation: _checkValue(data.observation, faker.paragraph())
  }
})

const _dateRange = (faker) => {
  let startDate = moment(faker.date())
  let endDate = startDate.add(faker.d12(), 'months')

  return {
    date: startDate.format('YYYY-MM-DD'),
    endDate: endDate.format('YYYY-MM-DD')
  }
}

const _checkValue = (data, fake) => {
  return (data === null || data === undefined || data === NaN || data === "" ) ? fake : data
}

const _isDefined = (data, fake) => {
  return (data === undefined) ? fake : data
}

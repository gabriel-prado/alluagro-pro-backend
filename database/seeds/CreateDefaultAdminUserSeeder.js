'use strict'

const Factory = use('Factory')

class CreateDefaultAdminUserSeeder {
  async run () {
    await Factory.model('App/Models/User').create({
      name: 'Alluagro',
      email: 'admin@alluagro.com.br',
      password: 'senha123',
      role: 'ADMIN'
    })
  }
}

module.exports = CreateDefaultAdminUserSeeder

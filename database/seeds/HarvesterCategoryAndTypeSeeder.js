'use strict'

const Factory = use('Factory')

class HarvesterCategoryAndTypeSeeder {
  async run () {
    let category = await Factory.model('App/Models/MachineCategory').create({
      name: 'Colheitadeiras',
      fields: 'HARVESTER'
    })

    await Factory.model('App/Models/MachineType').create({
      name: 'Colheitadeiras',
      machineCategory_id: category.id
    })
  }
}

module.exports = HarvesterCategoryAndTypeSeeder

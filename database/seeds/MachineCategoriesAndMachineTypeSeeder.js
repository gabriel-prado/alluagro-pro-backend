'use strict'

const Factory = use('Factory')

class MachineCategoriesAndMachineTypeSeeder {
  async run () {
    let category

    category = await this.createCategory('Agricultura de precisão')
    await this.createTypes(category.id, ['Análise de solo'])

    category = await this.createCategory('Fretes')
    await this.createTypes(category.id, ['Colheitadeiras', 'Graneleiros', 'Implementos agrícolas',
      'Pulverizadores', 'Terraplanagem', 'Tratores'])

    category = await this.createCategory('Implementos Agrícolas')
    await this.createTypes(category.id, ['Canavieiras', 'Colheita', 'Fenação', 'Irrigação', 'Plantio',
      'Preparo do Solo', 'Preparo e irrigação', 'Pós-plantio'])

    category = await this.createCategory('Pulverizadores')
    await this.createTypes(category.id, ['Acoplados', 'Aviões', 'Helicópteros', 'Tracionados'])

    category = await this.createCategory('Terraplanagem')
    await this.createTypes(category.id, ['Compactação', 'Escavação', 'Movimentação'])

    category = await this.createCategory('Tratores')
    await this.createTypes(category.id, ['75 - 200 CV', 'Traçados'])
  }

  async createCategory(name) {
    return await Factory.model('App/Models/MachineCategory').create({
      name: name,
      fields: 'COMMON'
    })
  }

  async createTypes(categoryId, types) {
    for (let i = 0; i < types.length; i++) {
      await Factory.model('App/Models/MachineType').create({
        name: types[i],
        machineCategory_id: categoryId
      })
    }
  }
}

module.exports = MachineCategoriesAndMachineTypeSeeder

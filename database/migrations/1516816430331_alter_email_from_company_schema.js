'use strict'

const Schema = use('Schema')

class AlterEmailFromCompanySchema extends Schema {
  up () {
    this.alter('companies', (table) => {
      table.string('email').alter()
    })
  }

  down () {
    this.alter('companies', (table) => {
      table.string('email').notNullable().alter()
    })
  }
}

module.exports = AlterEmailFromCompanySchema

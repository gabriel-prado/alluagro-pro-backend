'use strict'

const Schema = use('Schema')

class ProviderSchema extends Schema {
  up () {
    this.create('providers', (table) => {
      table.increments()
      table.string('name').notNullable()
      table.string('trading_name').notNullable()
      table.string('cnpj').notNullable()
      table.string('photo_url').notNullable()
      table.string('phone').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('providers')
  }
}

module.exports = ProviderSchema

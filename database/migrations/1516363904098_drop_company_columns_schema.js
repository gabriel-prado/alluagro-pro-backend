'use strict'

const Schema = use('Schema')

class DropCompanyColumnsSchema extends Schema {
  up () {
    this.table('companies', (table) => {
      table.dropColumns([
        'phone',
        'trading_name',
        'cnpj',
        'photo_url'
      ])
    })
  }

  down () {
    this.table('companies', (table) => {
      table.string('trading_name').notNullable()
      table.string('cnpj').notNullable()
      table.string('photo_url').notNullable()
      table.string('phone').notNullable()
    })
  }
}

module.exports = DropCompanyColumnsSchema

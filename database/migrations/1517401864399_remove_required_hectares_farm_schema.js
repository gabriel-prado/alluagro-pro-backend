'use strict'

const Schema = use('Schema')

class RemoveRequiredHectaresFarmSchema extends Schema {
  up () {
    this.table('farms', (table) => {
      table.float('hectares').alter()
    })
  }

  down () {
    this.table('farms', (table) => {
      table.float('hectares').notNullable().alter()
    })
  }
}

module.exports = RemoveRequiredHectaresFarmSchema

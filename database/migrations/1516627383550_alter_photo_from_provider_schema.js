'use strict'

const Schema = use('Schema')

class AlterPhotoFromProviderSchema extends Schema {
  up () {
    this.table('providers', (table) => {
      table.string('photo_url').defaultTo('assets/img/company-icon.png').alter()
    })
  }

  down () {
    this.table('providers', (table) => {
      table.string('photo_url').alter()
    })
  }
}

module.exports = AlterPhotoFromProviderSchema

'use strict'

const Schema = use('Schema')

class DemandObservationSchema extends Schema {
  up () {
    this.create('demand_observations', (table) => {
      table.increments()
      table.integer('demand_id').notNullable().unsigned()
      table.foreign('demand_id').references('demands.id')
      table.date('date').notNullable()
      table.text('observation').notNullable()
      table.timestamps()
    })
  }
  down () {
    this.drop('demand_observations')
  }
}

module.exports = DemandObservationSchema

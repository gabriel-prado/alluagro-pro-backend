'use strict'

const Schema = use('Schema')

class DemandSchema extends Schema {
  up () {
    this.create('demands', (table) => {
      table.increments()
      table.string('name').notNullable()
      table.date('start_date').notNullable()
      table.date('end_date').notNullable()
      table.integer('company_id').notNullable().unsigned()
      table.foreign('company_id').references('companies.id')
      table.integer('farm_id').notNullable().unsigned()
      table.foreign('farm_id').references('farms.id')
      table.text('contract_url')
      table.enum('status', ['REQUESTED', 'IN_ANALYSIS', 'IN_PROGRESS', 'COMPLETED']).notNullable()
      table.timestamps()
    })
  }
  down () {
    this.drop('demands')
  }
}

module.exports = DemandSchema

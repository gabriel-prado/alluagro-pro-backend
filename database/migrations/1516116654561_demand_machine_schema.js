'use strict'

const Schema = use('Schema')

class DemandMachineSchema extends Schema {
  up () {
    this.create('demand_machine', (table) => {
      table.integer('demand_id').notNullable().unsigned()
      table.foreign('demand_id').references('demands.id')
      table.integer('machine_id').notNullable().unsigned()
      table.foreign('machine_id').references('machines.id')
      table.timestamps()
    })
  }

  down () {
    this.drop('demand_machine')
  }
}

module.exports = DemandMachineSchema

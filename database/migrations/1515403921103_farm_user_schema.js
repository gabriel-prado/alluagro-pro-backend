'use strict'

const Schema = use('Schema')

class FarmUserSchema extends Schema {
  up () {
    this.create('farm_user', (table) => {
      table.integer('user_id').notNullable().unsigned()
      table.foreign('user_id').references('users.id')
      table.integer('farm_id').notNullable().unsigned()
      table.foreign('farm_id').references('farms.id')
      table.timestamps()
    })
  }

  down () {
    this.drop('farm_user')
  }
}

module.exports = FarmUserSchema

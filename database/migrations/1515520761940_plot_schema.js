'use strict'

const Schema = use('Schema')

class PlotSchema extends Schema {
  up () {
    this.create('plots', (table) => {
      table.increments()
      table.string('name').notNullable()
      table.text('coordinates').notNullable()
      table.integer('farm_id').notNullable().unsigned()
      table.foreign('farm_id').references('farms.id')
      table.timestamps()
    })
  }

  down () {
    this.drop('plots')
  }
}

module.exports = PlotSchema

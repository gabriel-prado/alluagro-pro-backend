'use strict'

const Schema = use('Schema')

class RemoveNotNullFromEmailInProviderSchema extends Schema {
  up () {
    this.table('providers', (table) => {
      table.string('email').alter()
    })
  }

  down () {
    this.table('providers', (table) => {
      table.string('email').notNullable().alter()
    })
  }
}

module.exports = RemoveNotNullFromEmailInProviderSchema

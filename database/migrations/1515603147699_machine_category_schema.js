'use strict'

const Schema = use('Schema')

class MachineCategorySchema extends Schema {
  up () {
    this.create('machine_categories', (table) => {
      table.increments()
      table.string('name').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('machine_categories')
  }
}

module.exports = MachineCategorySchema

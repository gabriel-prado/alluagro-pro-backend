'use strict'

const Schema = use('Schema')

class MachineSchema extends Schema {
  up () {
    this.create('machines', (table) => {
      table.increments()
      table.integer('machineCategory_id').notNullable().unsigned()
      table.foreign('machineCategory_id').references('machine_categories.id')
      table.integer('machineType_id').notNullable().unsigned()
      table.foreign('machineType_id').references('machine_types.id')
      table.integer('provider_id').notNullable().unsigned()
      table.foreign('provider_id').references('providers.id')
      table.string('brand').notNullable()
      table.string('model').notNullable()
      table.integer('year').notNullable().unsigned()
      table.timestamps()
    })
  }

  down () {
    this.drop('machines')
  }
}

module.exports = MachineSchema

'use strict'

const Schema = use('Schema')

class AlterProviderSchema extends Schema {
  up () {
    this.table('providers', (table) => {
      table.string('trading_name')
      table.string('cnpj')
      table.string('photo_url')
      table.enum('type', ['PF', 'PJ']).notNullable()
      table.string('email').notNullable()
      table.string('cpf')
      table.string('rural_inscription')
      table.string('zip_code').notNullable()
      table.string('city').notNullable()
      table.string('state').notNullable()
      table.string('district').notNullable()
      table.string('street').notNullable()
      table.integer('number').notNullable()
      table.string('complement')
      table.string('primary_phone').notNullable()
      table.string('secondary_phone')
      table.text('observation'),
      table.boolean('employees_are_clt').notNullable().defaultTo(false)
    })
  }

  down () {
    this.table('providers', (table) => {
      table.dropColumn('trading_name')
      table.dropColumn('cnpj')
      table.dropColumn('photo_url')
      table.dropColumn('type')
      table.dropColumn('email')
      table.dropColumn('cpf')
      table.dropColumn('rural_inscription')
      table.dropColumn('zip_code')
      table.dropColumn('city')
      table.dropColumn('state')
      table.dropColumn('district')
      table.dropColumn('street')
      table.dropColumn('number')
      table.dropColumn('complement')
      table.dropColumn('primary_phone')
      table.dropColumn('secondary_phone')
      table.dropColumn('observation')
      table.dropColumn('employees_are_clt')
    })
  }
}

module.exports = AlterProviderSchema

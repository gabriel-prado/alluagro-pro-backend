'use strict'

const Schema = use('Schema')

class AddCascadeOnDelete extends Schema {
  up () {
    this.table('demand_observations', (table) => {
      table.dropForeign('demand_id')
      table.foreign('demand_id').references('demands.id').onDelete('CASCADE')
    })
    this.table('plots', (table) => {
      table.dropForeign('farm_id')
      table.foreign('farm_id').references('farms.id').onDelete('CASCADE')
    })
    this.table('machine_photos', (table) => {
      table.dropForeign('machine_id')
      table.foreign('machine_id').references('machines.id').onDelete('CASCADE')
    })
    this.table('demand_machine', (table) => {
      table.dropForeign('demand_id')
      table.foreign('demand_id').references('demands.id').onDelete('CASCADE')
    })
  }

  down () {
    this.table('demand_observations', (table) => {
      table.dropForeign('demand_id')
      table.foreign('demand_id').references('demands.id')
    })
    this.table('plots', (table) => {
      table.dropForeign('farm_id')
      table.foreign('farm_id').references('farms.id')
    })
    this.table('machine_photos', (table) => {
      table.dropForeign('machine_id')
      table.foreign('machine_id').references('machines.id')
    })
    this.table('demand_machine', (table) => {
      table.dropForeign('demand_id')
      table.foreign('demand_id').references('demands.id')
    })
  }
}

module.exports = AddCascadeOnDelete

'use strict'

const Schema = use('Schema')

class UserCompanyRelation extends Schema {
  up () {
    this.table('users', (table) => {
      table.integer('company_id').unsigned()
      table.foreign('company_id').references('companies.id')
    })
  }

  down () {
    this.table('users', (table) => {
      table.dropForeign('company_id')
      table.dropColumn('company_id')
    })
  }
}

module.exports = UserCompanyRelation

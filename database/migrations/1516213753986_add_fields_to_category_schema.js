'use strict'

const Schema = use('Schema')

class AddFieldsToCategorySchema extends Schema {
  up () {
    this.table('machine_categories', (table) => {
      table.enum('fields', ['HARVESTER', 'COMMON']).notNullable().defaultTo('COMMON')
    })
  }

  down () {
    this.table('machine_categories', (table) => {
      table.dropColumn('fields')
    })
  }
}

module.exports = AddFieldsToCategorySchema

'use strict'

const Schema = use('Schema')

class DropProviderColumnsSchema extends Schema {
  up () {
    this.table('providers', (table) => {
      table.dropColumns([
        'phone',
        'trading_name',
        'cnpj',
        'photo_url'
      ])
    })
  }

  down () {
    this.table('providers', (table) => {
      table.string('trading_name').notNullable()
      table.string('cnpj').notNullable()
      table.string('photo_url').notNullable()
      table.string('phone').notNullable()
    })
  }
}

module.exports = DropProviderColumnsSchema

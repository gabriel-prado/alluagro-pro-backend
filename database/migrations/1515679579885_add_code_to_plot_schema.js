'use strict'

const Schema = use('Schema')

class AddCodeToPlotSchema extends Schema {
  up () {
    this.table('plots', (table) => {
      table.string('code').notNullable()
    })
  }

  down () {
    this.table('plots', (table) => {
      table.dropColumn('code')
    })
  }
}

module.exports = AddCodeToPlotSchema

'use strict'

const Schema = use('Schema')

class RemoveNotNullFromYearInMachineSchema extends Schema {
  up () {
    this.alter('machines', (table) => {
      table.integer('year').alter()
    })
  }

  down () {
    this.alter('machines', (table) => {
      table.integer('year').notNullable().alter()
    })
  }
}

module.exports = RemoveNotNullFromYearInMachineSchema

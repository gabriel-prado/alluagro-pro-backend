'use strict'

const Schema = use('Schema')

class MachineTypeSchema extends Schema {
  up () {
    this.create('machine_types', (table) => {
      table.increments()
      table.string('name').notNullable()
      table.integer('machineCategory_id').notNullable().unsigned()
      table.foreign('machineCategory_id').references('machine_categories.id')
      table.timestamps()
    })
  }

  down () {
    this.drop('machine_types')
  }
}

module.exports = MachineTypeSchema

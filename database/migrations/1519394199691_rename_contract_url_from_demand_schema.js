'use strict'

const Schema = use('Schema')

class RenameContractUrlFromDemandSchema extends Schema {
  up () {
    this.alter('demands', (table) => {
      table.renameColumn('contract_url', 'contracts')
    })
  }

  down () {
    this.alter('demands', (table) => {
      table.renameColumn('contracts', 'contract_url')
    })
  }
}

module.exports = RenameContractUrlFromDemandSchema

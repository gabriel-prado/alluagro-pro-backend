'use strict'

const Schema = use('Schema')

class AlterPhotoFromCompanySchema extends Schema {
  up () {
    this.alter('companies', (table) => {
      table.string('photo_url').defaultTo('assets/img/company-icon.png').alter()
    })
  }

  down () {
    this.alter('companies', (table) => {
      table.string('photo_url').alter()
    })
  }
}

module.exports = AlterPhotoFromCompanySchema

'use strict'

const Schema = use('Schema')

class ReportSchema extends Schema {
  up () {
    this.create('reports', (table) => {
      table.increments()
      table.integer('machine_id').notNullable().unsigned()
      table.foreign('machine_id').references('machines.id')
      table.integer('plot_id').notNullable().unsigned()
      table.foreign('plot_id').references('plots.id')
      table.integer('demand_id').notNullable().unsigned()
      table.foreign('demand_id').references('demands.id')
      table.float('engine_hourmeter')
      table.float('rotor_hourmeter')
      table.float('fuel')
      table.float('chopper_losses_1')
      table.float('chopper_losses_2')
      table.float('chopper_losses_3')
      table.float('chopper_losses_4')
      table.float('platform_losses_1')
      table.float('platform_losses_2')
      table.float('platform_losses_3')
      table.float('platform_losses_4')
      table.float('hectares_harvested')
      table.text('comments')
      table.date('date')
      table.timestamps()
    })
  }

  down () {
    this.drop('reports')
  }
}

module.exports = ReportSchema

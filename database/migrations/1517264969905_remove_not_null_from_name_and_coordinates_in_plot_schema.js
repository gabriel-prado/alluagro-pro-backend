'use strict'

const Schema = use('Schema')

class RemoveNotNullFromNameAndCoordinatesInPlotSchema extends Schema {
  up () {
    this.table('plots', (table) => {
      table.string('name').alter()
      table.text('coordinates').alter()
    })
  }

  down () {
    this.table('plots', (table) => {
      table.string('name').notNullable().alter()
      table.text('coordinates').notNullable().alter()
    })
  }
}

module.exports = RemoveNotNullFromNameAndCoordinatesInPlotSchema

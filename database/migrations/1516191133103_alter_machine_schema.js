'use strict'

const Schema = use('Schema')

class AlterMachineSchema extends Schema {
  up () {
    this.table('machines', (table) => {
      table.enum('harvester_brand', ['JOHN_DEERE', 'VALTRA', 'NEW_HOLLAND', 'CASE', 'MASSEY_FERGUSON'])
      table.boolean('harvester_soy')
      table.string('harvester_soy_brand')
      table.enum('harvester_soy_model', ['DRAPER', 'CARACOL'])
      table.integer('harvester_soy_feet').unsigned()
      table.boolean('harvester_corn')
      table.string('harvester_corn_brand')
      table.integer('harvester_corn_lines').unsigned()
      table.float('harvester_corn_spacing').unsigned()
      table.integer('harvester_corn_feet').unsigned()
      table.enum('harvester_motor_type', ['ROTOR', 'CILINDRO'])
    })
  }

  down () {
    this.table('machines', (table) => {
      table.dropColumn('harvester_brand')
      table.dropColumn('harvester_soy')
      table.dropColumn('harvester_soy_brand')
      table.dropColumn('harvester_soy_model')
      table.dropColumn('harvester_soy_feet')
      table.dropColumn('harvester_corn')
      table.dropColumn('harvester_corn_brand')
      table.dropColumn('harvester_corn_lines')
      table.dropColumn('harvester_corn_spacing')
      table.dropColumn('harvester_corn_feet')
      table.dropColumn('harvester_motor_type')
    })
  }
}

module.exports = AlterMachineSchema

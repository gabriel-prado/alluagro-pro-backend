'use strict'

const Schema = use('Schema')

class AddHectaresToDemandSchema extends Schema {
  up () {
    this.table('demands', (table) => {
      table.float('hectares').defaultTo(0).notNullable()
    })
  }

  down () {
    this.table('demands', (table) => {
      table.dropColumn('hectares')
    })
  }
}

module.exports = AddHectaresToDemandSchema

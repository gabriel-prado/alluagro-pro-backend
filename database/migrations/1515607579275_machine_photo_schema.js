'use strict'

const Schema = use('Schema')

class MachinePhotoSchema extends Schema {
  up () {
    this.create('machine_photos', (table) => {
      table.increments()
      table.integer('machine_id').notNullable().unsigned()
      table.foreign('machine_id').references('machines.id')
      table.string('photo_url').notNullable()
      table.boolean('featured').notNullable().defaultTo(false)
      table.timestamps()
    })
  }

  down () {
    this.drop('machine_photos')
  }
}

module.exports = MachinePhotoSchema

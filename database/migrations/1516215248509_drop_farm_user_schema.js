'use strict'

const Schema = use('Schema')

class DropFarmUserSchema extends Schema {
  up () {
    this.drop('farm_user')
  }

  down () {
    this.create('farm_user', (table) => {
      table.integer('user_id').notNullable().unsigned()
      table.foreign('user_id').references('users.id')
      table.integer('farm_id').notNullable().unsigned()
      table.foreign('farm_id').references('farms.id')
      table.timestamps()
    })
  }
}

module.exports = DropFarmUserSchema

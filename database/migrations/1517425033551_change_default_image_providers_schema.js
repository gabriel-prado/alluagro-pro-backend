'use strict'

const Schema = use('Schema')
const Database = use('Database')
const newPhotoUrl = 'assets/img/no-image.png'
const oldPhotoUrl = 'assets/img/company-icon.png'

class ChangeDefaultImageProvidersSchema extends Schema {
  up () {
    this.alter('providers', async (table) => {
      table.string('photo_url').defaultTo(newPhotoUrl).alter()

      try {
        await Database.table('providers').where({'photo_url': oldPhotoUrl}).update({
          'photo_url': newPhotoUrl
        })
      } catch(e) {}
    })
  }

  down () {
    this.alter('providers', async (table) => {
      table.string('photo_url').defaultTo(oldPhotoUrl).alter()

      try {
        await Database.table('providers').where({'photo_url': newPhotoUrl}).update({
          'photo_url': oldPhotoUrl
        })
      } catch(e) {}
    })
  }
}

module.exports = ChangeDefaultImageProvidersSchema

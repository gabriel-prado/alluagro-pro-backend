'use strict'

const Schema = use('Schema')

class AlterBrandFromMachineSchema extends Schema {
  up () {
    this.alter('machines', (table) => {
      table.string('brand').alter()
    })
  }

  down () {
    this.alter('machines', (table) => {
      table.string('brand').notNullable().alter()
    })
  }
}

module.exports = AlterBrandFromMachineSchema

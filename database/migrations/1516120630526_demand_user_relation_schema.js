'use strict'

const Schema = use('Schema')

class DemandUserRelationSchema extends Schema {
  up () {
    this.table('demands', (table) => {
      table.integer('monitor_id').unsigned()
      table.foreign('monitor_id').references('users.id')
    })
  }

  down () {
    this.table('demands', (table) => {
      table.dropForeign('monitor_id')
      table.dropColumn('monitor_id')
    })
  }
}

module.exports = DemandUserRelationSchema

'use strict'

const Schema = use('Schema')

class AddVerificationCodeToUserSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.string('verification_code')
    })
  }

  down () {
    this.table('users', (table) => {
      table.dropColumn('verification_code')
    })
  }
}

module.exports = AddVerificationCodeToUserSchema

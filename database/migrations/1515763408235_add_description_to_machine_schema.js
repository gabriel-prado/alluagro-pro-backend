'use strict'

const Schema = use('Schema')

class AddDescriptionToMachineSchema extends Schema {
  up () {
    this.table('machines', (table) => {
      table.text('description')
    })
  }

  down () {
    this.table('machines', (table) => {
      table.dropColumn('description')
    })
  }
}

module.exports = AddDescriptionToMachineSchema

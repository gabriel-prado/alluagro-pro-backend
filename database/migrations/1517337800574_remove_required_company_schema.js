'use strict'

const Schema = use('Schema')

class RemoveRequiredCompanySchema extends Schema {
  up () {
    this.table('companies', (table) => {
      table.string('zip_code').alter()
      table.string('state').alter()
      table.string('city').alter()
      table.string('district').alter()
      table.string('street').alter()
      table.integer('number').alter()
    })
  }

  down () {
    this.table('companies', (table) => {
      table.string('zip_code').notNullable().alter()
      table.string('state').notNullable().alter()
      table.string('city').notNullable().alter()
      table.string('district').notNullable().alter()
      table.string('street').notNullable().alter()
      table.integer('number').notNullable().alter()
    })
  }
}

module.exports = RemoveRequiredCompanySchema

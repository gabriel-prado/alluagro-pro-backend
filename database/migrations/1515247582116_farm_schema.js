'use strict'

const Schema = use('Schema')

class FarmSchema extends Schema {
  up () {
    this.create('farms', (table) => {
      table.increments()
      table.string('name').notNullable()
      table.float('hectares').notNullable()
      table.integer('company_id').notNullable().unsigned()
      table.foreign('company_id').references('companies.id')
      table.timestamps()
    })
  }

  down () {
    this.drop('farms')
  }
}

module.exports = FarmSchema

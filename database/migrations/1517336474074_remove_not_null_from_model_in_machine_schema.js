'use strict'

const Schema = use('Schema')

class RemoveNotNullFromModelInMachineSchema extends Schema {
  up () {
    this.alter('machines', (table) => {
      table.string('model').alter()
    })
  }

  down () {
    this.alter('machines', (table) => {
      table.string('model').notNullable().alter()
    })
  }
}

module.exports = RemoveNotNullFromModelInMachineSchema

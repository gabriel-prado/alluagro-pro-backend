'use strict'

const Schema = use('Schema')

class AddFieldsToDemandMachineSchema extends Schema {
  up () {
    this.table('demand_machine', (table) => {
      table.increments()
      table.string('machine_code').notNullable()
    })
  }

  down () {
    this.table('demand_machine', (table) => {
      table.dropColumn('id')
      table.dropColumn('machine_code')
    })
  }
}

module.exports = AddFieldsToDemandMachineSchema

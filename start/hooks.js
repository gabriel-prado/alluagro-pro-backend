const { hooks } = require('@adonisjs/ignitor')

hooks.after.providersBooted(() => {
  const Validator = use('Validator')
  const Database = use('Database')

  const isHarvester = async (data, field, message, args, get) => {
    const value = get(data, field)

    if (data.machineCategory_id) {
      const category = await Database.table('machine_categories').where('id', data.machineCategory_id).first()

      if ( (category.fields === args[0] && (value === null || value === undefined)) || !category ) {
        throw message
      }
      else return

    } else throw message
  }

  Validator.extend('machineFields', isHarvester)
})

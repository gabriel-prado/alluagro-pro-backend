'use strict'

const Route = use('Route')

Route.group('Authentication', () => {
  Route.post('/login', 'AuthController.login')
  Route.get('/me', 'AuthController.me').middleware(['auth', 'acl:ADMIN,MONITOR,MANAGER'])
  Route.get('/firebase', 'AuthController.generateFirebaseToken').middleware(['auth', 'acl:ADMIN,MONITOR,MANAGER'])
}).prefix('auth')

Route.group('Recover Password', () => {
  Route.post('/generate-code', 'RecoverPasswordController.generateCode').validator('GenerateCode')
  Route.put('/change-password', 'RecoverPasswordController.changePassword').validator('ChangePassword')
}).prefix('user')

Route
  .resource('user', 'UserController')
  .only(['index', 'store', 'show', 'update', 'destroy'])
  .middleware(['auth', 'acl:ADMIN'])
  .validator(new Map([
    [['user.store'], ['StoreUser']],
    [['user.update'], ['UpdateUser']]
  ]))

Route
  .resource('company', 'CompanyController')
  .only(['index', 'store', 'show', 'update', 'destroy'])
  .middleware(['auth', 'acl:ADMIN'])
  .validator(new Map([
    [['company.store'], ['StoreCompany']],
    [['company.update'], ['UpdateCompany']]
  ]))

Route
  .resource('company.farm', 'FarmController')
  .only(['index', 'store', 'show', 'update', 'destroy'])
  .middleware(['auth', 'acl:ADMIN'])
  .validator(new Map([
    [['company.farm.store'], ['StoreFarm']],
    [['company.farm.update'], ['UpdateFarm']]
  ]))

Route
  .resource('provider', 'ProviderController')
  .only(['index', 'store', 'show', 'update', 'destroy'])
  .middleware(['auth', 'acl:ADMIN'])
  .validator(new Map([
    [['provider.store'], ['StoreProvider']],
    [['provider.update'], ['UpdateProvider']]
  ]))

Route
  .resource('provider.machine', 'ProviderMachineController')
  .only(['index', 'store', 'update', 'show', 'destroy'])
  .middleware(['auth', 'acl:ADMIN'])
  .validator(new Map([
    [['provider.machine.store'], ['StoreMachine']],
    [['provider.machine.update'], ['UpdateMachine']]
  ]))

Route
  .resource('machine-category', 'MachineCategoryController')
  .only(['index'])
  .middleware(['auth', 'acl:ADMIN'])

Route
  .resource('machine-category.machine-type', 'MachineTypeController')
  .only(['index'])
  .middleware(['auth', 'acl:ADMIN'])

Route
  .resource('demand', 'DemandController')
  .only(['index', 'store', 'update', 'show', 'destroy'])
  .middleware('auth')
  .middleware(new Map([
    [['demand.store', 'demand.update', 'demand.destroy'], ['acl:ADMIN']],
    [['demand.show', 'demand.index'], ['acl:ADMIN,MANAGER,MONITOR']]
  ]))
  .validator(new Map([
    [['demand.store'], ['StoreDemand']],
    [['demand.update'], ['UpdateDemand']]
  ]))

Route
  .resource('demand.monitor', 'DemandMonitorController')
  .only(['index'])
  .middleware(['auth', 'acl:ADMIN'])

Route
  .group('Contract', () => {
    Route.post('/', 'DemandController.storeContract')
    Route.get('/', 'DemandController.getContract')
    Route.delete('/:index', 'DemandController.destroyContract')
  })
  .prefix('demand/:id/contract')
  .middleware(['auth', 'acl:ADMIN'])

Route
  .put('demand/:id/status', 'DemandController.status')
  .middleware(['auth', 'acl:ADMIN'])
  .validator('UpdateDemandStatus')

Route
  .get('demand/:id/plot', 'DemandController.fetchPlots')
  .middleware(['auth', 'acl:ADMIN,MONITOR,MANAGER'])

Route
  .get('demand/:id/user', 'DemandController.fetchUsers')
  .middleware(['auth', 'acl:ADMIN,MONITOR,MANAGER'])

Route
  .get('demand/:id/phase', 'DemandController.getPhases')
  .middleware(['auth', 'acl:ADMIN,MANAGER'])

Route
  .post('demand/:id/machine-request', 'DemandController.machineRequest')
  .middleware(['auth', 'acl:MANAGER'])

Route
  .resource('report', 'ReportController')
  .only(['index'])
  .middleware(['auth', 'acl:ADMIN,MONITOR,MANAGER'])

Route
  .post('demand/:demand_id/machine/:machine_id/report', 'ReportController.upsert')
  .middleware(['auth', 'acl:MONITOR'])
  .validator('UpsertReport')

Route
  .resource('farm.plot', 'PlotController')
  .only(['index'])
  .middleware(['auth', 'acl:ADMIN'])

Route
  .resource('demand.machine', 'DemandMachineController')
  .only(['index', 'show'])
  .middleware(['auth', 'acl:ADMIN,MANAGER,MONITOR'])

Route
  .get('demand/:demand_id/machine/:machine_id/report', 'DemandMachineController.indexOfReports')
  .middleware(['auth', 'acl:ADMIN,MONITOR,MANAGER'])

Route
  .resource('machine.machine-photo', 'MachinePhotoController')
  .only(['index'])
  .middleware(['auth', 'acl:ADMIN,MONITOR,MANAGER'])

Route
  .resource('machine', 'MachineController')
  .only(['index'])
  .middleware(['auth', 'acl:ADMIN'])

Route
  .resource('demand.observation', 'DemandObservationController')
  .only(['store', 'index'])
  .middleware('auth')
  .middleware(new Map([
    [['demand.observation.index'], ['acl:ADMIN,MONITOR,MANAGER']],
    [['demand.observation.store'], ['acl:MONITOR']]
  ]))
  .validator(new Map([
    [['demand.observation.store'], ['StoreDemandObservation']]
  ]))

Route
  .group('Demand Analytics', () => {
    Route.get('/percentage-harvested', 'DemandAnalyticsController.percentageHarvested')
    Route.get('/harvest-pace', 'DemandAnalyticsController.harvestPace')
  })
  .prefix('demand/:demand_id/analytics/')
  .middleware(['auth', 'acl:ADMIN,MANAGER'])

Route
  .group('Demand Machine Analytics', () => {
    Route.get('/general', 'DemandMachineAnalyticsController.general')
    Route.get('/harvested-hectare', 'DemandMachineAnalyticsController.hectaresHarvested')
    Route.get('/loss', 'DemandMachineAnalyticsController.lossesAverage')
    Route.get('/consumption', 'DemandMachineAnalyticsController.consumption')
    Route.get('/hour-meter', 'DemandMachineAnalyticsController.hourMeter')
  })
  .prefix('demand/:demand_id/machine/:machine_id/analytics/')
  .middleware(['auth', 'acl:ADMIN,MANAGER'])

# Alluagro PRO Backend

This is the API server for Alluagro PRO application, it was implemented with:

1. AdonisJs 
2. Bodyparser
3. JWT Authentication
4. CORS
5. Lucid ORM
6. Migrations and seeds
7. Validators

## Setup

* Install adonis-cli with ``` npm i -g @adonisjs/cli ```
* Copy .env.example to .env and set your environment configs
* Install all project dependencies with ``` npm i ```
* Run all migrations with ``` adonis migration:run ```
* Run all seeds with ``` adonis seed ```
* Run application in developmment mode with ``` adonis serve --dev ```
* For more command line tools, just type ``` adonis ```

## Testing

* Configure your test environment in .env.testing (overwrites .env)
* Create database and test database
* Run tests with ``` adonis test ```

## Contributing
* Tests are the minimum, do it well
* Clean, standardized and legible code is essential
* Infrastructure and tooling improvements are aways welcome

## Improvements
- [ ] Standard.js implementation
- [ ] ```npm run adonis``` wrapper to avoid local adonis-cli installation
- [ ] routes.js decoupling to each resource controller
- [ ] factory.js decoupling to each resource controller
- [ ] CrudController implementation for all basic operations
- [ ] Number validator

## Authors
* João Vitor Pires de Sousa (<joao@hubx.me>)
* Frederico Guimarães Genovez (<frederico@hubx.me>)

'use strict'

/**
 * This class handles all exceptions thrown during
 * the HTTP request lifecycle.
 *
 * @class ExceptionHandler
 */
class ExceptionHandler {
  /**
   * Handle exception thrown during the HTTP lifecycle
   *
   * @method handle
   *
   * @param  {Object} error
   * @param  {Object} options.request
   * @param  {Object} options.response
   *
   * @return {void}
   */
  async handle ({ status, code, message }, { request, response }) {
    if(code === 'ER_ROW_IS_REFERENCED_2') {
      const regex = new RegExp(/fails \(\`.*\`\.\`(.*)\`,/)
      const table  = regex.exec(message)

      return response.status(status).send({ code: 'ER_ROW_IS_REFERENCED', table: table[1].toUpperCase() })
    }

    response.status(status).send({ code, message })
  }

  /**
   * Report exception for logging or debugging.
   *
   * @method report
   *
   * @param  {Object} error
   * @param  {Object} options.request
   *
   * @return {void}
   */
  async report (error, { request }) {
  }
}

module.exports = ExceptionHandler

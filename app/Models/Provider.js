'use strict'

const Model = use('Model')

class Provider extends Model {
  machines() {
    return this.hasMany('App/Models/Machine')
  }

  getEmployeesAreClt(num) {
    return Number(num) === 1
  }
}

module.exports = Provider

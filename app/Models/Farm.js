'use strict'

const Model = use('Model')

class Farm extends Model {
  company() {
    return this.belongsTo('App/Models/Company')
  }

  plots() {
    return this.hasMany('App/Models/Plot')
  }

  demands() {
    return this.hasMany('App/Models/Demand')
  }
}

module.exports = Farm

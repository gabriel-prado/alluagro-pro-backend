'use strict'

const Model = use('Model')

class DemandMachine extends Model {
  static get table () {
    return 'demand_machine'
  }

  machine() {
    return this.belongsTo('App/Models/Machine')
  }
}

module.exports = DemandMachine

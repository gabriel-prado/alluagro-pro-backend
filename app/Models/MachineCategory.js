'use strict'

const Model = use('Model')

class MachineCategory extends Model {
  machineTypes() {
    return this.hasMany('App/Models/MachineType')
  }

  machines() {
    return this.hasMany('App/Models/Machine')
  }
}

module.exports = MachineCategory

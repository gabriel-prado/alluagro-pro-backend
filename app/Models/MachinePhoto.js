'use strict'

const Model = use('Model')

class MachinePhoto extends Model {
  machine() {
    return this.belongsTo('App/Models/Machine')
  }
}

module.exports = MachinePhoto

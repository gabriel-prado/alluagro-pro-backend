'use strict'

const Model = use('Model')
const moment = require('moment')

class Report extends Model {
  getDate(date) {
    return moment(date).format('YYYY-MM-DD')
  }

  demand() {
    return this.belongsTo('App/Models/Demand', 'demand_id')
  }

  machine() {
    return this.belongsTo('App/Models/Machine', 'machine_id')
  }

  plot() {
    return this.belongsTo('App/Models/Plot', 'plot_id')
  }
}

module.exports = Report

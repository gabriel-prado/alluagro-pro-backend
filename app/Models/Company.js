'use strict'

const Model = use('Model')

class Company extends Model {
  farms() {
    return this.hasMany('App/Models/Farm')
  }

  users() {
    return this.hasMany('App/Models/User')
  }

  demands() {
    return this.hasMany('App/Models/Demand')
  }
}

module.exports = Company

'use strict'

const Model = use('Model')

class User extends Model {
  static boot () {
    super.boot()
    this.addHook('beforeCreate', 'User.hashPassword')
    this.addHook('beforeUpdate', 'User.hashPassword')
  }

  static get hidden () {
    return ['password']
  }

  company() {
    return this.belongsTo('App/Models/Company')
  }

  demands() {
    return this.hasMany('App/Models/Demand')
  }
}

module.exports = User

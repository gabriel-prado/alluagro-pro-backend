'use strict'

const Model = use('Model')
const moment = require('moment')

class DemandObservation extends Model {
  getDate(date) {
    return moment(date).format('YYYY-MM-DD')
  }

  demand() {
    return this.belongsTo('App/Models/Demand')
  }
}

module.exports = DemandObservation

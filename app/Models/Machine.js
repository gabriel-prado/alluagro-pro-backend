'use strict'

const Model = use('Model')

class Machine extends Model {
  getHarvesterSoy(num) {
    return Number(num) === 1
  }

  getHarvesterCorn(num) {
    return Number(num) === 1
  }

  machineCategory() {
    return this.belongsTo('App/Models/MachineCategory')
  }

  machineType() {
    return this.belongsTo('App/Models/MachineType')
  }

  machinePhotos() {
    return this.hasMany('App/Models/MachinePhoto')
  }

  demands() {
    return this.belongsToMany('App/Models/Demand').withTimestamps().withPivot(['machine_code'])
  }

  reports() {
    return this.hasMany('App/Models/Report')
  }

  provider() {
    return this.belongsTo('App/Models/Provider')
  }
}

module.exports = Machine

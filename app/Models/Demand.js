'use strict'

const Model = use('Model')
const moment = require('moment')

class Demand extends Model {
  getContracts(contracts) {
    return JSON.parse(contracts) || []
  }

  setContracts(contracts) {
    return (!!contracts) ? JSON.stringify(contracts) : '[]'
  }

  getStartDate(date) {
    return moment(date).format('YYYY-MM-DD')
  }

  getEndDate(date) {
    return moment(date).format('YYYY-MM-DD')
  }

  company() {
    return this.belongsTo('App/Models/Company')
  }

  farm() {
    return this.belongsTo('App/Models/Farm')
  }

  machines() {
    return this.belongsToMany('App/Models/Machine').withTimestamps().withPivot(['machine_code'])
  }

  monitor() {
    return this.belongsTo('App/Models/User', 'monitor_id')
  }

  reports() {
    return this.hasMany('App/Models/Report')
  }

  observations() {
    return this.hasMany('App/Models/DemandObservation')
  }
}

module.exports = Demand

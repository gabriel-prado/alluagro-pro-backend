'use strict'

const Model = use('Model')

class Plot extends Model {
  getCoordinates(coordinates) {
    return JSON.parse(coordinates)
  }

  setCoordinates(coordinates) {
    return JSON.stringify(coordinates)
  }

  farm() {
    return this.belongsTo('App/Models/Farm')
  }

  reports() {
    return this.hasMany('App/Models/Report')
  }
}

module.exports = Plot

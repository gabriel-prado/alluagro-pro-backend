'use strict'

const Model = use('Model')

class MachineType extends Model {
  machineCategory() {
    return this.belongsTo('App/Models/MachineCategory')
  }

  machines() {
    return this.hasMany('App/Models/Machine')
  }
}

module.exports = MachineType

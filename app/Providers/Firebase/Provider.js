const { ServiceProvider } = require('@adonisjs/fold')
const firebase = require('./index')

class FirebaseProvider extends ServiceProvider {
  register() {
    const Config = this.app.use('Adonis/Src/Config')

    this.app.singleton('App/Firebase', () => firebase(Config.get('firebase')))
  }
}

module.exports = FirebaseProvider

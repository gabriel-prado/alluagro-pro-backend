const FirebaseAdmin = require('firebase-admin')

module.exports = (Config) => {
  return FirebaseAdmin.initializeApp({
    credential: FirebaseAdmin.credential.cert(Config.credential),
    databaseURL: Config.databaseURL
  })
}

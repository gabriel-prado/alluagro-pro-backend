const { ServiceProvider, ioc } = require('@adonisjs/fold')
const path = require('path')
const fs = require('fs')

class ServicesProvider extends ServiceProvider {
  register() {
    let servicesPath = path.join(__dirname, '..', '..', 'Services')
    let services = fs.readdirSync(servicesPath)

    services.forEach(filename => {
      let service = filename.replace('.js', '')

      this.app.singleton(`Services/${service}`, () => {
        return ioc.make(`App/Services/${service}`)
      })
    })
  }
}

module.exports = ServicesProvider
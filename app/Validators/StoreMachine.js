'use strict'

class StoreMachine {

  get rules () {
    let rules = {
      machineCategory_id: 'required|integer',
      machineType_id: 'required|integer',
      provider_id: 'integer',
      brand: 'string',
      model: 'string',
      year: 'integer',
      description: 'string',
      harvester_brand: 'machineFields:HARVESTER|in:JOHN_DEERE, VALTRA, NEW_HOLLAND, CASE, MASSEY_FERGUSON',
      harvester_soy: 'boolean',
      harvester_corn: 'boolean',
      harvester_motor_type: 'in:ROTOR, CILINDRO',
      harvester_soy_brand: 'string',
      harvester_soy_model: 'in:DRAPER, CARACOL',
      harvester_soy_feet: 'integer',
      harvester_corn_brand: 'string',
      harvester_corn_lines: 'integer',
      harvester_corn_spacing: 'integer',
      harvester_corn_feet: 'integer',
      machinePhotos: 'array',
      'machinePhotos.*.photo_url': 'required|string',
      'machinePhotos.*.featured': 'boolean'
    }

    return rules
  }

  get data () {
    const body = this.ctx.request.body
    let obj = {
      machineCategory_id: body.machineCategory_id,
      machineType_id: body.machineType_id,
      provider_id: body.provider_id,
      brand: body.brand,
      model: body.model,
      year: body.year,
      description: body.description,
      harvester_brand: body.harvester_brand,
      harvester_soy: body.harvester_soy,
      harvester_corn: body.harvester_corn,
      harvester_motor_type: body.harvester_motor_type,
      machinePhotos: body.machinePhotos
    }

    if(obj.harvester_soy) {
      obj.harvester_soy_brand = body.harvester_soy_brand
      obj.harvester_soy_model = body.harvester_soy_model
      obj.harvester_soy_feet = body.harvester_soy_feet
    }

    if(obj.harvester_corn) {
      obj.harvester_corn_brand = body.harvester_corn_brand
      obj.harvester_corn_lines = body.harvester_corn_lines
      obj.harvester_corn_spacing = body.harvester_corn_spacing
      obj.harvester_corn_feet = body.harvester_corn_feet
    }

    this.ctx.request.body = obj
    return Object.assign({}, this.ctx.request.body, {})
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = StoreMachine

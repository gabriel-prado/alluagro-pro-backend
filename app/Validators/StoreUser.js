'use strict'

class StoreUser {
  get rules () {
    return {
      name: 'required',
      email: 'required|email|unique:users,email',
      password: 'required',
      role: 'required|in:ADMIN,MONITOR,MANAGER',
      photo_url: 'string',
      company_id: 'integer',
      phone: 'string',
      verification_code: 'string'
    }
  }

  get data () {
    const body = this.ctx.request.body
    let obj = {
      name: body.name,
      email: body.email,
      password: body.password,
      role: body.role,
      photo_url: body.photo_url,
      company_id: body.company_id,
      phone: body.phone,
      verification_code: body.verification_code
    }

    this.ctx.request.body = obj
    return Object.assign({}, this.ctx.request.body, {})
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = StoreUser

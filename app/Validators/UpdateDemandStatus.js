'use strict'

class UpdateDemandStatus {
  get rules () {
    return {
      status: 'required|in:REQUESTED,IN_ANALYSIS,IN_PROGRESS,COMPLETED'
    }
  }

  get data () {
    const body = this.ctx.request.body
    let obj = {
      status: body.status
    }

    this.ctx.request.body = obj
    return Object.assign({}, this.ctx.request.body, {})
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = UpdateDemandStatus

'use strict'

class StoreDemandObservation {
  get rules () {
    return {
      demand_id: 'required|integer',
      observation: 'required|string',
      date: 'required|date'
    }
  }

  get data () {
    const body = this.ctx.request.body
    let obj = {
      demand_id: Number(this.ctx.request.params.demand_id),
      observation: body.observation,
      date: body.date
    }
    
    this.ctx.request.body = obj
    return Object.assign({}, this.ctx.request.body, {})
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = StoreDemandObservation

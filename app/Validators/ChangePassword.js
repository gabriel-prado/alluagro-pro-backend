'use strict'

class ChangePassword {
  get rules () {
    return {
      code: 'required|string',
      newPassword: 'required|string',
      confirmPassword: 'required|string|same:newPassword'
    }
  }

  get data () {
    let body = this.ctx.request.body
    let obj = {
      code: body.code,
      newPassword: body.newPassword,
      confirmPassword: body.confirmPassword
    }

    this.ctx.request.body = obj
    return Object.assign({}, this.ctx.request.body, {})
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = ChangePassword

'use strict'

class UpdateMachine {

  get rules () {
    let rules = {
      id: 'required|integer',
      machineCategory_id: 'integer',
      machineType_id: 'integer',
      provider_id: 'required|integer',
      brand: 'string',
      model: 'string',
      year: 'integer',
      description: 'string',
      harvester_brand: 'in:JOHN_DEERE, VALTRA, NEW_HOLLAND, CASE, MASSEY_FERGUSON',
      harvester_soy: 'boolean',
      harvester_corn: 'boolean',
      harvester_motor_type: 'in:ROTOR, CILINDRO',
      harvester_soy_brand: 'string',
      harvester_soy_model: 'in:DRAPER, CARACOL',
      harvester_soy_feet: 'integer',
      harvester_corn_brand: 'string',
      harvester_corn_lines: 'integer',
      harvester_corn_feet: 'integer',
      machinePhotos: 'array',
      'machinePhotos.*.id': 'integer',
      'machinePhotos.*.photo_url': 'string',
      'machinePhotos.*.featured': 'boolean',
      'machinePhotos.*.machine_id': 'required|integer'
    }

    return rules
  }

  get data () {
    const body = this.ctx.request.body
    let obj = {
      id: Number(this.ctx.request.params.id),
      machineCategory_id: body.machineCategory_id,
      machineType_id: body.machineType_id,
      provider_id: Number(this.ctx.request.params.provider_id),
      brand: body.brand,
      model: body.model,
      year: body.year,
      description: body.description,
      harvester_brand: body.harvester_brand,
      harvester_soy: body.harvester_soy,
      harvester_corn: body.harvester_corn,
      harvester_motor_type: body.harvester_motor_type,
      harvester_soy_brand: body.harvester_soy_brand,
      harvester_soy_model: body.harvester_soy_model,
      harvester_soy_feet: body.harvester_soy_feet,
      harvester_corn_brand: body.harvester_corn_brand,
      harvester_corn_lines: body.harvester_corn_lines,
      harvester_corn_spacing: body.harvester_corn_spacing,
      harvester_corn_feet: body.harvester_corn_feet,
      machinePhotos: body.machinePhotos
    }

    if(obj.machinePhotos) obj.machinePhotos.map(p => {
      p.machine_id = obj.id
    })
    
    this.ctx.request.body = obj
    return Object.assign({}, this.ctx.request.body, {})
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = UpdateMachine

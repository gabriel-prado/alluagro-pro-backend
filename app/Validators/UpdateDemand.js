'use strict'

class UpdateDemand {
  get rules () {
    return {
      name: 'required_without_all:start_date,end_date,company_id,farm_id,status,machines,monitor,contracts|string',
      start_date: 'dateFormat:YYYY-MM-DD',
      end_date: 'dateFormat:YYYY-MM-DD',
      company_id: 'integer',
      farm_id: 'integer',
      contracts: 'array',
      status: 'in:REQUESTED,IN_ANALYSIS,IN_PROGRESS,COMPLETED',
      hectares: 'required',
      machines: 'array',
      'machines.*.id': 'required_if:machines|integer',
      'machines.*.machine_code': 'required_if:machines|string',
      'monitor.id': 'required_if:monitor|integer',
      'monitor.name': 'string',
      'monitor.email': 'email',
      'monitor.password': 'string',
      'monitor.role': 'equals:MONITOR',
      'monitor.photo_url': 'string',
      'monitor.company_id': 'integer'
    }
  }

  get data () {
    const body = this.ctx.request.body
    let obj = {
      name: body.name,
      start_date: body.start_date,
      end_date: body.end_date,
      company_id: body.company_id,
      farm_id: body.farm_id,
      contracts: body.contracts,
      status: body.status,
      hectares: body.hectares,
      machines: body.machines,
      monitor: body.monitor
    }

    if(obj.monitor) {
      obj.monitor.role = 'MONITOR'
      delete obj.monitor.company_id
    }

    this.ctx.request.body = obj
    return Object.assign({}, this.ctx.request.body, {})
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = UpdateDemand

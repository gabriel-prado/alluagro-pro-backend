'use strict'

class UpdateProvider {
  get rules () {
    return {
      id: 'required|integer',
      name: 'string',
      trading_name: 'string',
      cnpj: 'string',
      photo_url: 'string',
      type: 'in:PF, PJ',
      email: 'email',
      cpf: 'string',
      rural_inscription: 'string',
      zip_code: 'string',
      city: 'string',
      state: 'string',
      district: 'string',
      street: 'string',
      number: 'integer',
      complement: 'string',
      primary_phone: 'string',
      secondary_phone: 'string',
      observation: 'string',
      employees_are_clt: 'boolean'
    }
  }

  get data () {
    const body = this.ctx.request.body
    let obj = {
      id: Number(this.ctx.request.params.id),
      name: body.name,
      trading_name: body.trading_name,
      cnpj: body.cnpj,
      photo_url: body.photo_url,
      type: body.type,
      email: body.email,
      cpf: body.cpf,
      rural_inscription: body.rural_inscription,
      zip_code: body.zip_code,
      city: body.city,
      state: body.state,
      district: body.district,
      street: body.street,
      number: body.number,
      complement: body.complement,
      primary_phone: body.primary_phone,
      secondary_phone: body.secondary_phone,
      observation: body.observation,
      employees_are_clt: body.employees_are_clt
    }

    this.ctx.request.body = obj
    return Object.assign({}, this.ctx.request.body, {})
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = UpdateProvider

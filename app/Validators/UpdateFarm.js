'use strict'

class UpdateFarm {

  get rules () {
    return {
      id: 'required|integer',
      name: 'string',
      company_id: 'required|integer',
      plots: 'array',
      'plots.*.id': 'integer',
      'plots.*.name': 'string',
      'plots.*.coordinates': 'array',
      'plots.*.farm_id': 'required|integer',
      'plots.*.code': 'string'
    }
  }

  get data () {
    const body = this.ctx.request.body
    let obj = {
      id: Number(this.ctx.request.params.id),
      name: body.name,
      hectares: body.hectares,
      company_id: Number(this.ctx.request.params.company_id),
      plots: body.plots
    }
    
    if(obj.plots) obj.plots.map(p => {
      p.farm_id = obj.id
    })

    this.ctx.request.body = obj
    return Object.assign({}, this.ctx.request.body, {})
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = UpdateFarm

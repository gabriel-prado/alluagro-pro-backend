'use strict'

class StoreDemand {
  get rules () {
    return {
      name: 'required',
      start_date: 'required|dateFormat:YYYY-MM-DD',
      end_date: 'required|dateFormat:YYYY-MM-DD',
      company_id: 'required|integer',
      farm_id: 'required|integer',
      contracts: 'array',
      status: 'required|in:REQUESTED,IN_ANALYSIS,IN_PROGRESS,COMPLETED',
      hectares: 'required',
      machines: 'array',
      'machines.*.id': 'required',
      'machines.*.machine_code': 'required',
      'monitor.name': 'required_if:monitor',
      'monitor.email': 'required_if:monitor|email|unique:users,email',
      'monitor.password': 'required_if:monitor',
      'monitor.role': 'required_if:monitor|equals:MONITOR',
      'monitor.photo_url': 'string'
    }
  }

  get data () {
    const body = this.ctx.request.body
    let obj = {
      name: body.name,
      start_date: body.start_date,
      end_date: body.end_date,
      company_id: body.company_id,
      farm_id: body.farm_id,
      contracts: body.contracts,
      status: body.status,
      hectares: body.hectares,
      machines: body.machines,
      monitor: body.monitor
    }

    if(obj.monitor) {
      obj.monitor.role = 'MONITOR'
      delete obj.monitor.id
      delete obj.monitor.company_id
    }

    this.ctx.request.body = obj
    return Object.assign({}, this.ctx.request.body, {})
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = StoreDemand

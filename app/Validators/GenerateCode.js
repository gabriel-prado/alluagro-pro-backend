'use strict'

class GenerateCode {
  get rules () {
    return {
      email: 'required|email'
    }
  }

  get data () {
    let obj = {
      email: this.ctx.request.body.email
    }

    this.ctx.request.body = obj
    return Object.assign({}, this.ctx.request.body, {})
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = GenerateCode

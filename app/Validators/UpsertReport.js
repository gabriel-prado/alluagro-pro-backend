'use strict'

class UpsertReport {
  get rules () {
    return {
      'date': 'required|dateFormat:YYYY-MM-DD',
      'reports': 'array',
      'reports.*.machine_id': 'integer',
      'reports.*.plot_id': 'integer',
      'reports.*.demand_id': 'integer',
      'reports.*.comments': 'string',
      'reports.*.date': 'dateFormat:YYYY-MM-DD'
    }
  }

  get data () {
    const body = this.ctx.request.body
    let obj = {
      date: body.date
    }

    obj.reports = body.reports.map(r => {
      return {
        machine_id: r.machine_id,
        plot_id: r.plot_id,
        demand_id: r.demand_id,
        engine_hourmeter: r.engine_hourmeter,
        rotor_hourmeter: r.rotor_hourmeter,
        fuel: r.fuel,
        chopper_losses_1: r.chopper_losses_1,
        chopper_losses_2: r.chopper_losses_2,
        chopper_losses_3: r.chopper_losses_3,
        chopper_losses_4: r.chopper_losses_4,
        platform_losses_1: r.platform_losses_1,
        platform_losses_2: r.platform_losses_2,
        platform_losses_3: r.platform_losses_3,
        platform_losses_4: r.platform_losses_4,
        hectares_harvested: r.hectares_harvested,
        comments: r.comments,
        date: r.date
      }
    })

    this.ctx.request.body = obj
    return Object.assign({}, this.ctx.request.body, {})
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = UpsertReport

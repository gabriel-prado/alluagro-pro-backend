'use strict'

class UpdateUser {
  get rules () {
    return {
      id: 'required|integer',
      name: 'string',
      email: 'email',
      password: 'string',
      role: 'in:ADMIN,MONITOR,MANAGER',
      photo_url: 'string',
      company_id: 'integer',
      phone: 'string',
      verification_code: 'string'
    }
  }

  get data () {
    const body = this.ctx.request.body
    let obj = {
      id: Number(this.ctx.request.params.id),
      name: body.name,
      email: body.email,
      password: body.password,
      role: body.role,
      photo_url: body.photo_url,
      company_id: body.company_id,
      phone: body.phone,
      verification_code: body.verification_code
    }

    this.ctx.request.body = obj
    return Object.assign({}, this.ctx.request.body, {})
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = UpdateUser

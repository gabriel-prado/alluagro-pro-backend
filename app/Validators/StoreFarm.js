'use strict'

class StoreFarm {

  get rules () {
    return {
      name: 'required',
      plots: 'array',
      'plots.*.code': 'required'
    }
  }

  get data () {
    const body = this.ctx.request.body
    let obj = {
      name: body.name,
      hectares: body.hectares,
      plots: body.plots
    }

    this.ctx.request.body = obj
    return Object.assign({}, this.ctx.request.body, {})
  }

  fails (errorMessages) {
    return this.ctx.response.status(400).send(errorMessages)
  }
}

module.exports = StoreFarm

'use strict'

const Company = use('App/Models/Company')

class CompanyController {

  async index({ request }) {
    let { all, page, perPage } = request.get()

    page = page || 1
    perPage = perPage || 5

    if(all) {
      return { data: await Company.query() }
    } else {
      return await Company.query().paginate(page, perPage)
    }
  }

  async show({ request, response }) {
    const id = request.params.id
    const company = await Company.find(id)
    if(company) {
        return company
    } else {
        return response.status(404).send({})
    }
  }

  async store({ request, response }) {
    const company = request.all()

    return response.status(201).json(await Company.create(company))
  }

  async update({ request, response }) {
    let attributes = request.all()
    let company = await Company.findOrFail(attributes.id)

    company.merge(attributes)
    await company.save()
    await company.reload()

    return company
  }

  async destroy({ request }) {
    const { id } = request.params
    const company = await Company.findOrFail(id)

    await company.delete()
  }

}

module.exports = CompanyController

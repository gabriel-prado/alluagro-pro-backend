'use strict'

const Demand = use('App/Models/Demand')

class DemandMachineAnalyticsController {

  static get inject() {
    return ['Services/Analytics']
  }

  constructor(AnalyticsService) {
    this.analyticsService = AnalyticsService
  }

  async hectaresHarvested({ request, response, auth }) {
    let { startDate, endDate } = request.all()
    let { demand_id, machine_id }  = request.params

    let demand = await Demand.findOrFail(demand_id)

    if(auth.user.role === 'MANAGER' && demand.company_id !== auth.user.company_id) {
      return response.status(403).send({})
    }

    let name = 'Hectares Colhidos'
    let series = await this.analyticsService.harvestedHectares(demand_id, machine_id, startDate, endDate)

    return [{ name, series }]
  }

  async lossesAverage({ request, response, auth }) {
    let { startDate, endDate } = request.all()
    let { demand_id, machine_id }  = request.params

    let demand = await Demand.findOrFail(demand_id)

    if(auth.user.role === 'MANAGER' && demand.company_id !== auth.user.company_id) {
      return response.status(403).send({})
    }

    return [
      {
        name: 'Picador',
        series: await this.analyticsService.averageLoss('chopper', demand_id, machine_id, startDate, endDate)
      },
      {
        name: 'Plataforma',
        series: await this.analyticsService.averageLoss('platform', demand_id, machine_id, startDate, endDate)
      }
    ]
  }

  async hourMeter({ request }) {
    const { startDate, endDate } = request.all()
    const { demand_id, machine_id }  = request.params

    const data = await this.analyticsService.hourMeter(demand_id, machine_id, startDate, endDate)
    return data
  }  

  async general({ request }) {
    let { demand_id, machine_id }  = request.params
    let stats = await this.analyticsService.machineGeneralStats(demand_id, machine_id)

    return [
      { name: 'Hectares colhidos', value: stats.harvested_hectares },
      { name: 'Horas trabalhadas', value: stats.worked_hours },
      { name: 'Consumo médio por hectare', value: stats.consumption_per_hectares },
      { name: 'Perda média do picador', value: stats.chopper_losses_average },
      { name: 'Horas ligada', value: stats.on_hours },
      { name: 'Consumo médio por hora trabalhada', value: stats.consumption_per_worked_hours },
      { name: 'Perda média da plataforma', value: stats.platform_losses_average }
    ]
  }

  async consumption({ request, response, auth }) {
    let { startDate, endDate } = request.all()
    let { demand_id, machine_id }  = request.params

    let demand = await Demand.findOrFail(demand_id)

    if(auth.user.role === 'MANAGER' && demand.company_id !== auth.user.company_id) {
      return response.status(403).send({})
    } else {
      return [
        {
          name: 'Por hectare',
          series: await this.analyticsService.consumptionPerHarvestedHectares(demand, machine_id, startDate, endDate)
        },
        {
          name: 'Por hora trabalhada',
          series: await this.analyticsService.consumptionPerWorkedHours(demand, machine_id, startDate, endDate)
        }
      ]
  
    }
  }

}

module.exports = DemandMachineAnalyticsController

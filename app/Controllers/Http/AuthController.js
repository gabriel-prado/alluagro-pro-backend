'use strict'

class AuthController {
  static get inject() {
    return ['Services/Firebase']
  }

  constructor(FirebaseService) {
    this.firebaseService = FirebaseService
  }

  async login({ request, auth }) {
    const { email, password } = request.all()
    const { token } = await auth.attempt(email, password)

    return { token: `Bearer ${token}`  }
  }

  me({ request, auth }) {
    return auth.user
  }

  async generateFirebaseToken({ request, auth }) {
    return await this.firebaseService.generateToken(auth.user)
  }
}

module.exports = AuthController

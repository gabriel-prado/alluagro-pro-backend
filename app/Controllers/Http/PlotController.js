'use strict'

const Farm = use('App/Models/Farm')

class PlotController {

  async index({ request, response }) {
    let { all, page, perPage } = request.get()

    const farm_id = Number(request.params.farm_id)

    page = page || 1
    perPage = perPage || 5

    let farm = await Farm.find(farm_id)

    if(farm) {
      if(all) {
        return { data: await farm.plots().fetch() }
      } else {
        return await farm.plots().paginate(page, perPage)
      }
    } else {
      return response.status(404).send({})
    }
  }

}

module.exports = PlotController

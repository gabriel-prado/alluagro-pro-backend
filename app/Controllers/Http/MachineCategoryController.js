'use strict'

const MachineCategory = use('App/Models/MachineCategory')

class MachineCategoryController {

  async index({ request }) {
    let { all, page, perPage } = request.get()

    page = page || 1
    perPage = perPage || 5

    if(all) {
      return { data: await MachineCategory.query() }
    } else {
      return await MachineCategory.query().paginate(page, perPage)
    }
  }

}

module.exports = MachineCategoryController

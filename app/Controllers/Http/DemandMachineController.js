'use strict'

const Demand = use('App/Models/Demand')
const Report = use('App/Models/Report')
const DemandMachine = use('App/Models/DemandMachine')

class DemandMachineController {

  async index({ request, response, auth }) {
    let { all, page, perPage } = request.get()

    const demand_id = Number(request.params.demand_id)
    const demand = await Demand.findOrFail(demand_id)

    if(_monitorChecker(auth.user, demand)) return response.status(403).send({})
    if(_managerChecker(auth.user, demand)) return response.status(403).send({})

    page = (all) ? null : page || 1
    perPage = (all) ? null : perPage || 5

    let result = await
      demand
        .machines()
        .with('provider')
        .with('reports', builder => builder.where('demand_id', demand_id))
        .with('machinePhotos')
        .paginate(page, perPage)
    result = result.toJSON()

    result.data = result.data.map(r => {
      r.machine_code = r.__meta__.pivot_machine_code
      r.hectares_harvested = r.reports.reduce((p, c) => p + c.hectares_harvested, 0)

      delete r.reports
      delete r.__meta__
      return r
    })

    return result
  }

  async show({ request, response, auth }) {
    const { demand_id, id } = request.params
    const demand = await Demand.findOrFail(demand_id)

    if(_monitorChecker(auth.user, demand)) return response.status(403).send({})
    if(_managerChecker(auth.user, demand)) return response.status(403).send({})

    let demandMachine =
      await DemandMachine
        .query()
        .with('machine.provider')
        .where({ machine_id: id, demand_id })
        .first()

    if(demandMachine) {
      demandMachine.getRelated('machine').machine_code = demandMachine.machine_code
      return demandMachine.getRelated('machine')
    } else {
      return response.status(404).send({})
    }
  }

  async indexOfReports({ request, response, auth }) {
    const { demand_id, machine_id } = request.params
    let { all, page, perPage, date } = request.get()
    const demand = await Demand.findOrFail(demand_id)

    if(_monitorChecker(auth.user, demand)) return response.status(403).send({})
    if(_managerChecker(auth.user, demand)) return response.status(403).send({})

    page = page || 1
    perPage = perPage || 5

    let reports =
      Report
        .query()
        .where({ date, demand_id, machine_id })

    return (all) ? { data: await reports.fetch() } : await reports.paginate(page, perPage)
  }

}

const _monitorChecker = (user, demand) => user.role === 'MONITOR' && demand.monitor_id !== user.id
const _managerChecker = (user, demand) => user.role === 'MANAGER' && demand.company_id !== user.company_id

module.exports = DemandMachineController

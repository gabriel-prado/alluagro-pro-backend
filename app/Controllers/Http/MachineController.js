'use strict'

const Machine = use('App/Models/Machine')

class MachineController {

  async index({ request }) {
    let { all, page, perPage } = request.get()

    page = page || 1
    perPage = perPage || 5

    let query = Machine.query().with('machinePhotos').with('provider')

    if(all) {
      return { data: await query.fetch() }
    } else {
      return await query.paginate(page, perPage)
    }
  }

}

module.exports = MachineController

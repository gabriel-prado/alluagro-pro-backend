'use strict'

const User = use('App/Models/User')

class UserController {

  async index({ request }) {
    let { all, page, perPage, company_id } = request.get()

    page = page || 1
    perPage = perPage || 5

    let query = User.query()

    if(company_id) {
      query = query.where('company_id', company_id).where('role', 'MANAGER')
    }

    return (all) ? { data: await query.fetch() } : await query.paginate(page, perPage)
  }

  async store({ request, response }) {
    const user = request.all()

    return response.status(201).json(await User.create(user))
  }

  async show({ request, response }) {
    const { id } = request.params
    const user = await User.find(id)
    if(user) {
        return user
    } else {
        response.status(404).send({})
    }
  }

  async update({ request, response, auth }) {
    let attributes = request.all()
    let user = await User.find(attributes.id)

    if(!user) return response.status(404).send({})
    if(((attributes.id !== auth.user.id) && auth.user.role !== 'ADMIN')) {
      return response.status(401).send({})
    }

    user.merge(attributes)
    await user.save()

    return user
  }

  async destroy({ request }) {
    const { id } = request.params
    const user = await User.findOrFail(id)

    await user.delete()
  }

}

module.exports = UserController

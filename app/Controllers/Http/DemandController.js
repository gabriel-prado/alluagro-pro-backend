'use strict'

const Demand = use('App/Models/Demand')
const Monitor = use('App/Models/User')
const User = use('App/Models/User')
const Plot = use('App/Models/Plot')
const Report = use('App/Models/Report')
const Database = use('Database')
const moment = require('moment')
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey(process.env.SENDGRID_API_KEY)

class DemandController {
  static get inject() {
    return ['Services/Demand']
  }

  constructor(DemandService) {
    this.demandService = DemandService
  }

  async index({ request, response, auth }) {
    let { all, page, perPage } = request.get()

    page = (all) ? null : page || 1
    perPage = (all) ? null : perPage || 5

    let demands =
      Demand
        .query()
        .with('company')
        .with('farm')

    if(auth.user.role === 'MANAGER') demands.where('company_id', auth.user.company_id)
    if(auth.user.role === 'MONITOR') demands.where('monitor_id', auth.user.id)

    return await demands.paginate(page, perPage)
  }

  async store({ request, response }) {
    const trx = await Database.beginTransaction()
    try {
      let data = request.all()
      let machines = data.machines
      let monitor = data.monitor
      delete data.machines
      delete data.monitor

      let demand = await Demand.create(data, trx)

      if(machines && demand) {
        const arr = machines.map(m => m.id)
        await demand.machines().attach(arr, (row) => {
          let machineRow = machines.find(m => m.id === row.machine_id)
          row.machine_code = machineRow.machine_code
        }, trx)
      }

      if(monitor && demand) {
        monitor = await Monitor.create(monitor, trx)
        await demand.monitor().associate(monitor, trx)
      }

      trx.commit()
      demand.machines = await demand.machines().fetch()
      demand.monitor = await demand.monitor().fetch()
      return response.status(201).json(demand)
    } catch (e) {
      trx.rollback()
      return response.status(500).send(e)
    }
  }

  async update({ request, response }) {
    const trx = await Database.beginTransaction()
    try {
      let attributes = request.body

      let monitorAttributes = request.body.monitor
      delete attributes.monitor
      let machinesAttributes = request.body.machines
      delete attributes.machines

      let demand = await Demand.find(request.params.id)

      demand.merge(attributes)
      await demand.save(trx)

      if(monitorAttributes) {
        let monitor = await Monitor.find(monitorAttributes.id)
        delete monitor['$attributes'].password
        monitor.merge(monitorAttributes)
        await monitor.save(trx)
      }

      if(machinesAttributes) {
        const arr = machinesAttributes.map(m => m.id)
        await demand.machines().sync(arr, (row) => {
          let machineRow = machinesAttributes.find(m => m.id === row.machine_id)
          row.machine_code = machineRow.machine_code
        }, trx)
      }

      trx.commit()
      demand.machines = await demand.machines().fetch()
      demand.monitor = await demand.monitor().fetch()

      return response.status(200).json(demand)
    } catch (e) {
      trx.rollback()
      return response.status(500).send(e)
    }
  }

  async status({ request, response }) {
    try {
      const status = request.body.status
      let demand = await Demand.findBy('id', request.params.id)

      if (demand) {
        demand.status = status
        await demand.save()
        return response.status(200).json(await Demand.findBy('id', request.params.id))
      } else {
        return response.status(404).send({})
      }

    } catch (e) {
      return response.status(500).send(e)
    }
  }

  async show({ request, response, auth }) {
    const id = request.params.id
    let demand =
      Demand
        .query()
        .where('id', id)
        .with('farm')
        .with('company')

    if(auth.user.role === 'MANAGER') demand.where('company_id', auth.user.company_id)
    if(auth.user.role === 'MONITOR') demand.where('monitor_id', auth.user.id)

    demand = await demand.first()

    return demand ? demand : response.status(404).send({})
  }

  async fetchPlots({ request, response, auth }) {
    let { all, page, perPage } = request.get()
    const id = Number(request.params.id)
    const demand = await Demand.findOrFail(id)

    if(auth.user.role === 'MONITOR' && demand.monitor_id !== auth.user.id) {
      return response.status(403).send({})
    }

    if(auth.user.role === 'MANAGER' && demand.company_id !== auth.user.company_id) {
      return response.status(403).send({})
    }

    const plots = Plot.query().where('farm_id', demand.farm_id)

    page = (all) ? null : page || 1
    perPage = (all) ? null : perPage || 5

    return await plots.paginate(page, perPage)
  }

  async fetchUsers({ request, response, auth }) {
    let { all, page, perPage } = request.get()
    const id = Number(request.params.id)
    const demand = await Demand.findOrFail(id)

    if(auth.user.role === 'MONITOR' && demand.monitor_id !== auth.user.id) {
      return response.status(403).send({})
    }

    if(auth.user.role === 'MANAGER' && demand.company_id !== auth.user.company_id) {
      return response.status(403).send({})
    }

    const users =
      User
        .query()
        .where('role', 'ADMIN')
        .orWhere('id', demand.monitor_id)
        .orWhere('company_id', demand.company_id)

    page = (all) ? null : page || 1
    perPage = (all) ? null : perPage || 5

    return await users.paginate(page, perPage)
  }

  async getPhases({ request, response, auth }) {
    let demand = await Demand.findOrFail(request.params.id)

    if(auth.user.role === 'MANAGER' && demand.company_id !== auth.user.company_id) {
      return response.status(403).send({})
    }

    let type = request.all().type || 'WEEK'

    if(type === 'WEEK') {
      let weeks = await this.demandService.weeks(demand)
      return response.status(200).send(weeks)
    }

    return response.status(400).send({ error: 'INVALID_TYPE' })
  }

  async destroy({ request }) {
    const { id } = request.params
    const demand = await Demand.findOrFail(id)
    const monitor = await demand.monitor().fetch()
    const trx = await Database.beginTransaction()

    try {
      await demand.delete(trx)
      if(monitor) await monitor.delete(trx)

      trx.commit()

      await this.demandService.deleteContractFolder(id)
      await this.demandService.deleteMessages(id)
    } catch(e) {
      trx.rollback()
      throw e
    }
  }

  async machineRequest({ request, response, auth }) {
    const { observation } = request.all()

    let demand = await Demand.findOrFail(Number(request.params.id))
    let farm = await demand.farm().column('name').fetch()
    let company = await demand.company().column('name').fetch()

    if(demand.company_id !== auth.user.company_id) return response.status(403).send({})

    const msg = {
      to: process.env.DEFAULT_MAIL,
      from: auth.user.email,
      subject: `Solicitação de máquina no acompanhamento ${demand.name}` ,
      templateId: process.env.MACHINE_REQUEST_TEMPLATE_ID,
      substitutions: {
        name: auth.user.name,
        farm: farm.name,
        company: company.name,
        demand: demand.name,
        observation
      }
    }

    await sgMail.send(msg)
  }

  async storeContract({ request, response }) {
    const demandId = Number(request.params.id)

    request.multipart.file('contract', {}, async (file) => {
      let url = await this.demandService.addContract(demandId, file.stream)

      return response.status(200).send({ url })
    })

    await request.multipart.process()
  }

  async getContract({ request, response }) {
    const { contract } = request.get()
    const demandId = Number(request.params.id)

    let file = await this.demandService.findContract(demandId, contract)

    response.header('content-type', file.ContentType)
    response.header('content-disposition', `filename=${contract}`)

    return file.Body
  }

  async destroyContract({ request }) {
    const { index } = request.params
    const demandId = Number(request.params.id)
    let demand = await Demand.findOrFail(demandId)
    let contracts = demand.toJSON().contracts
    let contract = null

    if(contracts.length && index > -1) {
      contract = contracts.splice(index, 1)[0]
      demand.merge({ contracts })
      await demand.save()
    }

    await this.demandService.deleteContract(demandId, contract)
  }
}

module.exports = DemandController

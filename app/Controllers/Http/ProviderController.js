'use strict'

const Provider = use('App/Models/Provider')

class ProviderController {

  async index({ request }) {
    let { all, page, perPage } = request.get()

    page = page || 1
    perPage = perPage || 5

    if(all) {
      return { data: await Provider.query() }
    } else {
      return await Provider.query().paginate(page, perPage)
    }
  }

  async show({ request, response }) {
    const id = request.params.id
    const provider = await Provider.find(id)
    if(provider) {
      return provider
    } else {
      response.status(404).send({})
    }
  }

  async store({ request, response }) {
    const provider = request.all()

    return response.status(201).json(await Provider.create(provider))
  }

  async update({ request, response }) {
    let attributes = request.all()
    let provider = await Provider.findOrFail(attributes.id)

    provider.merge(attributes)
    await provider.save()
    await provider.reload()

    return provider
  }

  async destroy({ request }) {
    const { id } = request.params
    const provider = await Provider.findOrFail(id)

    await provider.delete()
  }

}

module.exports = ProviderController

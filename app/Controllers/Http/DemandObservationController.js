'use strict'

const DemandObservation = use('App/Models/DemandObservation')
const Demand = use('App/Models/Demand')

class DemandObservationController {

  async index({ request, response, auth }) {
    let { all, page, perPage, date } = request.get()
    let demandId = request.params.demand_id
    const demand = await Demand.findOrFail(demandId)

    if(auth.user.role === 'MONITOR' && demand.monitor_id !== auth.user.id) {
      return response.status(403).send({})
    }

    if(auth.user.role === 'MANAGER' && demand.company_id !== auth.user.company_id) {
      return response.status(403).send({})
    }

    page = (all) ? null : page || 1
    perPage = (all) ? null : perPage || 5
    
    if(date && demandId) {
      return await DemandObservation.query().where('demand_id', demandId).where('date', date).paginate(page, perPage)
    } else {
      return response.status(404).send({})
    }
  }

  async store({ request, response, auth }) {
    let demandObservation = request.all()
    demandObservation.demand_id = request.params.demand_id
    const demand = await Demand.findOrFail(request.params.demand_id)

    if(auth.user.role === 'MONITOR' && demand.monitor_id !== auth.user.id) {
      return response.status(403).send({})
    }

    return response.status(201).json(await DemandObservation.create(demandObservation))
  }

}

module.exports = DemandObservationController

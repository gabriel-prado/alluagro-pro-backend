'use strict'

const Demand = use('App/Models/Demand')

class DemandAnalyticsController {

  static get inject() {
    return ['Services/Analytics']
  }

  constructor(AnalyticsService) {
    this.analyticsService = AnalyticsService
  }

  async percentageHarvested({ request, response, auth }) {
    const demandId = Number(request.params.demand_id)

    let demand = await Demand.findOrFail(demandId)

    if(auth.user.role === 'MANAGER' && demand.company_id !== auth.user.company_id) {
      return response.status(403).send({})
    }

    return this.analyticsService.percentageHarvested(demandId)
  }

  async harvestPace({ request, response, auth }) {
    const demandId = Number(request.params.demand_id)

    let demand = await Demand.findOrFail(demandId)

    if(auth.user.role === 'MANAGER' && demand.company_id !== auth.user.company_id) {
      return response.status(403).send({})
    }

    return [
      {
        name: 'BURN_UP',
        series: await this.analyticsService.burnup(demandId)
      },
      {
        name: 'COLHIDO',
        series: await this.analyticsService.harvestedHectares(demandId, null, null, null, false, true)
      }
    ]
  }
}

module.exports = DemandAnalyticsController

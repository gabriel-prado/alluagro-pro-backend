'use strict'

const Demand = use('App/Models/Demand')
const Monitor = use('App/Models/User')

class DemandMonitorController {
  async index({ request, response }) {
    let demandId = Number(request.params.demand_id)
    let demand = await Demand.findOrFail(demandId)

    return await Monitor.findOrFail(demand.monitor_id)
  }
}

module.exports = DemandMonitorController

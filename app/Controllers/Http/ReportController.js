'use strict'

const Report = use('App/Models/Report')
const Demand = use('App/Models/Demand')
const Database = use('Database')

class ReportController {
  async upsert({ request, response, auth }) {
    let reports = request.body.reports || []

    const trx = await Database.beginTransaction()
    const machine_id = Number(request.params.machine_id)
    const demand_id = Number(request.params.demand_id)
    const date = request.body.date
    const demand = await Demand.findOrFail(demand_id)

    if(auth.user.role === 'MONITOR' && demand.monitor_id !== auth.user.id) {
      return response.status(403).send({})
    }

    reports = reports.map((report) => {
      report.machine_id = machine_id
      report.demand_id = demand_id
      report.date = date

      return report
    })

    try {
      await Report.query().where({ machine_id, demand_id, date }).transacting(trx).delete()

      const savedReports = await Report.createMany(reports, trx)

      await trx.commit()

      return response.status(200).send(savedReports)
    } catch(e) {
      trx.rollback()
      return response.status(400).send(e)
    }
  }

  async index({ request, response, auth }) {
    let { date } = request.get()
    let demand = await Demand.findBy('monitor_id', auth.user.id)

    if(demand) {
      let machines = await demand
        .machines()
        .with('machinePhotos')
        .with('provider')
        .withCount('reports', (builder) => {
          builder.where('demand_id', demand.id)
          builder.where('date', '=', date || null)
        })
        .fetch()

      machines = machines.toJSON()

      demand.machines = machines.map(m => {
        m.status = ( m.__meta__.reports_count > 0 ) ? 'COMPLETED' : 'PENDING'
        m.machine_code = m.pivot.machine_code
        delete m.pivot
        delete m.__meta__
        return m
      })
    }
    return response.status(200).json(demand)
  }
}

module.exports = ReportController

'use strict'

const Machine = use('App/Models/Machine')
const MachinePhoto = use('App/Models/MachinePhoto')
const Database = use('Database')

class ProviderMachineController {

  async index({ request }) {
    let { all, page, perPage } = request.get()

    const provider_id = Number(request.params.provider_id)

    page = page || 1
    perPage = perPage || 5

    if(all) {
      return { data: await Machine.query().where('provider_id', provider_id) }
    } else {
      return await Machine.query().where('provider_id', provider_id).paginate(page, perPage)
    }
  }

  async store({ request, response }) {
    const trx = await Database.beginTransaction()
    try {
      let machine = request.all()
      let machinePhotos = machine.machinePhotos
      delete machine.machinePhotos
      machine.provider_id = Number(request.params.provider_id)

      machine = await Machine.create(machine, trx)
      if(machinePhotos) await machine.machinePhotos().createMany(machinePhotos, trx)

      trx.commit()
      machine.machinePhotos = await machine.machinePhotos().fetch()
      return response.status(201).json(machine)
    } catch (e) {
      trx.rollback()
      return response.status(500).send(e)
    }
  }

  async show({ request, response }) {
    const { id, provider_id } = request.params
    const machine = await Machine.query().where({ id, provider_id }).first()

    if(!machine) return response.status(404).send({})
    return machine
  }

  async update({ request, response }) {
    const trx = await Database.beginTransaction()
    try {

      let machine = request.all()
      let photos = machine.machinePhotos || []
      let photosToUpdate = photos.filter(p => p.id)
      let newPhotos = photos.filter(p => !p.id)
      let photosIds = photosToUpdate.map(p => p.id)

      delete machine.machinePhotos

      let machineInDatabase = await Machine.find(machine.id)

      if(!machineInDatabase || machineInDatabase.provider_id != machine.provider_id)
        return response.status(404).send({})

      machineInDatabase.merge(machine)
      await machineInDatabase.save(trx)

      for(let i = 0; i < photosToUpdate.length; i++) {
        await
          MachinePhoto
            .query()
            .where('id', photosToUpdate[i].id)
            .transacting(trx)
            .update(photosToUpdate[i])
      }

      newPhotos = await machineInDatabase
        .machinePhotos()
        .createMany(newPhotos, trx)
      photosIds = photosIds.concat( newPhotos.map(p => p.id) )

      await
        MachinePhoto
          .query()
          .where('machine_id', machine.id)
          .whereNotIn('id', photosIds)
          .transacting(trx)
          .delete()

      trx.commit()
      return await Machine.query().where('id', machine.id).with('machinePhotos').first()
    } catch(e) {
      trx.rollback()
      return response.status(500).send(e)
    }
  }

  async destroy({ request }) {
    const { id } = request.params
    const machine = await Machine.findOrFail(id)

    await machine.delete()
  }

}

module.exports = ProviderMachineController

'use strict'

const MachineType = use('App/Models/MachineType')

class MachineTypeController {

  async index({ request }) {
    let { all, page, perPage } = request.get()
    
    const categoryId = Number(request.params.category_id)

    page = page || 1
    perPage = perPage || 5

    if(all) {
      return { data: await MachineType.query().where('machineCategory_id', categoryId) }
    } else {
      return await MachineType.query().where('machineCategory_id', categoryId).paginate(page, perPage)
    }
  }

}

module.exports = MachineTypeController

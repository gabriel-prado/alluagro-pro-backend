'use strict'

const Farm = use('App/Models/Farm')
const Plot = use('App/Models/Plot')
const Database = use('Database')

class FarmController {

  async index({ request }) {
    let { all, page, perPage } = request.get()

    const company_id = Number(request.params.company_id)

    page = page || 1
    perPage = perPage || 5

    if(all) {
      return { data: await Farm.query().where('company_id', company_id) }
    } else {
      return await Farm.query().where('company_id', company_id).paginate(page, perPage)
    }
  }

  async store({ request, response }) {
    const trx = await Database.beginTransaction()
    try {
      let farm = request.all()
      let plots = farm.plots
      delete farm.plots

      farm.company_id = Number(request.params.company_id)

      farm = await Farm.create(farm, trx)
      if(plots) await farm.plots().createMany(plots, trx)

      trx.commit()
      farm.plots = await farm.plots().fetch()
      return response.status(201).json(farm)
    } catch(e) {
      trx.rollback()
      return response.status(500).send(e)
    }
  }

  async show({ request, response }) {
    const { id } = request.params
    const farm = await Farm.find(id)
    if(farm) {
        return farm
    } else {
        response.status(404).send({})
    }
  }

  async update({ request, response }) {
    const trx = await Database.beginTransaction()
    try {
      let farmAttributes = request.all()
      let plots = farmAttributes.plots || []
      delete farmAttributes.plots

      let farm = await Farm.find(farmAttributes.id)

      if(farm) {
        farm.merge(farmAttributes)
        await farm.save(trx)

        if(plots.length) {
          let plotsIds = []
          let newPlots = []

          for (let i = 0; i < plots.length; i++) {
            if(plots[i].id) {
              let plot = await Plot.find(plots[i].id)
              if(plot) {
                plot.merge(plots[i])
                await plot.save(trx)
                plotsIds.push(plots[i].id)
              } else {
                let err = new Error()
                err.code = 'ERR_PLOT_NOT_FOUND'
                throw err
              }
            }
            else {
              newPlots.push(plots[i])
            }
          }

          newPlots = await farm
            .plots()
            .createMany(newPlots, trx)

          plotsIds = plotsIds.concat( newPlots.map(p => p.id) )

          await Plot
            .query()
            .where('farm_id', farm.id)
            .whereNotIn('id', plotsIds)
            .transacting(trx)
            .delete()
        } else {
          await Plot
            .query()
            .where('farm_id', farm.id)
            .transacting(trx)
            .delete()
        }

        trx.commit()
        return await Farm.query().where('id', farm.id).with('plots').first()
      }
      else return response.status(404).send({})
    } catch(e) {
      trx.rollback()
      if(e.code === 'ERR_PLOT_NOT_FOUND') return response.status(400).send(e)
      throw e
    }
  }

  async destroy({ request }) {
    const { id } = request.params
    const farm = await Farm.findOrFail(id)

    await farm.delete()
  }

}

module.exports = FarmController

'use strict'

const sgMail = require('@sendgrid/mail')
const User = use('App/Models/User')
const moment = use('moment')
sgMail.setApiKey(process.env.SENDGRID_API_KEY)

class RecoverPasswordController {

  async generateCode({ request, response }) {
    const { email } = request.all()
    const verification_code = moment().format('x')
    const user = await User.findBy('email', email)

    if(!user) return

    const msg = {
      to: email,
      from: process.env.DEFAULT_MAIL,
      subject: 'Alluagro - recuperar senha',
      templateId: process.env.CODE_CONFIRM_TEMPLATE_ID,
      substitutions: {
        name: user.name,
        email: user.email,
        code: verification_code
      }
    }

    try {
      user.merge({ verification_code })
      await user.save()
      await sgMail.send(msg)
      return
    } catch(e) {
      return response.status(500).send(e)
    }
  }

  async changePassword({ request, response, auth }) {
    const { code, newPassword } = request.all()
    const user = await User.findByOrFail('verification_code', code)

    user.merge({
      password: newPassword,
      verification_code: null
    })

    await user.save()

    let { token } = await auth.attempt(user.email, newPassword)

    return { token: `Bearer ${token}` }
  }

}

module.exports = RecoverPasswordController

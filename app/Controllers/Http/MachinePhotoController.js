'use strict'

const Machine = use ('App/Models/Machine')

class MachinePhotoController {

  async index({ request, response }) {
    let { all, page, perPage } = request.get()

    const machine_id = Number(request.params.machine_id)

    page = page || 1
    perPage = perPage || 5

    let machine = await Machine.find(machine_id)

    if(machine) {
      if(all) {
        return { data: await machine.machinePhotos().fetch() }
      } else {
        return await machine.machinePhotos().paginate(page, perPage)
      }
    } else {
      return response.status(404).send({})
    }
  }

}

module.exports = MachinePhotoController

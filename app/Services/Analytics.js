'use strict'

const moment = require('moment')
const Twix = require('twix')

const Demand = use('App/Models/Demand')
const Machine = use('App/Models/Machine')
const Report = use('App/Models/Report')

class AnalyticsService {

  static get inject() {
    return ['Services/Demand']
  }

  constructor(DemandService) {
    moment.locale('pt_BR')
    this.demandService = DemandService
  }

  async machineGeneralStats(demandId, machineId) {
    let reports = (await this._getReports(demandId, machineId, null, null)).toJSON()
    let losses = {
      platform: (r) => [r.platform_losses_1, r.platform_losses_2, r.platform_losses_3, r.platform_losses_4],
      chopper: (r) => [r.chopper_losses_1, r.chopper_losses_2, r.chopper_losses_3, r.chopper_losses_4]
    }

    reports.map(r => {
      r.average_platform_losses = this._calculateAverageLoss(losses.platform(r))
      r.average_chopper_losses = this._calculateAverageLoss(losses.chopper(r))
      return r
    })

    const consumption = this._sum(reports, 'fuel')
    const workedHours = this._sum(reports, 'rotor_hourmeter')
    const onHours = this._sum(reports, 'engine_hourmeter')
    const harvestedHectares = this._sum(reports, 'hectares_harvested')

    let consumptionPerHectares = null
    if(consumption != 0 && harvestedHectares != 0) {
      consumptionPerHectares = Number((consumption / harvestedHectares).toFixed(2))
    }

    let consumptionPerWorkedHours = null
    if(consumption != 0 && workedHours != 0) {
      consumptionPerWorkedHours = Number((consumption / workedHours).toFixed(2))
    }

    return {
      harvested_hectares: harvestedHectares,
      worked_hours: workedHours,
      consumption_per_hectares: consumptionPerHectares,
      chopper_losses_average: (reports.length) ? Number((this._sum(reports, 'average_chopper_losses') / reports.length).toFixed(2)) : null,
      on_hours: onHours,
      consumption_per_worked_hours: consumptionPerWorkedHours,
      platform_losses_average: (reports.length) ? Number((this._sum(reports, 'average_platform_losses') / reports.length).toFixed(2)) : null
    }
  }

  async harvestedHectares(demandId, machineId, startDate, endDate, considerDelay = true, accumulate = false) {
    let demand = await Demand.findOrFail(demandId)
    let machine = (machineId) ? await Machine.findOrFail(machineId) : null

    let weeks = await this._getWeeks(demand, considerDelay)
    let reports = (await this._getReports(demandId, machineId, startDate, endDate)).toJSON()

    if (startDate && endDate) {
      return await this._sumReportsByWeekDay(weeks, reports, startDate, endDate, 'hectares_harvested', accumulate)
    } else {
      return await this._sumReportsByWeek(weeks, reports, 'hectares_harvested', accumulate)
    }

    throw new Error('INVALID_GROUP_STRATEGY')
  }

  async averageLoss(type, demandId, machineId, startDate, endDate) {
    let demand = await Demand.findOrFail(demandId)
    let machine = (machineId) ? await Machine.findOrFail(machineId) : null

    let weeks = await this._getWeeks(demand)
    let reports = (await this._getReports(demand.id, machineId, startDate, endDate)).toJSON()
    
    let losses = {
      platform: (r) => [r.platform_losses_1, r.platform_losses_2, r.platform_losses_3, r.platform_losses_4],
      chopper: (r) => [r.chopper_losses_1, r.chopper_losses_2, r.chopper_losses_3, r.chopper_losses_4]
    }

    if(!losses[type]) throw new Error('INVALID_LOSS_TYPE')

    reports.map(r => {
      let loss = losses[type](r)
      r.average_loss = this._calculateAverageLoss(loss)
      return r
    })

    if(!startDate && !endDate) {      
      return this._groupByWeek(weeks, (start, end) => {
        let iterator = (new Twix(start, end)).iterate('days')
        let daysAverage = []

        while(iterator.hasNext()) {
          let date = iterator.next()
          let dayReports = reports.filter(r => moment(r.date).isSame(date, 'day'))
          let lossSum = this._sum(dayReports, 'average_loss')
          if(dayReports.length) daysAverage.push(Number((lossSum / dayReports.length).toFixed(2)))
        }

        if(!daysAverage.length) return 0
        return Number((this._sum(daysAverage) / daysAverage.length).toFixed(2))
      })
    } else if (startDate && endDate) {
      return this._groupByWeekDay(weeks, startDate, endDate, (date) => {
        let dayReports = reports.filter(r => moment(r.date).isSame(date, 'day'))
        let lossSum = this._sum(dayReports, 'average_loss')
        return (dayReports.length) ? Number((lossSum / dayReports.length).toFixed(2)) : 0
      })
    }

    throw new Error('INVALID_GROUP_STRATEGY')
  }

  async burnup(demandId) {
    let demand = await Demand.findOrFail(demandId)
    let weeks = await this._getWeeks(demand, false)

    let expectedHectaresPerWeek = (weeks.length) ? Number((demand.hectares / weeks.length).toFixed(2)) : 0

    return this._groupByWeek(weeks, (start, end) => {
      return expectedHectaresPerWeek
    }, true)
  }

  async percentageHarvested(demandId) {
    let demand = await Demand.findOrFail(demandId)
    let totalValue = demand.hectares || 0

    let value = await Report.query().where('demand_id', demand.id).getSum('hectares_harvested') || 0

    return { value, totalValue }
  }

  async hourMeter(demandId, machineId, startDate, endDate) {
    const demand = await Demand.findOrFail(demandId)
    const weeks = await this._getWeeks(demand, true)

    const reports = (await this._getReports(demandId, machineId, startDate, endDate)).toJSON()

    const on = { name: 'Horas ligada' }
    const worked = { name: 'Horas trabalhadas' }

    if(startDate && endDate) {
      on.series = await this._sumReportsByWeekDay(weeks, reports, startDate, endDate, 'engine_hourmeter', false)
      worked.series = await this._sumReportsByWeekDay(weeks, reports, startDate, endDate, 'rotor_hourmeter', false)      
    } else {
      on.series = await this._sumReportsByWeek(weeks, reports, 'engine_hourmeter', false)
      worked.series = await this._sumReportsByWeek(weeks, reports, 'rotor_hourmeter', false)
    }

    return [ on, worked ]
  }

  async consumption(demand, machineId, startDate, endDate) {
    const weeks = await this._getWeeks(demand, true)
    const reports = (await this._getReports(demand.id, machineId, startDate, endDate)).toJSON()

    let consumption = null
    if(startDate && endDate) {
      consumption = await this._sumReportsByWeekDay(weeks, reports, startDate, endDate, 'fuel', false)
    } else {
      consumption = await this._sumReportsByWeek(weeks, reports, 'fuel', false)
    }

    return consumption
  }

  async consumptionPerHarvestedHectares(demand, machineId, startDate, endDate) {
    return await this._consumptionPerIndicator(demand, machineId, 'HARVESTED_HECTARES', startDate, endDate)
  }

  async consumptionPerWorkedHours(demand, machineId, startDate, endDate) {
    return await this._consumptionPerIndicator(demand, machineId, 'WORKED_HOURS', startDate, endDate)
  }

  async _consumptionPerIndicator(demand, machineId, indicator, startDate, endDate) {
    let consumption = await this.consumption(demand, machineId, startDate, endDate)
    consumption = this._toObject(consumption)

    let dividers = null
    if(indicator === 'HARVESTED_HECTARES') {
      let harvestedHectares = await this.harvestedHectares(demand.id, machineId, startDate, endDate)
      dividers = this._toObject(harvestedHectares)
  
    } else if (indicator === 'WORKED_HOURS') {
      let hourMeter = await this.hourMeter(demand.id, machineId, startDate, endDate)
      dividers = this._toObject(hourMeter[1].series)  
    }

    if(startDate && endDate) {
      const days = ['DOM', 'SEG', 'TER', 'QUA', 'QUI', 'SEX', 'SÁB']
      return this._generateConsumptionResults(days, consumption, dividers)
    } else {
      let weeks = await this._getWeeks(demand)
      weeks = weeks.map((w) => { return w.label })
      return this._generateConsumptionResults(weeks, consumption, dividers)
    }
  }

  _generateConsumptionResults(keys, consumption, dividers) {
    const result = new Array()
    keys.forEach((key) => {
      const fuel = consumption[key]
      const divider = dividers[key]

      let value = 0
      if(fuel !== 0 && divider !== 0) {
        value = fuel/divider
        value = value.toFixed(2)
      }

      result.push({ name: key, value })
    })
    return result
  }

  async _sumReportsByWeekDay(weeks, reports, startDate, endDate, property, accumulate) {
    const calculator = (date) => {
      return this._sum(reports.filter(r => moment(r.date).isSame(date)), property)
    }
    return this._groupByWeekDay(weeks, startDate, endDate, calculator, accumulate)
  }

  async _sumReportsByWeek(weeks, reports, property, accumulate) {
    const calculator = (start, end) => {
      return this._sum(reports.filter(r => moment(r.date).isBetween(start, end, null, '[]')), property)
    }
    return this._groupByWeek(weeks, calculator, accumulate)
  }

  _groupByWeek(weeks, calculator, accumulate = false) {
    let results = []
    let accumulator = 0
    for(let i = 0; i < weeks.length; i++) {
      let name = weeks[i].label
      let value = calculator(weeks[i].date.start, weeks[i].date.end)
      if(accumulate) {
        accumulator += value
        results.push({ name, value: accumulator })
      } else {
        results.push({ name, value })
      }
    }

    return results
  }

  _groupByWeekDay(weeks, startDate, endDate, calculator, accumulate = false) {
    let results = []
    let week = weeks.find(w => w.date.start.isSame(startDate, 'day') && w.date.end.isSame(endDate, 'day'))
    if(!week) throw new Error('INVALID_DATE_RANGE')

    let interval = new Twix(week.date.start, week.date.end)
    let iterator = interval.iterate('days')
    let accumulator = 0

    while(iterator.hasNext()) {
      let date = iterator.next()
      let name = date.format('ddd').toUpperCase()
      let value = calculator(date)

      if(accumulate) {
        accumulator += value
        results.push({ name, value: accumulator })
      } else {
        results.push({ name, value })
      }
    }

    return results
  }

  async _getWeeks(demand, considerDelay = true) {
    let weeks = await this.demandService.weeks(demand, considerDelay)
    weeks.forEach(w => {
      w.date.start = moment(w.date.start)
      w.date.end = moment(w.date.end)
    })

    return weeks
  }
  
  _getReports(demandId, machineId, startDate, endDate) {
    let query = Report.query().where('demand_id', demandId)
    
    if(machineId) query.where('machine_id', machineId)
    if(startDate) query.where('date', '>=', startDate)
    if(endDate) query.where('date', '<=', endDate)

    query.orderBy('date', 'asc')

    return query.fetch()
  }

  _getOccurrences(arr) {
    return arr.filter(i => (i !== null && i !== undefined && i !== NaN)).length
  }

  _calculateAverageLoss(losses) {
    let occurrences = this._getOccurrences(losses)
    return (occurrences) ? Number((this._sum(losses) / occurrences).toFixed(2)) : 0
  }

  _sum(arr, property) {
    if(property)
      return arr.reduce((prev, curr) => prev + curr[property], 0)
    return arr.reduce((prev, curr) => prev + curr, 0)
  }

  _toObject(array) {
    const data = {}
    array.forEach((i) => {
        data[i.name] = i.value
    })
    return data
  }

}

module.exports = AnalyticsService
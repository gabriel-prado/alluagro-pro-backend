const moment = require('moment')
const Report = use('App/Models/Report')
const Demand = use('App/Models/Demand')
const Drive = use('Drive')
const AWS = require('aws-sdk')
const Env = use('Env')

class DemandService {
  static get inject() {
    return ['App/Firebase']
  }

  constructor(FirebaseApp) {
    this.firebaseApp = FirebaseApp
  }

  async fetchMyDemands(user) {
    if (user.role === 'ADMIN') {
      let demands = await Demand.all()
      return demands.rows
    }
    if (user.role === 'MONITOR') return await Demand.query().where('monitor_id', user.id)
    if (user.role === 'MANAGER') return await Demand.query().where('company_id', user.company_id)
  }

  async weeks(demand, considerDelay = true) {
    demand.start_date = moment(demand.start_date)
    demand.end_date = moment(demand.end_date)

    let { startDate, endDate } = await this._dateRange(demand, considerDelay)

    let iterator = {
      start: moment(startDate),
      end: moment(startDate).add(1, 'week').subtract(1, 'day').endOf('day')
    }

    let response = []
    let numberOfWeeks = endDate.diff(startDate, 'week') + 1

    for(let w = 1; w <= numberOfWeeks; w++) {
      response.push({
        label: `Semana ${w}`,
        date: {
          start: iterator.start.format('YYYY-MM-DD'),
          end: iterator.end.format('YYYY-MM-DD')
        }
      })

      iterator.start = iterator.start.add(1, 'week')
      iterator.end = iterator.end.add(1, 'week')
    }

    return response
  }

  async _dateRange(demand, considerDelay) {

    let reportsDateRange = await this._reportsDateRange(demand)
    let startDate = moment(demand.start_date)
    let endDate = moment(demand.end_date)

    if(considerDelay && reportsDateRange.first && reportsDateRange.first.isBefore(startDate)) {
      startDate = reportsDateRange.first
    }

    if(considerDelay && reportsDateRange.last && reportsDateRange.last.isAfter(endDate)) {
      endDate = reportsDateRange.last
    }

    startDate = startDate.startOf('week').startOf('day')
    endDate = endDate.endOf('week').endOf('day')

    return { startDate, endDate }
  }

  async _reportsDateRange(demand) {
    let dates = await
      Report
        .query()
        .max('date as last')
        .min('date as first')
        .where('demand_id', demand.id)
        .first()

    dates.first = dates.first ? moment(dates.first) : null
    dates.last = dates.first ? moment(dates.last) : null

    return dates
  }

  async deleteMessages(demandId) {
    let collection = this.firebaseApp.firestore().collection('chat')
      .doc('message').collection(`demand_${demandId}`)

    let documents = await collection.get()

    if (documents.size) {
      let batch = this.firebaseApp.firestore().batch()

      documents.docs.forEach(doc => batch.delete(doc.ref))

      return batch.commit()
    }
  }

  async addContract(demandId, file) {
    let demand = await Demand.findOrFail(demandId)
    let contracts = demand.toJSON().contracts
    let fileName = file.filename

    await Drive.disk('s3').put(`${demandId}/${fileName}`, file, { Key: fileName, ContentType: file.headers['content-type']})

    contracts.push(fileName)
    demand.merge({ contracts })
    await demand.save()

    return fileName
  }

  async findContract(demandId, fileName) {
    return await Drive.disk('s3').getObject(`${demandId}/${fileName}`)
  }

  async deleteContract(demandId, fileName) {
    return await Drive.disk('s3').delete(`${demandId}/${fileName}`)
  }

  async deleteContractFolder(demandId) {
    let params = {
      Bucket: Env.get('S3_BUCKET'),
      Prefix: `${demandId}/`
    }
    let s3 = new AWS.S3({
      accessKeyId: Env.get('S3_KEY'),
      secretAccessKey: Env.get('S3_SECRET'),
      region: Env.get('S3_REGION')
    })

    params = await _listObjects(s3, params)
    if(params) await _deleteObjects(s3, params)
  }
}

const _listObjects = (s3, params) => {
  return new Promise(async (res, rej) => {
    await s3.listObjects(params, (err, data) => {
      if (err) rej(err)
      if (data.Contents.length == 0) res(false)

      delete params.Prefix
      params.Delete = {Objects:[]}

      data.Contents.forEach((content) => {
        params.Delete.Objects.push({Key: content.Key})
      })

      res(params)
    })
  })
}

const _deleteObjects = (s3, params) => {
  return new Promise(async (res, rej) => {
    await s3.deleteObjects(params, (err, data) => {
      if (err) rej(err)
      else res()
    })
  })
}

module.exports = DemandService

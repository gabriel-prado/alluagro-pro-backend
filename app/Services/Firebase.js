class FirebaseService {
  static get inject() {
    return ['Services/Demand', 'App/Firebase']
  }

  constructor(DemandService, FirebaseApp) {
    this.demandService = DemandService
    this.firebaseApp = FirebaseApp
  }

  async generateToken(user) {
    let demands = await this.demandService.fetchMyDemands(user)
    let tokenData = {}

    demands.forEach((d) => tokenData[`demand_${d.id}`] = true)
    return { token: await this.firebaseApp.auth().createCustomToken(String(user.id), tokenData) }
  }
}

module.exports = FirebaseService

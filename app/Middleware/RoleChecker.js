'use strict'

class RoleChecker {
  async handle ({ response, auth }, next, roles) {
    return ( auth.user && roles.includes(auth.user.role) ) ? await next() : response.status(401).send({})
  }
}

module.exports = RoleChecker

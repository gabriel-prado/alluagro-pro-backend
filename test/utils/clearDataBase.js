const Database = use('Database')

module.exports = async () => {

  await Database.transaction(async (trx) => {
    await trx.raw('SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;')
    await trx.raw('SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;')
    await trx.raw('SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE="TRADITIONAL,ALLOW_INVALID_DATES";')

    await trx.truncate('users')
    await trx.truncate('companies')
    await trx.truncate('farms')
    await trx.truncate('plots')
    await trx.truncate('providers')
    await trx.truncate('machine_categories')
    await trx.truncate('machine_types')
    await trx.truncate('machines')
    await trx.truncate('machine_photos')
    await trx.truncate('demands')
    await trx.truncate('demand_machine')
    await trx.truncate('reports')
    await trx.truncate('demand_observations')

    await trx.raw('SET SQL_MODE=@OLD_SQL_MODE;')
    await trx.raw('SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;')
    await trx.raw('SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;')
  })

}

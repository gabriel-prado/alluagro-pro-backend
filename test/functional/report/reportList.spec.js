'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const Report = use('App/Models/Report')
const { test, trait, beforeEach } = use('Test/Suite')('Report - GET /demand/id/machine/id/report')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultMonitor = null
let defaultCompany = null
let defaultFarm = null
let defaultProvider = null
let defaultMachineType = null
let defaultMachineCategory = null
let defaultMachine = null
let defaultPlot = null
let defaultDemandMachine = null
let defaultDemand = null
let defaultReports = null

beforeEach(async () => {
  await clearDataBase()
  defaultMonitor = await Factory.model('App/Models/User').create({
    role: 'MONITOR'
  })
  defaultCompany = await Factory.model('App/Models/Company').create()
  defaultFarm = await Factory.model('App/Models/Farm').create({
    company_id: defaultCompany.id
  })
  defaultProvider = await Factory.model('App/Models/Provider').create()
  defaultMachineCategory = await Factory.model('App/Models/MachineCategory').create()
  defaultMachineType = await Factory.model('App/Models/MachineType').create({
    machineCategory_id: defaultMachineCategory.id
  })
  defaultMachine = await Factory.model('App/Models/Machine').create({
    machineCategory_id: defaultMachineCategory.id,
    machineType_id: defaultMachineType.id,
    provider_id: defaultProvider.id
  })
  defaultPlot = await Factory.model('App/Models/Plot').create({
    farm_id: defaultFarm.id
  })
  defaultDemand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: defaultMonitor.id
  })
  defaultDemandMachine = await Factory.model('App/Models/DemandMachine').create({
    demand_id: defaultDemand.id,
    machine_id: defaultMachine.id
  })
  defaultReports = await Factory.model('App/Models/Report').createMany(10, {
    machine_id: defaultMachine.id,
    plot_id: defaultPlot.id,
    demand_id: defaultDemand.id,
    date: '2017-01-04'
  })
})

test('should retrieve reports paginated', async({ client, assert }) => {
  const response = await client.get(`demand/${defaultDemand.id}/machine/${defaultMachine.id}/report`).query({ page: 4, perPage: 3, date: '2017-01-04' }).loginVia(defaultMonitor).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.page, 4)
  assert.lengthOf(response.body.data, 1)
  assert.equal(response.body.data[0].date, '2017-01-04')
})

test('should retrieve reports with default pagination', async({ client, assert }) => {
  const response = await client.get(`demand/${defaultDemand.id}/machine/${defaultMachine.id}/report`).query({ date: '2017-01-04' }).loginVia(defaultMonitor).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.page, 1)
  assert.lengthOf(response.body.data, 5)
  assert.equal(response.body.data[0].date, '2017-01-04')
})

test('should retrieve all reports', async({ client, assert }) => {
  const response = await client.get(`demand/${defaultDemand.id}/machine/${defaultMachine.id}/report`).query({ all: true, date: '2017-01-04' }).loginVia(defaultMonitor).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 10)
})

test('should get a forbidden to incorrect monitor', async({ client, assert}) => {
  let monitor = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.get(`demand/${defaultDemand.id}/machine/${defaultMachine.id}/report`).loginVia(monitor).end()

  assert.equal(response.status, 403)
})

test('should return no report for wrong date', async({ client, assert }) => {
  const response = await client.get(`demand/${defaultDemand.id}/machine/${defaultMachine.id}/report`).query({ all: true, date: '2014-01-04' }).loginVia(defaultMonitor).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 0)
})

test('should not list reports for not logged user', async({ client, assert }) => {
  const response = await client.get(`demand/${defaultDemand.id}/machine/${defaultMachine.id}/report`).end()

  assert.equal(response.status, 401)
})

test('should get a forbidden to incorrect manager', async({ client, assert}) => {
  const managerUser = await Factory.model('App/Models/User').create({
    role: 'MANAGER'
  })

  const response = await
    client
      .get(`demand/${defaultDemand.id}/machine/${defaultMachine.id}/report`)
      .loginVia(managerUser)
      .end()

  assert.equal(response.status, 403)
})

test('should retrieve reports to correct manager', async({ client, assert}) => {
  const managerUser = await Factory.model('App/Models/User').create({
    role: 'MANAGER',
    company_id: defaultCompany.id
  })

  const response = await
    client
      .get(`demand/${defaultDemand.id}/machine/${defaultMachine.id}/report`)
      .query({ all: true, date: '2017-01-04' })
      .loginVia(managerUser)
      .end()
  
  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 10)
})

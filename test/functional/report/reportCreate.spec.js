'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const Report = use('App/Models/Report')
const { test, trait, beforeEach } = use('Test/Suite')('Report - POST /demand/:demand_id/machine/:machine_id/report')
const ROUTE = '/demand/:demand_id/machine/:machine_id/report'

trait('Test/ApiClient')
trait('Auth/Client')

let defaultMonitor = null
let defaultCompany = null
let defaultFarm = null
let defaultProvider = null
let defaultMachineType = null
let defaultMachineCategory = null
let defaultMachine = null
let defaultPlot = null
let defaultDemandMachine = null
let defaultDemand = null

beforeEach(async () => {
  await clearDataBase()
  defaultMonitor = await Factory.model('App/Models/User').create({
    role: 'MONITOR'
  })
  defaultCompany = await Factory.model('App/Models/Company').create()
  defaultFarm = await Factory.model('App/Models/Farm').create({
    company_id: defaultCompany.id
  })
  defaultProvider = await Factory.model('App/Models/Provider').create()
  defaultMachineCategory = await Factory.model('App/Models/MachineCategory').create()
  defaultMachineType = await Factory.model('App/Models/MachineType').create({
    machineCategory_id: defaultMachineCategory.id
  })
  defaultMachine = await Factory.model('App/Models/Machine').create({
    machineCategory_id: defaultMachineCategory.id,
    machineType_id: defaultMachineType.id,
    provider_id: defaultProvider.id
  })
  defaultPlot = await Factory.model('App/Models/Plot').create({
    farm_id: defaultFarm.id
  })
  defaultDemand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: defaultMonitor.id
  })
  defaultDemandMachine = await Factory.model('App/Models/DemandMachine').create({
    demand_id: defaultDemand.id,
    machine_id: defaultMachine.id
  })
})

test('should save report and return 200', async ({ client, assert }) => {
  const route = ROUTE.replace(':demand_id', defaultDemand.id)
                    .replace(':machine_id', defaultMachine.id)

  const reports = await Factory.model('App/Models/Report').makeMany(3, {
    machine_id: defaultMachine.id,
    plot_id: defaultPlot.id,
    demand_id: defaultDemand.id,
    date: '2018-10-10'
  })

  let reportsJson = {
    reports: reports.map(r => r.toJSON()),
    date: '2018-10-10'
  }

  const response = await client.post(route).loginVia(defaultMonitor).send(reportsJson).end()

  assert.equal(response.status, 200)
  const reportInDatabase = await Report.query().where({machine_id: defaultMachine.id, demand_id: defaultDemand.id, date: '2018-10-10'})
  assert.lengthOf(reportInDatabase, 3)
})

test('should try save report and return 401', async ({ client, assert }) => {
  const route = ROUTE.replace(':demand_id', defaultDemand.id)
                    .replace(':machine_id', defaultMachine.id)

  const response = await client.post(route).end()

  assert.equal(response.status, 401)
})

test('should try save report and return 400', async ({ client, assert }) => {
  const route = ROUTE.replace(':demand_id', defaultDemand.id)
                    .replace(':machine_id', defaultMachine.id)

  let reportsJson = {
    reports: []
  }

  const response = await client.post(route).loginVia(defaultMonitor).send(reportsJson).end()

  assert.equal(response.status, 400)
})

test('should save and delete reports', async ({ client, assert }) => {
  const route = ROUTE.replace(':demand_id', defaultDemand.id)
                    .replace(':machine_id', defaultMachine.id)

  const reports = await Factory.model('App/Models/Report').makeMany(3, {
    machine_id: defaultMachine.id,
    plot_id: defaultPlot.id,
    demand_id: defaultDemand.id,
    date: '2018-10-10'
  })

  let reportsJson = {
    reports: reports.map(r => r.toJSON()),
    date: '2018-10-10'
  }

  let response = await client.post(route).loginVia(defaultMonitor).send(reportsJson).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 3)

  response = await client.post(route).loginVia(defaultMonitor).send({reports: [], date: '2018-10-10'}).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 0)
})

test('should get a forbidden to incorrect monitor', async ({ client, assert }) => {
  let reportsJson = { reports: [], date: '2018-10-10' }
  const monitor = await Factory.model('App/Models/User').create({
    role: 'MONITOR'
  })
  const route = ROUTE.replace(':demand_id', defaultDemand.id)
                    .replace(':machine_id', monitor.id)

  const response = await client.post(route).send(reportsJson).loginVia(monitor).end()
  
  assert.equal(response.status, 403)
})

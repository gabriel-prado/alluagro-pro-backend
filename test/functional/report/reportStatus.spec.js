'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const Report = use('App/Models/Report')
const { test, trait, beforeEach } = use('Test/Suite')('Report - GET /report?date')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultMonitor = null
let defaultCompany = null
let defaultFarm = null
let defaultProvider = null
let defaultMachineType = null
let defaultMachineCategory = null
let defaultMachine = null
let defaultPlot = null
let defaultDemandMachine = null
let defaultDemand = null
let defaultMachinePhotos = null

beforeEach(async () => {
  await clearDataBase()

  defaultMonitor = await Factory.model('App/Models/User').create({
    role: 'MONITOR'
  })

  defaultCompany = await Factory.model('App/Models/Company').create()

  defaultFarm = await Factory.model('App/Models/Farm').create({
    company_id: defaultCompany.id
  })

  defaultProvider = await Factory.model('App/Models/Provider').create()

  defaultMachineCategory = await Factory.model('App/Models/MachineCategory').create()

  defaultMachineType = await Factory.model('App/Models/MachineType').create({
    machineCategory_id: defaultMachineCategory.id
  })

  defaultMachine = await Factory.model('App/Models/Machine').create({
    machineCategory_id: defaultMachineCategory.id,
    machineType_id: defaultMachineType.id,
    provider_id: defaultProvider.id
  })

  defaultPlot = await Factory.model('App/Models/Plot').create({
    farm_id: defaultFarm.id
  })

  defaultDemand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: defaultMonitor.id
  })

  defaultDemandMachine = await Factory.model('App/Models/DemandMachine').create({
    demand_id: defaultDemand.id,
    machine_id: defaultMachine.id
  })

  defaultMachinePhotos = await Factory.model('App/Models/MachinePhoto').createMany(3, {
    machine_id: defaultMachine.id
  })
})

test('should get demand, machine, status and return 200', async ({ client, assert }) => {
  const reports = await Factory.model('App/Models/Report').createMany(2, {
    machine_id: defaultMachine.id,
    plot_id: defaultPlot.id,
    demand_id: defaultDemand.id,
    date: '2017-01-30'
  })

  const response = await client.get('/report').loginVia(defaultMonitor).query({ date: '2017-01-30' }).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.machines, 1)
  assert.containsAllKeys(response.body.machines[0], ['id', 'machine_code', 'status', 'provider', 'machinePhotos', 'created_at', 'updated_at'])
  assert.equal(response.body.machines[0].status, 'COMPLETED')
  assert.lengthOf(response.body.machines[0].machinePhotos, 3)
})

test('should try get demand but monitor dont have demand and return 204', async ({ client, assert }) => {
  let monitor = await Factory.model('App/Models/User').create({
    role: 'MONITOR'
  })

  const response = await client.get('/report').loginVia(monitor).query({ date: '2017-01-30' }).end()

  assert.equal(response.status, 204)
})

test('should try get demand, machine, status and return 401', async ({ client, assert }) => {
  const response = await client.get('/report').end()

  assert.equal(response.status, 401)
})

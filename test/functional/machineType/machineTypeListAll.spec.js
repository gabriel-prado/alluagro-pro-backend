'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Machine Type - GET /machine-category/id/machine-type')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultMachineCategory = null
let defaultResourceUrl = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultMachineCategory = await Factory.model('App/Models/MachineCategory').create()
  defaultResourceUrl = `machine-category/${defaultMachineCategory.id}/machine-type`
})

test('should not list types for not logged user', async ({ client, assert }) => {
  const response = await client.get(defaultResourceUrl).end()
  assert.equal(response.status, 401)
})

test('should list types with default pagination', async({ client, assert }) => {
  const types = await Factory.model('App/Models/MachineType').createMany(6, {
    machineCategory_id: defaultMachineCategory.id
  })

  const response = await client.get(defaultResourceUrl).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 5)
})

test('should list types with all', async({ client, assert }) => {
  const types = await Factory.model('App/Models/MachineType').createMany(6, {
    machineCategory_id: defaultMachineCategory.id
  })

  const response = await client.get(defaultResourceUrl).query({ all: true }).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 6)
})

test('should list any type of category not registered.', async({ client, assert }) => {
  const types = await Factory.model('App/Models/MachineType').createMany(2, {
    machineCategory_id: defaultMachineCategory.id
  })

  const response = await client.get('machine-category/999/machine-type').loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 0)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.get('machine-category/1/machine-type').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.get('machine-category/1/machine-type').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

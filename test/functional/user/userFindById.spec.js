'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const User = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('User - GET /user/id')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
})

test('should retrieve existent user', async({ client, assert}) => {
  const resourceUrl = `user/1`

  const response = await client.get(resourceUrl).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.include(response.body, defaultUser.toJSON())
})

test('should get 404 for a non existent user', async({ client, assert}) => {
  const resourceUrl = `user/10`

  const response = await client.get(resourceUrl).loginVia(defaultUser).end()

  assert.equal(response.status, 404)
})

test('should not get user for not logged user', async({ client, assert}) => {
  const resourceUrl = `user/1`

  const response = await client.get(resourceUrl).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.get('user/1').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.get('user/1').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const User = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('User - DELETE /user/id')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
})

test('should destroy user by id', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').create()
  const response = await client.delete(`user/${user.id}`).loginVia(defaultUser).end()

  assert.equal(response.status, 204)
  const userInDatabase =  await User.find(user.id)
  assert.isNull(userInDatabase)
})

test('should try to destroy non-existent user and return status 404', async ({ client, assert }) => {
  const response = await client.delete(`user/123`).loginVia(defaultUser).end()

  assert.equal(response.status, 404)
})

test('should try to destroy user with demand and return status 500', async ({ client, assert }) => {
  const monitor = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  let company = await Factory.model('App/Models/Company').create()
  let farm = await Factory.model('App/Models/Farm').create({
    company_id: company.id
  })
  let plot = await Factory.model('App/Models/Plot').create({
    farm_id: farm.id
  })
  let demand = await Factory.model('App/Models/Demand').create({
    company_id: company.id,
    farm_id: farm.id,
    monitor_id: monitor.id
  })

  const response = await client.delete(`user/${monitor.id}`).loginVia(defaultUser).end()

  assert.equal(response.status, 500)
  assert.equal(response.body.code, 'ER_ROW_IS_REFERENCED')
  assert.equal(response.body.table, 'DEMANDS')
  const monitorInDatabase =  await User.find(monitor.id)
  assert.isNotNull(monitorInDatabase)
})

test('should try destroy user without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.delete(`user/12`).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with manager role and get 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.delete(`user/1`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with monitor role and get 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.delete(`user/1`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const User = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('User - GET /user')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultCompany = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultCompany = await Factory.model('App/Models/Company').create()
})

test('should list users without authenticate and retrieve 401', async ({ client, assert }) => {
  await Factory.model('App/Models/User').createMany(2, {
    company_id: defaultCompany.id
  })

  const response = await client.get('user').end()

  assert.equal(response.status, 401)
})

test('should list all users', async ({ client, assert }) => {
  await Factory.model('App/Models/User').createMany(2, {
    company_id: defaultCompany.id
  })

  const response = await client.get('user').query({ all: true }).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 3)
})

test('should list users paginated', async ({ client, assert }) => {
  await Factory.model('App/Models/User').createMany(2)

  const response = await client.get('user').query({ page: 1, perPage: 1 }).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 1)
})

test('should list all users by company', async({ client, assert }) => {
  await Factory.model('App/Models/User').createMany(1, {
    company_id: defaultCompany.id,
    role: 'MANAGER'
  })

  await Factory.model('App/Models/User').createMany(2)

  const queryParams = { all: true, company_id: defaultCompany.id }
  const response = await client.get('user').query(queryParams).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 1)
})

test('should list users paginated by company', async({ client, assert }) => {
  await Factory.model('App/Models/User').createMany(3, {
    company_id: defaultCompany.id,
    role: 'MANAGER'
  })

  await Factory.model('App/Models/User').createMany(1)

  const queryParams = { company_id: defaultCompany.id, perPage: 2, page: 2 }
  const response = await client.get('user').query(queryParams).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 1)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.get('user').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.get('user').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

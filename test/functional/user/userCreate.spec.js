'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const User = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('User - POST /user')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultCompany = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultCompany = await Factory.model('App/Models/Company').create()
})

test('should store user and retrieve 201', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').make()
  const userJson = user.toJSON()
  userJson.password = 'somePassword123'

  const response = await client.post('user').loginVia(defaultUser).send(userJson).end()

  assert.equal(response.status, 201)
  const userInDatabase = await User.find(response.body.id)
  assert.isNotNull(userInDatabase)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
})

test('should store invalid user and retrieve 400', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').make()
  let userJson = user.toJSON()
  delete userJson.email

  const response = await client.post('user').loginVia(defaultUser).send(userJson).end()

  assert.equal(response.status, 400)
})

test('should store user without authenticate and retrieve 401', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').make()
  const userJson = user.toJSON()
  userJson.password = 'somePassword123'

  const response = await client.post('user').send(userJson).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.post('user').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.post('user').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const User = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('User - PUT /user/id')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
})

test('should update user and retrieve 200', async ({ client, assert }) => {
  let userJson = defaultUser.toJSON()
  userJson.name = 'user_new_name'
  userJson.password = 'somePassword123'

  const response =
    await client
      .put(`user/${defaultUser.id}`)
      .loginVia(defaultUser)
      .send(userJson)
      .end()

  assert.equal(response.status, 200)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  assert.doesNotHaveAllKeys(response.body, ['password'])
  const userInDatabase = await User.find(response.body.id)
  assert.isNotNull(userInDatabase)
  assert.equal(userInDatabase.name, userJson.name)
})

test('should try update wrong user and retrieve 404', async ({ client, assert }) => {
  let userJson = defaultUser.toJSON()
  userJson.name = 'user_new_name'

  const response =
    await client
      .put(`user/123`)
      .loginVia(defaultUser)
      .send(userJson)
      .end()

  assert.equal(response.status, 404)
})

test('should update user without authenticate and retrieve 401', async ({ client, assert }) => {
  const response =
    await client
      .put(`user/${defaultUser.id}`)
      .end()

  assert.equal(response.status, 401)
})

test('should try update another user and retrieve 401', async ({ client, assert }) => {
  let anotherUser = await Factory.model('App/Models/User').create({
    role: 'MONITOR'
  })
  let userJson = defaultUser.toJSON()
  userJson.name = 'user_new_name'

  const response =
    await client
      .put(`user/${defaultUser.id}`)
      .loginVia(anotherUser)
      .send(userJson)
      .end()

  assert.equal(response.status, 401)
})

test('should as admin update another user and retrieve 200', async ({ client, assert }) => {
  let anotherUser = await Factory.model('App/Models/User').create({
    role: 'ADMIN'
  })
  let userJson = defaultUser.toJSON()
  userJson.name = 'user_new_name'

  const response =
    await client
      .put(`user/${defaultUser.id}`)
      .loginVia(anotherUser)
      .send(userJson)
      .end()

  assert.equal(response.status, 200)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  assert.doesNotHaveAllKeys(response.body, ['password'])
  const userInDatabase = await User.find(response.body.id)
  assert.isNotNull(userInDatabase)
  assert.equal(userInDatabase.name, userJson.name)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.put('user/1').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.put('user/1').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

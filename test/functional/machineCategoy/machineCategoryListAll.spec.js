'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Machine Category - GET /machine-category')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
})

test('should not list categories for not logged user', async ({ client, assert }) => {
  const response = await client.get('/machine-category').end()
  assert.equal(response.status, 401)
})

test('should list categories with default pagination', async ({ client, assert }) => {
  const categories = await Factory.model('App/Models/MachineCategory').createMany(6)

  const response = await client.get('/machine-category').loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 5)
})

test('should list categories with all', async ({ client, assert }) => {
  const categories = await Factory.model('App/Models/MachineCategory').createMany(6)

  const response = await client.get('/machine-category').query({ all: true }).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 6)
})

test('should list categories using pagination', async ({ client, assert }) => {
  const categories = await Factory.model('App/Models/MachineCategory').createMany(5)

  const response = await client.get('/machine-category').query({ page: 3, perPage: 2 }).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 1)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.get('/machine-category').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.get('/machine-category').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const Farm = use('App/Models/Farm')
const { test, trait, beforeEach } = use('Test/Suite')('Farm - GET /company/id/farm/id')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultCompany = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultCompany = await Factory.model('App/Models/Company').create()
})

test('should retrieve existent farm', async({ client, assert}) => {
  const farm = await Factory.model('App/Models/Farm').create()
  const resourceUrl = `company/${defaultCompany.id}/farm/1`

  const response = await client.get(resourceUrl).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.include(response.body, farm.toJSON())
})

test('should get 404 for a non existent farm', async({ client, assert}) => {
  const resourceUrl = `company/${defaultCompany.id}/farm/1`

  const response = await client.get(resourceUrl).loginVia(defaultUser).end()

  assert.equal(response.status, 404)
})

test('should not get farm for not logged user', async({ client, assert}) => {
  const resourceUrl = `company/${defaultCompany.id}/farm/1`

  const response = await client.get(resourceUrl).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.get(`company/1/farm/1`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.get(`company/1/farm/1`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

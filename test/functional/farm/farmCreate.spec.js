'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const Farm = use('App/Models/Farm')
const { test, trait, beforeEach } = use('Test/Suite')('Farm - POST /company/id/farm')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultCompany = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultCompany = await Factory.model('App/Models/Company').create()
})

test('should save farm and return 201', async ({ client, assert }) => {
  const farm = await Factory.model('App/Models/Farm').make()
  const resourceUrl = `company/${defaultCompany.id}/farm`

  const response = await client.post(resourceUrl).loginVia(defaultUser).send(farm.toJSON()).end()

  assert.equal(response.status, 201)
  const farmInDatabase =  await Farm.find(response.body.id)
  assert.isNotNull(farmInDatabase)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
})

test('should save farm with slim plots and return 201', async ({ client, assert }) => {
  const farm = await Factory.model('App/Models/Farm').make()
  const plots = await Factory.model('App/Models/Plot').makeMany(3, {
    coordinates: null,
    name: null
  })

  let farmJson = farm.toJSON()
  farmJson.plots = plots.map(p => p.toJSON())

  const response = await client.post(`company/${defaultCompany.id}/farm`).loginVia(defaultUser).send(farmJson).end()

  assert.equal(response.status, 201)
  const farmInDatabase =  await Farm.find(response.body.id)
  assert.isNotNull(farmInDatabase)
  assert.lengthOf(response.body.plots, 3)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at', 'plots'])
})

test('should try save farm and return 401', async ({ client, assert }) => {
  const farm = await Factory.model('App/Models/Farm').make()
  const resourceUrl = `company/${defaultCompany.id}/farm`

  const response = await client.post(resourceUrl).send(farm.toJSON()).end()

  assert.equal(response.status, 401)
})

test('should try save farm and return 400', async ({ client, assert }) => {
  const farm = await Factory.model('App/Models/Farm').make()
  const json = farm.toJSON()
  const resourceUrl = `company/${defaultCompany.id}/farm`

  delete json.name

  const response = await client.post(resourceUrl).loginVia(defaultUser).send(json).end()

  assert.equal(response.status, 400)
})

test('should try save farm with plots and return 201', async ({ client, assert }) => {
  const farm = await Factory.model('App/Models/Farm').make()
  const plots = await Factory.model('App/Models/Plot').makeMany(4)

  let farmJson = farm.toJSON()
  farmJson.plots = plots.map(p => p.toJSON())

  const response = await client.post(`company/${defaultCompany.id}/farm`).loginVia(defaultUser).send(farmJson).end()

  assert.equal(response.status, 201)
  const farmInDatabase =  await Farm.find(response.body.id)
  assert.isNotNull(farmInDatabase)
  assert.lengthOf(response.body.plots, 4)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at', 'plots'])
})

test('should try save farm with wrong plots and return 400', async ({ client, assert }) => {
  const farm = await Factory.model('App/Models/Farm').make()
  const plots = await Factory.model('App/Models/Plot').makeMany(4)

  let farmJson = farm.toJSON()
  farmJson.plots = plots.map(p => p.toJSON())
  delete farmJson.plots[0].code

  const response = await client.post(`company/${defaultCompany.id}/farm`).loginVia(defaultUser).send(farmJson).end()

  assert.equal(response.status, 400)
})

test('should save farm without hectares and return 201', async ({ client, assert }) => {
  const farm = await Factory.model('App/Models/Farm').make()
  const json = farm.toJSON()
  const resourceUrl = `company/${defaultCompany.id}/farm`

  delete json.hectares

  const response = await client.post(resourceUrl).loginVia(defaultUser).send(json).end()

  assert.equal(response.status, 201)
  const farmInDatabase =  await Farm.find(response.body.id)
  assert.isNotNull(farmInDatabase)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.post(`company/${defaultCompany.id}/farm`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.post(`company/${defaultCompany.id}/farm`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

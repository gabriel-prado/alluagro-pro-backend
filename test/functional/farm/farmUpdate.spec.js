'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const Farm = use('App/Models/Farm')
const Plot = use('App/Models/Plot')
const { test, trait, beforeEach } = use('Test/Suite')('Farm - PUT /company/id/farm/id')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultCompany = null
let defaultFarm = null
let defaultPlots = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultCompany = await Factory.model('App/Models/Company').create()
  defaultFarm = await Factory.model('App/Models/Farm').create({
    company_id: defaultCompany.id
  })
  defaultPlots = await Factory.model('App/Models/Plot').createMany(2, {
    farm_id: defaultFarm.id
  })
})

test('should update farm and return 200', async({ client, assert }) => {
  let farmJson = defaultFarm.toJSON()
  farmJson.name = 'farm_new_name'
  farmJson.hectares = 1234.99

  const response = await client.put(`company/${defaultCompany.id}/farm/${defaultFarm.id}`).loginVia(defaultUser).send(farmJson).end()

  assert.equal(response.status, 200)
  const farmInDatabase =  await Farm.find(response.body.id)
  assert.isNotNull(farmInDatabase)
  assert.containsAllKeys(response.body, ['id', 'name', 'hectares', 'company_id', 'created_at', 'updated_at'])
  assert.equal(farmInDatabase.name, farmJson.name)
  assert.equal(farmInDatabase.hectares, farmJson.hectares)
})

test('should try update farm and return 404', async ({ client, assert }) => {
  let farmJson = defaultFarm.toJSON()

  const response = await client.put(`company/${defaultCompany.id}/farm/777`).loginVia(defaultUser).send(farmJson).end()

  assert.equal(response.status, 404)
})

test('should not update farm for not logged user', async({ client, assert}) => {
  const response = await client.put(`company/${defaultCompany.id}/farm/${defaultFarm.id}`).end()

  assert.equal(response.status, 401)
})

test('should update farm with plots and return 200', async({ client, assert }) => {
  let farmJson = defaultFarm.toJSON()
  farmJson.name = 'farm_new_name'
  let plotJson = defaultPlots[0].toJSON()
  plotJson.name = "plot_new_name"
  farmJson.plots = [
    plotJson
  ]

  const response = await client.put(`company/${defaultCompany.id}/farm/${defaultFarm.id}`).loginVia(defaultUser).send(farmJson).end()

  assert.equal(response.status, 200)
  const farmInDatabase =  await Farm.find(response.body.id)
  assert.isNotNull(farmInDatabase)
  assert.containsAllKeys(response.body, ['id', 'name', 'hectares', 'plots', 'company_id', 'created_at', 'updated_at'])
  assert.equal(farmInDatabase.name, farmJson.name)
  const plotInDatabase =  await Plot.find(plotJson.id)
  assert.isNotNull(plotInDatabase)
  assert.containsAllKeys(response.body.plots[0], ['id', 'name', 'coordinates', 'farm_id', 'code', 'created_at', 'updated_at'])
  assert.equal(plotInDatabase.name, plotJson.name)
})

test('should update farm without plots and return 200', async({ client, assert }) => {
  let farmJson = defaultFarm.toJSON()
  farmJson.name = 'farm_new_name'
  farmJson.plots = []

  const response = await client.put(`company/${defaultCompany.id}/farm/${defaultFarm.id}`).loginVia(defaultUser).send(farmJson).end()

  assert.equal(response.status, 200)
  const farmInDatabase =  await Farm.find(response.body.id)
  assert.isNotNull(farmInDatabase)
  assert.containsAllKeys(response.body, ['id', 'name', 'hectares', 'plots', 'company_id', 'created_at', 'updated_at'])
  assert.equal(farmInDatabase.name, farmJson.name)
  const plotInDatabase =  await Plot.findBy('farm_id', farmJson.id)
  assert.isNull(plotInDatabase)
})

test('should update farm with plots and return 500', async({ client, assert }) => {
  let provider = await Factory.model('App/Models/Provider').create()
  let machineCategory = await Factory.model('App/Models/MachineCategory').create()
  let machineType = await Factory.model('App/Models/MachineType').create({
    machineCategory_id: machineCategory.id
  })
  let machine = await Factory.model('App/Models/Machine').create({
    machineCategory_id: machineCategory.id,
    machineType_id: machineType.id,
    provider_id: provider.id
  })
  let demand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: defaultUser.id
  })
  let demandMachine = await Factory.model('App/Models/DemandMachine').create({
    demand_id: demand.id,
    machine_id: machine.id
  })
  let report = await Factory.model('App/Models/Report').create({
    machine_id: machine.id,
    plot_id: defaultPlots[0].id,
    demand_id: demand.id
  })

  let farmJson = defaultFarm.toJSON()
  farmJson.plots = [
    defaultPlots[1]
  ]

  const response = await client.put(`company/${defaultCompany.id}/farm/${defaultFarm.id}`).loginVia(defaultUser).send(farmJson).end()

  assert.equal(response.status, 500)
  assert.equal(response.body.code, 'ER_ROW_IS_REFERENCED')
  assert.equal(response.body.table, 'REPORTS')
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.put(`company/1/farm/1`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.put(`company/1/farm/1`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const Farm = use('App/Models/Farm')
const { test, trait, beforeEach } = use('Test/Suite')('Farm - GET /company/id/farm')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultCompany = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultCompany = await Factory.model('App/Models/Company').create()
})

test('should retrieve farms paginated', async({ client, assert}) => {
  await Factory.model('App/Models/Farm').createMany(13, {
    company_id: defaultCompany.id
  })
  const resourceUrl = `company/${defaultCompany.id}/farm`

  const response = await client.get(resourceUrl).query({ page: 3, perPage: 5 }).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.page, 3)
  assert.lengthOf(response.body.data, 3)
})

test('should retrieve farms with default pagination', async({ client, assert}) => {
  await Factory.model('App/Models/Farm').createMany(10, {
    company_id: defaultCompany.id
  })
  const resourceUrl = `company/${defaultCompany.id}/farm`

  const response = await client.get(resourceUrl).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.page, 1)
  assert.lengthOf(response.body.data, 5)
})

test('should retrieve all farms', async({ client, assert}) => {
  await Factory.model('App/Models/Farm').createMany(10, {
    company_id: defaultCompany.id
  })
  const resourceUrl = `company/${defaultCompany.id}/farm`

  const response = await client.get(resourceUrl).query({ all: true }).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 10)
})

test('should not list farms for not logged user', async({ client, assert}) => {
  const resourceUrl = `company/${defaultCompany.id}/farm`

  const response = await client.get(resourceUrl).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.get(`company/1/farm`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.get(`company/1/farm`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

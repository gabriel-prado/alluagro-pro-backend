'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const Farm = use('App/Models/Farm')
const { test, trait, beforeEach } = use('Test/Suite')('Farm - DELETE /company/id/farm/id')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultCompany = null
let defaultFarm = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultCompany = await Factory.model('App/Models/Company').create()
  defaultFarm = await Factory.model('App/Models/Farm').create({
    company_id: defaultCompany.id
  })
})

test('should destroy farm by id', async ({ client, assert }) => {
  const response = await client.delete(`company/${defaultCompany.id}/farm/${defaultFarm.id}`).loginVia(defaultUser).end()

  assert.equal(response.status, 204)
  const farmInDatabase =  await Farm.find(defaultFarm.id)
  assert.isNull(farmInDatabase)
})

test('should try to destroy non-existent farm and return status 404', async ({ client, assert }) => {
  const response = await client.delete(`company/${defaultCompany.id}/farm/123`).loginVia(defaultUser).end()

  assert.equal(response.status, 404)
  const farmInDatabase =  await Farm.find(defaultFarm.id)
  assert.isNotNull(farmInDatabase)
})

test('should try to destroy farm with demand and return status 500', async ({ client, assert }) => {
  await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id
  })

  const response = await client.delete(`company/${defaultCompany.id}/farm/${defaultFarm.id}`).loginVia(defaultUser).end()

  assert.equal(response.status, 500)
  assert.equal(response.body.code, 'ER_ROW_IS_REFERENCED')
  assert.equal(response.body.table, 'DEMANDS')
  const farmInDatabase =  await Farm.find(defaultFarm.id)
  assert.isNotNull(farmInDatabase)
})

test('should try destroy farm without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.delete(`company/${defaultCompany.id}/farm/${defaultFarm.id}`).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.delete(`company/${defaultCompany.id}/farm/${defaultFarm.id}`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.delete(`company/${defaultCompany.id}/farm/${defaultFarm.id}`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

'use strict'

const clearDataBase = require('../../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Analytics - GET /demand/id/analytics/general')
const { ioc } = use('@adonisjs/fold')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()
})

test('analytics controller should return the harvested hectares', async({ client, assert }) => {
  ioc.fake('Services/Analytics', () => {
    return {
      machineGeneralStats: async (demandId, machineId) => {
        return {
          harvested_hectares: 104.83,
          chopper_losses_average: 12.15,
          platform_losses_average: 9.94
        }
      }
    }
  })

  let user = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  const response = await client.get('demand/1/machine/1/analytics/general').loginVia(user).end()

  let expectedResponse = [
    { name: 'Hectares colhidos', value: 104.83 },
    null,
    null,
    { name: 'Perda média do picador', value: 12.15 },
    null,
    null,
    { name: 'Perda média da plataforma', value: 9.94 }
  ]

  assert.equal(response.status, 200)
  assert.sameDeepOrderedMembers(response.body, expectedResponse)

  ioc.restore('Services/Analytics')
})

test('analytics controller should return 401 when have no logged user', async({ client, assert }) => {
  const response = await client.get('demand/1/machine/1/analytics/general').end()
  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const monitor = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.get('demand/1/machine/1/analytics/general').loginVia(monitor).end()

  assert.equal(response.status, 401)
})

'use strict'

const clearDataBase = require('../../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Analytics - GET /demand/id/analytics/harvested-hectare')
const { ioc } = use('@adonisjs/fold')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()
})

test('analytics controller should return the harvested hectares', async({ client, assert }) => {
  let series = [{ label: 'Semana 1', value: 36.04 }, { label: 'Semana 2', value: 6.34 }]

  ioc.fake('Services/Analytics', () => {
    return {
      harvestedHectares: (demandId, machineId, startDate, endDate) => {
        return series
      }
    }
  })

  let company = await Factory.model('App/Models/Company').create()
  let user = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  let farm = await Factory.model('App/Models/Farm').create({
    company_id: company.id
  })
  let demand = await Factory.model('App/Models/Demand').create({
    company_id: company.id,
    farm_id: farm.id
  })

  const response = await client.get('demand/1/machine/1/analytics/harvested-hectare').loginVia(user).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 1)
  assert.equal(response.body[0].name, 'Hectares Colhidos')
  assert.lengthOf(response.body[0].series, 2)
  assert.deepEqual(response.body[0].series, series)

  ioc.restore('Services/Analytics')
})

test('analytics controller should return the harvested hectares to manager', async({ client, assert }) => {

  let series = [{ label: 'Semana 1', value: 36.04 }, { label: 'Semana 2', value: 6.34 }]

  ioc.fake('Services/Analytics', () => {
    return {
      harvestedHectares: (demandId, machineId, startDate, endDate) => {
        return series
      }
    }
  })

  let company = await Factory.model('App/Models/Company').create()
  let user = await Factory.model('App/Models/User').create({ role: 'MANAGER', company_id: company.id })
  let farm = await Factory.model('App/Models/Farm').create({
    company_id: company.id
  })
  let demand = await Factory.model('App/Models/Demand').create({
    company_id: company.id,
    farm_id: farm.id
  })

  const response = await client.get('demand/1/machine/1/analytics/harvested-hectare').loginVia(user).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 1)
  assert.equal(response.body[0].name, 'Hectares Colhidos')
  assert.lengthOf(response.body[0].series, 2)
  assert.sameDeepOrderedMembers(response.body[0].series, series)

  ioc.restore('Services/Analytics')
})

test('analytics controller should return 401 when have no logged user', async({ client, assert }) => {
  const response = await client.get('demand/1/machine/1/analytics/harvested-hectare').end()
  assert.equal(response.status, 401)
})

test('analytics controller should get a forbidden to incorrect manager', async({ client, assert }) => {
  let company = await Factory.model('App/Models/Company').create()
  let user = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  let farm = await Factory.model('App/Models/Farm').create({
    company_id: company.id
  })
  let demand = await Factory.model('App/Models/Demand').create({
    company_id: company.id,
    farm_id: farm.id
  })

  const response = await client.get('demand/1/machine/1/analytics/harvested-hectare').loginVia(user).end()

  assert.equal(response.status, 403)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.get('demand/1/machine/1/analytics/harvested-hectare').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

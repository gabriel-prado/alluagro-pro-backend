'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Demand Machine - GET /demand/id/machine')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultAdmin = null
let defaultMonitor = null
let defaultCompany = null
let defaultFarm = null
let defaultPlot = null
let defaultDemand = null
let defaultMachines = null
let defaultDemandMachines = null
let defaultProvider = null
let defaultMachineCategory = null
let defaultMachineType = null

beforeEach(async () => {
  await clearDataBase()
  defaultAdmin = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultMonitor = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  defaultCompany = await Factory.model('App/Models/Company').create()
  defaultFarm = await Factory.model('App/Models/Farm').create({
    company_id: defaultCompany.id
  })
  defaultPlot = await Factory.model('App/Models/Plot').create({
    farm_id: defaultFarm.id
  })
  defaultDemand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: defaultMonitor.id
  })
  defaultProvider = await Factory.model('App/Models/Provider').create()
  defaultMachineCategory = await Factory.model('App/Models/MachineCategory').create()
  defaultMachineType = await Factory.model('App/Models/MachineType').create({
    machineCategory_id: defaultMachineCategory.id
  })
  defaultMachines = await Factory.model('App/Models/Machine').createMany(10, {
    machineCategory_id: defaultMachineCategory.id,
    machineType_id: defaultMachineType.id,
    provider_id: defaultProvider.id
  })

  for (let i = 0; i < defaultMachines.length; i++) {
    await Factory.model('App/Models/DemandMachine').create({
      demand_id: defaultDemand.id,
      machine_id: defaultMachines[i].id
    })
  }
})

test('should retrieve machines paginated', async({ client, assert }) => {
  const response = await client.get(`demand/${defaultDemand.id}/machine`).query({ page: 4, perPage: 3 }).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.page, 4)
  assert.lengthOf(response.body.data, 1)
  assert.equal(response.body.data[0].id, defaultMachines[9].id)
  assert.lengthOf(response.body.data[0].machinePhotos, 0)
})

test('should retrieve machines paginated to manager', async({ client, assert }) => {
  let user = await Factory.model('App/Models/User').create({ role: 'MANAGER', company_id: defaultCompany.id })
  const response = await client.get(`demand/${defaultDemand.id}/machine`).query({ page: 4, perPage: 3 }).loginVia(user).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.page, 4)
  assert.lengthOf(response.body.data, 1)
  assert.equal(response.body.data[0].id, defaultMachines[9].id)
  assert.lengthOf(response.body.data[0].machinePhotos, 0)
})

test('should retrieve machines paginated to monitor', async({ client, assert }) => {
  const response = await client.get(`demand/${defaultDemand.id}/machine`).query({ page: 4, perPage: 3 }).loginVia(defaultMonitor).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.page, 4)
  assert.lengthOf(response.body.data, 1)
  assert.equal(response.body.data[0].id, defaultMachines[9].id)
  assert.lengthOf(response.body.data[0].machinePhotos, 0)
})

test('should get a forbidden to incorrect monitor', async({ client, assert }) => {
  const monitor = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.get(`demand/${defaultDemand.id}/machine`).query({ page: 4, perPage: 3 }).loginVia(monitor).end()

  assert.equal(response.status, 403)
})

test('should retrieve machines with default pagination', async({ client, assert }) => {
  const response = await client.get(`demand/${defaultDemand.id}/machine`).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.page, 1)
  assert.lengthOf(response.body.data, 5)
})

test('should retrieve all machines', async({ client, assert }) => {
  const response = await client.get(`demand/${defaultDemand.id}/machine`).query({ all: true }).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 10)
})

test('should not list machines for not logged user', async({ client, assert }) => {
  const response = await client.get(`demand/${defaultDemand.id}/machine`).end()

  assert.equal(response.status, 401)
})

test('shoul retrieve machines with code', async({ client, assert }) => {
  const response = await client.get(`demand/${defaultDemand.id}/machine`).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.containsAllKeys(response.body.data[0], ['machine_code'])
})

test('should retrieve machines with hectares_harvested sum', async({ client, assert }) => {

  const report = {
    demand_id: defaultDemand.id,
    plot_id: defaultPlot.id
  }

  const firstMachineReports = await
    Factory
      .model('App/Models/Report')
      .createMany(3, { ...report, machine_id: defaultMachines[0].id, hectares_harvested: 3 })

  const secondMachineReports = await
    Factory
      .model('App/Models/Report')
      .createMany(2, { ...report, machine_id: defaultMachines[1].id, hectares_harvested: 2 })

  const response = await client.get(`demand/${defaultDemand.id}/machine`).query({ all: true }).loginVia(defaultAdmin).end()

  const firstMachine = response.body.data.find(m => m.id === defaultMachines[0].id)
  const secondMachine = response.body.data.find(m => m.id === defaultMachines[1].id)
  const thirdMachine = response.body.data.find(m => m.id === defaultMachines[2].id)

  assert.isNotNull(firstMachine)
  assert.isNotNull(secondMachine)
  assert.isNotNull(thirdMachine)
  assert.lengthOf(response.body.data, 10)
  assert.equal(firstMachine.hectares_harvested, 9)
  assert.equal(secondMachine.hectares_harvested, 4)
  assert.equal(thirdMachine.hectares_harvested, 0)
})

test('should get a forbidden to incorrect manager', async({ client, assert}) => {
  const managerUser = await Factory.model('App/Models/User').create({
    role: 'MANAGER'
  })

  const response = await
    client
      .get(`demand/${defaultDemand.id}/machine`)
      .loginVia(managerUser)
      .end()

  assert.equal(response.status, 403)
})

test('should retrieve machines to correct manager', async({ client, assert}) => {
  const managerUser = await Factory.model('App/Models/User').create({
    role: 'MANAGER',
    company_id: defaultCompany.id
  })

  const response = await
    client
      .get(`demand/${defaultDemand.id}/machine`)
      .query({ all: true })
      .loginVia(managerUser)
      .end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 10)
})

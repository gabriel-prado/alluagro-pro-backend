'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Demand Machine - GET /demand/id/machine/id')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultAdmin = null
let defaultMonitor = null
let defaultCompany = null
let defaultFarm = null
let defaultDemand = null
let defaultMachine = null
let defaultDemandMachine = null
let defaultProvider = null
let defaultMachineCategory = null
let defaultMachineType = null

beforeEach(async () => {
  await clearDataBase()
  defaultAdmin = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultMonitor = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  defaultCompany = await Factory.model('App/Models/Company').create()
  defaultFarm = await Factory.model('App/Models/Farm').create({
    company_id: defaultCompany.id
  })
  defaultDemand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: defaultMonitor.id
  })
  defaultProvider = await Factory.model('App/Models/Provider').create()
  defaultMachineCategory = await Factory.model('App/Models/MachineCategory').create()
  defaultMachineType = await Factory.model('App/Models/MachineType').create({
    machineCategory_id: defaultMachineCategory.id
  })
  defaultMachine = await Factory.model('App/Models/Machine').create({
    machineCategory_id: defaultMachineCategory.id,
    machineType_id: defaultMachineType.id,
    provider_id: defaultProvider.id
  })
  defaultDemandMachine = await Factory.model('App/Models/DemandMachine').create({
    demand_id: defaultDemand.id,
    machine_id: defaultMachine.id
  })
})

test('should not get machine for not logged user', async({ client, assert }) => {
  const response = await client.get(`demand/${defaultDemand.id}/machine/${defaultMachine.id}`).end()

  assert.equal(response.status, 401)
})

test('should retrieve existent machine', async({ client, assert}) => {
  const response = await client.get(`demand/${defaultDemand.id}/machine/${defaultMachine.id}`).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.include(response.body, defaultMachine.toJSON())
  assert.include(response.body.provider, defaultProvider.toJSON())
  assert.equal(defaultDemandMachine.machine_code, response.body.machine_code)
})

test('should retrieve existent machine to manager', async({ client, assert}) => {
  let user = await Factory.model('App/Models/User').create({ role: 'MANAGER', company_id: defaultCompany.id })
  const response = await client.get(`demand/${defaultDemand.id}/machine/${defaultMachine.id}`).loginVia(user).end()

  assert.equal(response.status, 200)
})

test('should retrieve existent machine to monitor', async({ client, assert}) => {
  const response = await client.get(`demand/${defaultDemand.id}/machine/${defaultMachine.id}`).loginVia(defaultMonitor).end()

  assert.equal(response.status, 200)
})

test('should get a forbidden to incorrect monitor', async({ client, assert}) => {
  let monitor = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.get(`demand/${defaultDemand.id}/machine/${defaultMachine.id}`).loginVia(monitor).end()

  assert.equal(response.status, 403)
})

test('should get 404 for a non existent demand', async({ client, assert}) => {
  const response = await client.get(`demand/233/machine/${defaultMachine.id}`).loginVia(defaultAdmin).end()

  assert.equal(response.status, 404)
})

test('should get 404 for a non existent machine', async({ client, assert}) => {
  const response = await client.get(`demand/${defaultDemand.id}/machine/123`).loginVia(defaultAdmin).end()

  assert.equal(response.status, 404)
})

test('should get a forbidden to incorrect manager', async({ client, assert}) => {
  const managerUser = await Factory.model('App/Models/User').create({
    role: 'MANAGER'
  })

  const response = await
    client
      .get(`demand/${defaultDemand.id}/machine/${defaultMachine.id}`)
      .loginVia(managerUser)
      .end()

  assert.equal(response.status, 403)
})

test('should retrieve machine to correct manager', async({ client, assert}) => {
  const managerUser = await Factory.model('App/Models/User').create({
    role: 'MANAGER',
    company_id: defaultCompany.id
  })

  const response = await
    client
      .get(`demand/${defaultDemand.id}/machine/${defaultMachine.id}`)
      .loginVia(managerUser)
      .end()

  assert.equal(response.status, 200)
  assert.include(response.body, defaultMachine.toJSON())
})

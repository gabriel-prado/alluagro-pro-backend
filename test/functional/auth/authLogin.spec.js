'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const User = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('Authentication - POST /auth/login')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()
})

test('should authenticate user', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').create({
    password: 'flatron123'
  });

  const response = await client.post('auth/login').send({
    email: user.email,
    password: 'flatron123'
  }).end()

  assert.equal(response.status, 200)
  assert.hasAllKeys(response.body, ['token'])
})

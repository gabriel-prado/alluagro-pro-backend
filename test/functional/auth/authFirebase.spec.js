'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const User = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('Authentication - GET /auth/firebase')
const { ioc } = use('@adonisjs/fold')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => await clearDataBase())

test('should retrieve firebase authenticated token', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').create()
  const tokenFirebase = 'TOKEN_123'

  ioc.fake('Services/Firebase', () => {
    return {
      generateToken: async () => {
        return new Promise((resolve, reject) => {
          resolve({ token: tokenFirebase })
        })
      }
    }
  })

  const response = await client.get('auth/firebase').loginVia(user).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.token, tokenFirebase)

  ioc.restore('Services/Firebase')
})

'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const User = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('Authentication - GET /auth/me')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()
})

test('should retrieve authenticated user', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').create();

  const response = await client.get('auth/me').loginVia(user).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.email, user.email)
})

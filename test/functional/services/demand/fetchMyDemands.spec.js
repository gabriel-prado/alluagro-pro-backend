'use strict'

const clearDataBase = require('../../../utils/clearDataBase')

const Factory = use('Factory')
const DemandService = use('Services/Demand')
const { test, trait, beforeEach } = use('Test/Suite')('Services/Demand.fetchMyDemands')

let defaultCompany = null
let defaultFarm = null

beforeEach(async () => {
  await clearDataBase()

  defaultCompany = await Factory.model('App/Models/Company').create()
  defaultFarm = await Factory.model('App/Models/Farm').create({ company_id: defaultCompany.id })
})

test('Tests the method that returns all the demands that the ADMIN user has access to', async({ assert }) => {
  let user = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  let demands = await Factory.model('App/Models/Demand').createMany(5, {
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    hectares: 353
  })

  let response = await DemandService.fetchMyDemands(user)

  assert.lengthOf(response, demands.length)
})

test('Tests the method that returns all the demands that the MONITOR user has access to', async({ assert }) => {
  let user = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  await Factory.model('App/Models/Demand').createMany(5, {
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    hectares: 353
  })

  let demand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    hectares: 353,
    monitor_id: user.id
  })

  let response = await DemandService.fetchMyDemands(user)

  assert.lengthOf(response, 1)
})

test('Tests the method that returns all the demands that the MANAGER user has access to', async({ assert }) => {
  let managerCompany = await Factory.model('App/Models/Company').create()
  let managerFarm = await Factory.model('App/Models/Farm').create({ company_id: managerCompany.id })
  let user = await Factory.model('App/Models/User').create({ role: 'MANAGER', company_id: managerCompany.id })

  await Factory.model('App/Models/Demand').createMany(2, {
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    hectares: 353
  })

  let demands = await Factory.model('App/Models/Demand').createMany(3, {
    company_id: managerCompany.id,
    farm_id: managerFarm.id,
    hectares: 353
  })

  let response = await DemandService.fetchMyDemands(user)

  assert.lengthOf(response, demands.length)
})

'use strict'

const clearDataBase = require('../../../utils/clearDataBase')
const Firebase = require('firebase-mock')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Services/Demand.deleteMessages')
const { ioc } = use('@adonisjs/fold')

let company = null
let farm = null

beforeEach(async () => {
  await clearDataBase()

  company = await Factory.model('App/Models/Company').create()
  farm = await Factory.model('App/Models/Farm').create({ company_id: company.id })
})

test('Tests the method of deleting chat messages from a demand', async({ assert }) => {
  ioc.fake('App/Firebase', () => {
    const mockfirestore = new Firebase.MockFirestore()
    const mockauth = new Firebase.MockFirebase()
    const mocksdk = new Firebase.MockFirebaseSdk(null, () => mockauth, () => mockfirestore)

    mockfirestore.autoFlush()

    return mocksdk.initializeApp()
  })

  const DemandService = use('Services/Demand')

  let demand = await Factory.model('App/Models/Demand').create({
    company_id: company.id,
    farm_id: farm.id,
    hectares: 353,
    start_date: '2018-02-04',
    end_date: '2018-03-03'
  })

  let response = await DemandService.deleteMessages(demand.id)

  assert.equal(response, null)

  ioc.restore('App/Firebase')
})

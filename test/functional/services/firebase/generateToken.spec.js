'use strict'

const clearDataBase = require('../../../utils/clearDataBase')
const { constants: { DEFAULT_DATABASE_URL }, AdminRoot } = require('firebase-admin-mock')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Services/Firebase.generateToken')
const { ioc } = use('@adonisjs/fold')

beforeEach(async () => await clearDataBase())

test('Tests the method that returns a custom token from the Firebase', async({ assert }) => {
  ioc.fake('App/Firebase', () => {
    let admin = new AdminRoot()
    let firebase = admin.initializeApp({databaseUrl: DEFAULT_DATABASE_URL})
    firebase.auth().createCustomToken = (id, data) => 'TOKEN_123'

    return firebase
  })

  const FirebaseService = use('Services/Firebase')

  let user = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  let response = await FirebaseService.generateToken(user)

  assert.isNotNull(response.token)

  ioc.restore('App/Firebase')
})

'use strict'

const clearDataBase = require('../../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Services/Analytics.hourMeter')

const moment = require('moment')
const Twix = require('twix')

const AnalyticsService = use('Services/Analytics')

let plot = null
let demand = null
let machine = null

beforeEach(async () => {
    await clearDataBase()
    let company = await Factory.model('App/Models/Company').create()
    let farm = await Factory.model('App/Models/Farm').create({ company_id: company.id })
    plot = await Factory.model('App/Models/Plot').create({ farm_id: farm.id })

    demand = await Factory.model('App/Models/Demand').create({
      company_id: company.id,
      farm_id: farm.id,
      hectares: 200,
      start_date: '2018-02-15',
      end_date: '2018-02-28'
    })

    let provider = await Factory.model('App/Models/Provider').create()
    let machineCategory = await Factory.model('App/Models/MachineCategory').create()
    let machineType = await Factory.model('App/Models/MachineType').create({ machineCategory_id: machineCategory.id })
    machine = await Factory.model('App/Models/Machine').create({
        machineCategory_id: machineCategory.id,
        machineType_id: machineType.id,
        provider_id: provider.id
    })
})

test('analytics service should return one-week hour meter', async({ assert }) => {
    _saveReport(machine.id, '2018-02-18', 10, 0)
    _saveReport(machine.id, '2018-02-19', 10, 9)
    _saveReport(machine.id, '2018-02-20', 5, 4)
    _saveReport(machine.id, '2018-02-20', 2, 2)
    _saveReport(machine.id, '2018-02-21', 4, 4)
    _saveReport(machine.id, '2018-02-22', 0, null)
    _saveReport(machine.id, '2018-02-24', 10, 5)

    const hourMeter = await AnalyticsService.hourMeter(demand.id, machine.id, '2018-02-18', '2018-02-24')

    const days = ['DOM', 'SEG', 'TER', 'QUA', 'QUI', 'SEX', 'SÁB']

    const on = hourMeter[0]
    assert.equal(on.name, 'Horas ligada')
    assert.equal(on.series.length, 7)
    on.series.forEach((d, i) => assert.equal(d.name, days[i]))

    const worked = hourMeter[1]
    assert.equal(worked.name, 'Horas trabalhadas')
    assert.equal(worked.series.length, 7)
    worked.series.forEach((d, i) => assert.equal(d.name, days[i]))

    const data = _toKeyValuePair(on.series, worked.series, 'DAYS')

    _assertValues(data, 'DOM', 10, 0, assert)
    _assertValues(data, 'SEG', 10, 9, assert)
    _assertValues(data, 'TER', 7, 6, assert)
    _assertValues(data, 'QUA', 4, 4, assert)
    _assertValues(data, 'QUI', 0, 0, assert)
    _assertValues(data, 'SEX', 0, 0, assert)
    _assertValues(data, 'SÁB', 10, 5, assert)
})

test('analytics service should return hour meter for every week', async({ assert }) => {
    // Week 1
    _saveReport(machine.id, '2018-02-12', 10, 2)
    _saveReport(machine.id, '2018-02-12', 5, 4)
    _saveReport(machine.id, '2018-02-13', 5, 2)
    // Week 3
    _saveReport(machine.id, '2018-02-26', null, 0)
    _saveReport(machine.id, '2018-02-27', 15, 6)
    _saveReport(machine.id, '2018-02-28', 15, 6)

    const hourMeter = await AnalyticsService.hourMeter(demand.id, machine.id)

    const weeks = ['Semana 1', 'Semana 2', 'Semana 3']

    const on = hourMeter[0]
    assert.equal(on.name, 'Horas ligada')
    assert.equal(on.series.length, 3)
    on.series.forEach((w, i) => assert.equal(w.name, weeks[i]))

    const worked = hourMeter[1]
    assert.equal(worked.name, 'Horas trabalhadas')
    assert.equal(worked.series.length, 3)
    worked.series.forEach((w, i) => assert.equal(w.name, weeks[i]))

    const data = _toKeyValuePair(on.series, worked.series, 'WEEKS')

    _assertValues(data, 'Semana 1', 20, 8, assert)
    _assertValues(data, 'Semana 2', 0, 0, assert)
    _assertValues(data, 'Semana 3', 30, 12, assert)
})

async function _saveReport(machineId, date, engine_hourmeter, rotor_hourmeter) {
    const report = await Factory.model('App/Models/Report').create({
        plot_id: plot.id,
        demand_id: demand.id,
        machine_id: machineId,
        date: date,
        engine_hourmeter: engine_hourmeter,
        rotor_hourmeter: rotor_hourmeter
    })

    return report
}

function _assertValues(response, key, engine_hourmeter, rotor_hourmeter, assert) {
    const data = response[key]
    assert.exists(data, key + ' data does not exist')
    assert.equal(data.engine_hourmeter, engine_hourmeter, key + ' engine with wrong value')
    assert.equal(data.rotor_hourmeter, rotor_hourmeter, key + ' rotor with wrong value')
}

function _toKeyValuePair(onSeries, workedSeries, type) {
    const data = {}

    let keys = null
    if(type === 'DAYS') {
        keys = ['DOM', 'SEG', 'TER', 'QUA', 'QUI', 'SEX', 'SÁB']
    } else if (type === 'WEEKS') {
        keys = ['Semana 1', 'Semana 2', 'Semana 3']
    }

    keys.forEach(key => {
        data[key] = {
            engine_hourmeter: onSeries.find(e => e.name === key).value,
            rotor_hourmeter: workedSeries.find(w => w.name === key).value
        }
    })

    return data
}
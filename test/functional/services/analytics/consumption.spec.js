'use strict'

const clearDataBase = require('../../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Services/Analytics.consumption')

const moment = require('moment')
const Twix = require('twix')

const AnalyticsService = use('Services/Analytics')

let plot = null
let demand = null
let machine = null

beforeEach(async () => {
    await clearDataBase()
    let company = await Factory.model('App/Models/Company').create()
    let farm = await Factory.model('App/Models/Farm').create({ company_id: company.id })
    plot = await Factory.model('App/Models/Plot').create({ farm_id: farm.id })

    demand = await Factory.model('App/Models/Demand').create({
      company_id: company.id,
      farm_id: farm.id,
      hectares: 200,
      start_date: '2018-02-15',
      end_date: '2018-02-28'
    })

    let provider = await Factory.model('App/Models/Provider').create()
    let machineCategory = await Factory.model('App/Models/MachineCategory').create()
    let machineType = await Factory.model('App/Models/MachineType').create({ machineCategory_id: machineCategory.id })
    machine = await Factory.model('App/Models/Machine').create({
        machineCategory_id: machineCategory.id,
        machineType_id: machineType.id,
        provider_id: provider.id
    })

    // Week 1
    _saveReport(machine.id, '2018-02-12', 22, null, null)
    _saveReport(machine.id, '2018-02-12', 10, null, null)
    _saveReport(machine.id, '2018-02-13', 8, null, null)
    // Week 2
    _saveReport(machine.id, '2018-02-18', null, 0, null)
    _saveReport(machine.id, '2018-02-19', 0, 1, null)
    _saveReport(machine.id, '2018-02-20', 15, 5, 1)
    _saveReport(machine.id, '2018-02-20', 20, null, 1)
    _saveReport(machine.id, '2018-02-20', 15, null, 2)
    _saveReport(machine.id, '2018-02-21', 10, 2, 3)
    _saveReport(machine.id, '2018-02-24', 15, 3, 5)
    // Week 3
    _saveReport(machine.id, '2018-02-26', null, null, null)
    _saveReport(machine.id, '2018-02-27', 35, null, null)
    _saveReport(machine.id, '2018-02-28', 20, null, null)
})

test('analytics service should return one-week consumption', async({ assert }) => {
    const consumption = await AnalyticsService.consumption(demand, machine.id, '2018-02-18', '2018-02-24')

    assert.exists(consumption)

    const data = _toObject(consumption)
    assert.equal(data['DOM'], 0)
    assert.equal(data['SEG'], 0)
    assert.equal(data['TER'], 50)
    assert.equal(data['QUA'], 10)
    assert.equal(data['QUI'], 0)
    assert.equal(data['SEX'], 0)
    assert.equal(data['SÁB'], 15)
})

test('analytics service should return consumption for every week', async({ assert }) => {
    const consumption = await AnalyticsService.consumption(demand, machine.id)

    assert.exists(consumption)

    const data = _toObject(consumption)
    assert.equal(data['Semana 1'], 40)
    assert.equal(data['Semana 2'], 75)
    assert.equal(data['Semana 3'], 55)
})

test('analytics service should return one-week consumption per harvested hectares', async({ assert }) => {
    const consumption = await AnalyticsService.consumptionPerHarvestedHectares(demand, machine.id, '2018-02-18', '2018-02-24')

    assert.exists(consumption)

    const data = _toObject(consumption)
    assert.equal(data['DOM'], 0)
    assert.equal(data['SEG'], 0)
    assert.equal(data['TER'], 12.5)
    assert.equal(data['QUA'], 3.33)
    assert.equal(data['QUI'], 0)
    assert.equal(data['SEX'], 0)
    assert.equal(data['SÁB'], 3)
})

test('analytics service should return consumption per harvested hectares for every week', async({ assert }) => {
    const consumption = await AnalyticsService.consumptionPerHarvestedHectares(demand, machine.id)

    assert.exists(consumption)

    const data = _toObject(consumption)
    assert.equal(data['Semana 1'], 0)
    assert.equal(data['Semana 2'], 6.25)
    assert.equal(data['Semana 3'], 0)
})

test('analytics service should return one-week consumption per worked hours', async({ assert }) => {
    const consumption = await AnalyticsService.consumptionPerWorkedHours(demand, machine.id, '2018-02-18', '2018-02-24')

    assert.exists(consumption)

    const data = _toObject(consumption)
    assert.equal(data['DOM'], 0)
    assert.equal(data['SEG'], 0)
    assert.equal(data['TER'], 10)
    assert.equal(data['QUA'], 5)
    assert.equal(data['QUI'], 0)
    assert.equal(data['SEX'], 0)
    assert.equal(data['SÁB'], 5)
})

test('analytics service should return consumption per harvested hectares for every week', async({ assert }) => {
    const consumption = await AnalyticsService.consumptionPerWorkedHours(demand, machine.id)

    assert.exists(consumption)

    const data = _toObject(consumption)
    assert.equal(data['Semana 1'], 0)
    assert.equal(data['Semana 2'], 6.82)
    assert.equal(data['Semana 3'], 0)
})

async function _saveReport(machineId, date, fuel, rotor_hourmeter, hectares_harvested) {
    const report = await Factory.model('App/Models/Report').create({
        plot_id: plot.id,
        demand_id: demand.id,
        machine_id: machineId,
        date,
        fuel, 
        rotor_hourmeter,
        hectares_harvested
    })

    return report
}

function _toObject(array) {
    const data = {}
    array.forEach((i) => {
        data[i.name] = i.value
    })
    return data
}
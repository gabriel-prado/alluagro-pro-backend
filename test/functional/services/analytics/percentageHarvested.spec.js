'use strict'

const clearDataBase = require('../../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Services/Analytics.percentageHarvested')

const AnalyticsService = use('Services/Analytics')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()
})

test('analytics service should return the percentage harvested', async({ client, assert }) => {
  let company = await Factory.model('App/Models/Company').create()
  let farm = await Factory.model('App/Models/Farm').create({ company_id: company.id })
  let plot = await Factory.model('App/Models/Plot').create({ farm_id: farm.id })
  let provider = await Factory.model('App/Models/Provider').create()
  let machineCategory = await Factory.model('App/Models/MachineCategory').create()
  let machineType = await Factory.model('App/Models/MachineType').create({ machineCategory_id: machineCategory.id })

  let demand = await Factory.model('App/Models/Demand').create({
    company_id: company.id,
    farm_id: farm.id,
    hectares: 200
  })

  let machines = await Factory.model('App/Models/Machine').createMany(2, {
    machineCategory_id: machineCategory.id,
    machineType_id: machineType.id,
    provider_id: provider.id
  })

  await Factory.model('App/Models/Report').create({
    machine_id: machines[0].id,
    plot_id: plot.id,
    demand_id: demand.id,
    date: '2017-01-04',
    hectares_harvested: 20.40
  })

  await Factory.model('App/Models/Report').create({
    machine_id: machines[1].id,
    plot_id: plot.id,
    demand_id: demand.id,
    date: '2017-01-04',
    hectares_harvested: 15.64
  })

  let data = await AnalyticsService.percentageHarvested(demand.id)

  assert.isObject(data)
  assert.equal(data.value, 36.04)
  assert.equal(data.totalValue, 200)
})
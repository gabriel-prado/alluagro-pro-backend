'use strict'

const clearDataBase = require('../../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Services/Analytics.harvestedHectares')

const AnalyticsService = use('Services/Analytics')

let company = null
let farm = null

beforeEach(async () => {
  await clearDataBase()
  company = await Factory.model('App/Models/Company').create()
  farm = await Factory.model('App/Models/Farm').create({ company_id: company.id })
})

test('analytics service should return demand burnup weekly', async({ assert }) => {
  let demand = await Factory.model('App/Models/Demand').create({
    company_id: company.id,
    farm_id: farm.id,
    hectares: 353, 
    start_date: '2018-02-04',
    end_date: '2018-03-03'
  })

  let response = await AnalyticsService.burnup(demand.id)

  let expectedResponse = [
    { name: 'Semana 1', value: 88.25 }, 
    { name: 'Semana 2', value: 176.5 }, 
    { name: 'Semana 3', value: 264.75 }, 
    { name: 'Semana 4', value: 353 }
  ]

  assert.sameDeepOrderedMembers(response, expectedResponse)
})
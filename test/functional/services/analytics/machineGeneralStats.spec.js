'use strict'

const clearDataBase = require('../../../utils/clearDataBase')

const Factory = use('Factory')
const { test, beforeEach } = use('Test/Suite')('Services/Analytics.machineGeneralStats')

const AnalyticsService = use('Services/Analytics')

let machines = null
let demand = null
let plot = null

beforeEach(async () => {
  await clearDataBase()
  let company = await Factory.model('App/Models/Company').create()
  let farm = await Factory.model('App/Models/Farm').create({ company_id: company.id })
  let provider = await Factory.model('App/Models/Provider').create()
  let machineCategory = await Factory.model('App/Models/MachineCategory').create()
  let machineType = await Factory.model('App/Models/MachineType').create({ machineCategory_id: machineCategory.id })
  plot = await Factory.model('App/Models/Plot').create({ farm_id: farm.id })
  demand = await Factory.model('App/Models/Demand').create({
    company_id: company.id,
    farm_id: farm.id,
    hectares: 200,
    start_date: '2018-02-04',
    end_date: '2018-02-24'
  })
  machines = await Factory.model('App/Models/Machine').createMany(2, {
    machineCategory_id: machineCategory.id,
    machineType_id: machineType.id,
    provider_id: provider.id
  })
})

test('analytics service should return the machine general stats', async({ assert }) => {
  let reportData = {
    machine_id: machines[0].id, 
    plot_id: plot.id,
    demand_id: demand.id
  }

  await Factory.model('App/Models/Report').create({
    ...reportData,
    ...buildLoss('platform', [10, null, 11, 12]),
    ...buildLoss('chopper', [9, 8, 7, 6]),
    hectares_harvested: 30.21,
    rotor_hourmeter: 25,
    fuel: 400,
    date: '2018-02-04',
    engine_hourmeter: 20
  })

  await Factory.model('App/Models/Report').create({
    ...reportData,
    ...buildLoss('platform', [11, 10, 9, 8]),
    ...buildLoss('chopper', [10, null, 10, 10]),
    hectares_harvested: 21.20,
    rotor_hourmeter: 25,
    fuel: 400,
    date: '2018-02-05',
    engine_hourmeter: 20
  })

  await Factory.model('App/Models/Report').create({
    ...reportData,
    ...buildLoss('platform', [18, 17, 16, 15]),
    ...buildLoss('chopper', [12, 12, null, 6]),
    hectares_harvested: 21.31,
    rotor_hourmeter: 25,
    fuel: 200,
    date: '2018-02-06',
    engine_hourmeter: 60
  })

  await Factory.model('App/Models/Report').create({
    ...reportData,
    ...buildLoss('platform', [10, 13.20, null, null]), // 11,6
    ...buildLoss('chopper', [12.80, 11.20, 12, 13]), // 12,25
    hectares_harvested: 32.11,
    rotor_hourmeter: 27,
    fuel: 48.3,
    date: '2018-02-24',
    engine_hourmeter: 34
  })

  let data = await AnalyticsService.machineGeneralStats(demand.id, machines[0].id)

  let expectedResponse = {
    harvested_hectares: 104.83,
    worked_hours: 102,
    consumption_per_hectares: 10.00,
    chopper_losses_average: 9.94,
    on_hours: 134,
    consumption_per_worked_hours: 10.28,
    platform_losses_average: 12.15
  }

  assert.deepEqual(data, expectedResponse)
})

const buildLoss = (type, values) => {
  if(type === 'platform') {
    return {
      platform_losses_1: (values[0]) ? values[0] : null,
      platform_losses_2: (values[1]) ? values[1] : null,
      platform_losses_3: (values[2]) ? values[2] : null,
      platform_losses_4: (values[3]) ? values[3] : null
    }
  } else if(type === 'chopper') {
    return {
      chopper_losses_1: (values[0]) ? values[0] : null,
      chopper_losses_2: (values[1]) ? values[1] : null,
      chopper_losses_3: (values[2]) ? values[2] : null,
      chopper_losses_4: (values[3]) ? values[3] : null
    }
  }

  return {}
}

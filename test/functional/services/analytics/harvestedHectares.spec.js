'use strict'

const clearDataBase = require('../../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Services/Analytics.harvestedHectares')

const AnalyticsService = use('Services/Analytics')

trait('Test/ApiClient')
trait('Auth/Client')

let machines = null
let demand = null
let plot = null

beforeEach(async () => {
  await clearDataBase()
  let company = await Factory.model('App/Models/Company').create()
  let farm = await Factory.model('App/Models/Farm').create({ company_id: company.id })
  let provider = await Factory.model('App/Models/Provider').create()
  let machineCategory = await Factory.model('App/Models/MachineCategory').create()
  let machineType = await Factory.model('App/Models/MachineType').create({ machineCategory_id: machineCategory.id })
  plot = await Factory.model('App/Models/Plot').create({ farm_id: farm.id })
  demand = await Factory.model('App/Models/Demand').create({
    company_id: company.id,
    farm_id: farm.id,
    hectares: 200,
    start_date: '2018-02-04',
    end_date: '2018-02-24'
  })
  machines = await Factory.model('App/Models/Machine').createMany(2, {
    machineCategory_id: machineCategory.id,
    machineType_id: machineType.id,
    provider_id: provider.id
  })
})

test('analytics service should return the harvested hectares weekly', async({ client, assert }) => {
  await Factory.model('App/Models/Report').create({
    machine_id: machines[0].id,
    plot_id: plot.id,
    demand_id: demand.id,
    date: '2018-02-04',
    hectares_harvested: 20.40
  })

  await Factory.model('App/Models/Report').create({
    machine_id: machines[0].id,
    plot_id: plot.id,
    demand_id: demand.id,
    date: '2018-02-24',
    hectares_harvested: 15.64
  })

  await Factory.model('App/Models/Report').create({
    machine_id: machines[1].id,
    plot_id: plot.id,
    demand_id: demand.id,
    date: '2018-02-24',
    hectares_harvested: 18.24
  })

  let data = await AnalyticsService.harvestedHectares(demand.id, machines[0].id, null, null)

  let expectedResponse = [
    { name: 'Semana 1', value: 20.40 },
    { name: 'Semana 2', value: 0 },
    { name: 'Semana 3', value: 15.64 }
  ]

  assert.sameDeepOrderedMembers(data, expectedResponse)
})

test('analytics service should return the harvested hectares per week day', async({ client, assert }) => {
  await Factory.model('App/Models/Report').create({
    machine_id: machines[0].id,
    plot_id: plot.id,
    demand_id: demand.id,
    date: '2018-02-04',
    hectares_harvested: 20.40
  })

  await Factory.model('App/Models/Report').create({
    machine_id: machines[0].id,
    plot_id: plot.id,
    demand_id: demand.id,
    date: '2018-02-08',
    hectares_harvested: 15.64
  })

  await Factory.model('App/Models/Report').create({
    machine_id: machines[1].id,
    plot_id: plot.id,
    demand_id: demand.id,
    date: '2018-02-04',
    hectares_harvested: 18.24
  })

  let data = await AnalyticsService.harvestedHectares(demand.id, machines[0].id, '2018-02-04', '2018-02-10')

  let expectedResponse = [
    { name: 'DOM', value: 20.40 },
    { name: 'SEG', value: 0 },
    { name: 'TER', value: 0 },
    { name: 'QUA', value: 0 },
    { name: 'QUI', value: 15.64 },
    { name: 'SEX', value: 0 },
    { name: 'SÁB', value: 0 }
  ]

  assert.sameDeepOrderedMembers(data, expectedResponse)
})

test('analytics service should return the harvested hectares accumulated weekly', async({ client, assert }) => {

  let reportFields = {
    machine_id: machines[0].id,
    plot_id: plot.id,
    demand_id: demand.id,
  }

  await Factory.model('App/Models/Report').create({
    ...reportFields,
    date: '2018-02-05',
    hectares_harvested: 20.40
  })

  await Factory.model('App/Models/Report').create({
    ...reportFields,
    date: '2018-02-07',
    hectares_harvested: 15.64
  })

  await Factory.model('App/Models/Report').create({
    ...reportFields,
    date: '2018-02-12',
    hectares_harvested: 18.24
  })

  await Factory.model('App/Models/Report').create({
    ...reportFields,
    date: '2018-02-14',
    hectares_harvested: 11.24
  })

  let data = await AnalyticsService.harvestedHectares(demand.id, machines[0].id, null, null, true, true)

  let expectedResponse = [
    { name: 'Semana 1', value: 36.04 },
    { name: 'Semana 2', value: 65.52 },
    { name: 'Semana 3', value: 65.52 }
  ]

  assert.sameDeepOrderedMembers(data, expectedResponse)
})

test('analytics service should return the harvested hectares accumulated per week day', async({ client, assert }) => {

  let reportFields = {
    machine_id: machines[0].id,
    plot_id: plot.id,
    demand_id: demand.id,
  }

  await Factory.model('App/Models/Report').create({
    ...reportFields,
    date: '2018-02-05',
    hectares_harvested: 20.40
  })

  await Factory.model('App/Models/Report').create({
    ...reportFields,
    date: '2018-02-07',
    hectares_harvested: 15.64
  })

  await Factory.model('App/Models/Report').create({
    ...reportFields,
    date: '2018-02-08',
    hectares_harvested: 18.24
  })

  let data = await AnalyticsService.harvestedHectares(demand.id, machines[0].id, '2018-02-04', '2018-02-10', true, true)

  let expectedResponse = [
    { name: 'DOM', value: 0 },
    { name: 'SEG', value: 20.40 },
    { name: 'TER', value: 20.40 },
    { name: 'QUA', value: 36.04 },
    { name: 'QUI', value: 54.28 },
    { name: 'SEX', value: 54.28 },
    { name: 'SÁB', value: 54.28 }
  ]

  assert.sameDeepOrderedMembers(data, expectedResponse)
})
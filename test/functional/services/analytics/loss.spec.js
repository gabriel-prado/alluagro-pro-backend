'use strict'

const clearDataBase = require('../../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Services/Analytics.averageLoss')

const AnalyticsService = use('Services/Analytics')

trait('Test/ApiClient')
trait('Auth/Client')

let machines = null
let demand = null
let plot = null

beforeEach(async () => {
  await clearDataBase()
  let company = await Factory.model('App/Models/Company').create()
  let farm = await Factory.model('App/Models/Farm').create({ company_id: company.id })
  let provider = await Factory.model('App/Models/Provider').create()
  let machineCategory = await Factory.model('App/Models/MachineCategory').create()
  let machineType = await Factory.model('App/Models/MachineType').create({ machineCategory_id: machineCategory.id })
  plot = await Factory.model('App/Models/Plot').create({ farm_id: farm.id })
  demand = await Factory.model('App/Models/Demand').create({
    company_id: company.id,
    farm_id: farm.id,
    hectares: 200,
    start_date: '2018-02-04',
    end_date: '2018-02-24'
  })
  machines = await Factory.model('App/Models/Machine').createMany(2, {
    machineCategory_id: machineCategory.id,
    machineType_id: machineType.id,
    provider_id: provider.id
  })
})

test('analytics service should return the chopper losses weekly', async({ client, assert }) => {
  let reportData = {
    machine_id: machines[0].id, 
    plot_id: plot.id,
    demand_id: demand.id
  }

  let report1 = await Factory.model('App/Models/Report').create({
    ...reportData,
    ...buildLoss('chopper', [10, 13.20, null, null]),
    date: '2018-02-04'
  })

  let report2 = await Factory.model('App/Models/Report').create({
    ...reportData,
    ...buildLoss('chopper', [9, null, 10.48, 11.31]),
    date: '2018-02-05'
  })

  let report3 = await Factory.model('App/Models/Report').create({
    ...reportData,
    ...buildLoss('chopper', [18.23, 17.64, 10.48, 7.31]),
    date: '2018-02-06'
  })

  let report4 = await Factory.model('App/Models/Report').create({
    ...reportData,
    ...buildLoss('chopper', [12.80, 11.20, 12.48, 13.31]),
    date: '2018-02-24'
  })

  let data = await AnalyticsService.averageLoss('chopper', demand.id, machines[0].id, null, null)

  let firstWeek = data.find(d => d.name === 'Semana 1')
  let lastWeek = data.find(d => d.name === 'Semana 3')

  assert.isDefined(firstWeek)
  assert.isDefined(lastWeek)
  assert.lengthOf(data, 3)
  assert.equal(firstWeek.value, 11.76)
  assert.equal(lastWeek.value, 12.45)
})

test('analytics service should return the chopper losses per week day', async({ client, assert }) => {
  let reportData = {
    machine_id: machines[0].id, 
    plot_id: plot.id,
    demand_id: demand.id
  }

  await Factory.model('App/Models/Report').create({
    ...reportData,
    ...buildLoss('chopper', [10, 13.20, null, null]),
    date: '2018-02-04'
  })

  await Factory.model('App/Models/Report').create({
    ...reportData,
    ...buildLoss('chopper', [9, null, 10.48, 11.31]),
    date: '2018-02-05'
  })

  await Factory.model('App/Models/Report').create({
    ...reportData,
    ...buildLoss('chopper', [18.23, 17.64, 10.48, 7.31]),
    date: '2018-02-06'
  })

  let data = await AnalyticsService.averageLoss('chopper', demand.id, machines[0].id, '2018-02-04', '2018-02-10')

  let sunday = data.find(d => d.name === 'DOM')
  let monday = data.find(d => d.name === 'SEG')
  let tuesday = data.find(d => d.name === 'TER')

  assert.isDefined(sunday)
  assert.isDefined(monday)
  assert.isDefined(tuesday)
  assert.lengthOf(data, 7)
  assert.equal(sunday.value, 11.6)
  assert.equal(monday.value, 10.26)
  assert.equal(tuesday.value, 13.42)
})

test('analytics service should return the platform losses weekly', async({ client, assert }) => {
  let reportData = {
    machine_id: machines[0].id, 
    plot_id: plot.id,
    demand_id: demand.id
  }

  await Factory.model('App/Models/Report').create({
    ...reportData,
    ...buildLoss('platform', [10, 13.20, null, null]),
    date: '2018-02-04'
  })

  await Factory.model('App/Models/Report').create({
    ...reportData,
    ...buildLoss('platform', [9, null, 10.48, 11.31]),
    date: '2018-02-05'
  })

  await Factory.model('App/Models/Report').create({
    ...reportData,
    ...buildLoss('platform', [18.23, 17.64, 10.48, 7.31]),
    date: '2018-02-06'
  })

  await Factory.model('App/Models/Report').create({
    ...reportData,
    ...buildLoss('platform', [12.80, 11.20, 12.48, 13.31]),
    date: '2018-02-24'
  })

  let data = await AnalyticsService.averageLoss('platform', demand.id, machines[0].id, null, null)

  let firstWeek = data.find(d => d.name === 'Semana 1')
  let lastWeek = data.find(d => d.name === 'Semana 3')

  assert.isDefined(firstWeek)
  assert.isDefined(lastWeek)
  assert.lengthOf(data, 3)
  assert.equal(firstWeek.value, 11.76)
  assert.equal(lastWeek.value, 12.45)
})

test('analytics service should return the platform losses per week day', async({ client, assert }) => {
  let reportData = {
    machine_id: machines[0].id, 
    plot_id: plot.id,
    demand_id: demand.id
  }

  await Factory.model('App/Models/Report').create({
    ...reportData,
    ...buildLoss('platform', [10, 13.20, null, null]),
    date: '2018-02-04'
  })

  await Factory.model('App/Models/Report').create({
    ...reportData,
    ...buildLoss('platform', [9, null, 10.48, 11.31]),
    date: '2018-02-05'
  })

  await Factory.model('App/Models/Report').create({
    ...reportData,
    ...buildLoss('platform', [18.23, 17.64, 10.48, 7.31]),
    date: '2018-02-06'
  })

  let data = await AnalyticsService.averageLoss('platform', demand.id, machines[0].id, '2018-02-04', '2018-02-10')

  let sunday = data.find(d => d.name === 'DOM')
  let monday = data.find(d => d.name === 'SEG')
  let tuesday = data.find(d => d.name === 'TER')

  assert.isDefined(sunday)
  assert.isDefined(monday)
  assert.isDefined(tuesday)
  assert.lengthOf(data, 7)
  assert.equal(sunday.value, 11.6)
  assert.equal(monday.value, 10.26)
  assert.equal(tuesday.value, 13.42)
})

const buildLoss = (type, values) => {
  if(type === 'platform') {
    return {
      platform_losses_1: (values[0]) ? values[0] : null,
      platform_losses_2: (values[1]) ? values[1] : null,
      platform_losses_3: (values[2]) ? values[2] : null,
      platform_losses_4: (values[3]) ? values[3] : null
    }
  } else if(type === 'chopper') {
    return {
      chopper_losses_1: (values[0]) ? values[0] : null,
      chopper_losses_2: (values[1]) ? values[1] : null,
      chopper_losses_3: (values[2]) ? values[2] : null,
      chopper_losses_4: (values[3]) ? values[3] : null
    }
  }

  return {}
}

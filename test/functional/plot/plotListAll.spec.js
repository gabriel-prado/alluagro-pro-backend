'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const Plot = use('App/Models/Plot')
const { test, trait, beforeEach } = use('Test/Suite')('Plot - GET /farm/id/plot')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultCompany = null
let defaultFarm = null
let defaultPlots = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultCompany = await Factory.model('App/Models/Company').create()
  defaultFarm = await Factory.model('App/Models/Farm').create({
    company_id: defaultCompany.id
  })
  defaultPlots = await Factory.model('App/Models/Plot').createMany(5, {
    farm_id: defaultFarm.id
  })
})

test('should retrieve plots paginated', async({ client, assert}) => {
  const response = await client.get(`farm/${defaultFarm.id}/plot`).query({ page: 3, perPage: 2 }).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.page, 3)
  assert.lengthOf(response.body.data, 1)
})

test('should retrieve plots with default pagination', async({ client, assert}) => {
  const response = await client.get(`farm/${defaultFarm.id}/plot`).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.page, 1)
  assert.lengthOf(response.body.data, 5)
})

test('should retrieve all plots', async({ client, assert}) => {
  const response = await client.get(`farm/${defaultFarm.id}/plot`).query({ all: true }).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 5)
})

test('should not list plots for not logged user', async({ client, assert}) => {
  const response = await client.get(`farm/${defaultFarm.id}/plot`).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.get(`farm/${defaultFarm.id}/plot`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.get(`farm/${defaultFarm.id}/plot`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

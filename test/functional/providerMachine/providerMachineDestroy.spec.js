'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const Machine = use('App/Models/Machine')
const { test, trait, beforeEach } = use('Test/Suite')('Provider Machine - DELETE /provider/id/machine/id')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultMachine = null
let defaultProvider = null
let defaultMachineCategory = null
let defaultMachineType = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultProvider = await Factory.model('App/Models/Provider').create()
  defaultMachineCategory = await Factory.model('App/Models/MachineCategory').create()
  defaultMachineType = await Factory.model('App/Models/MachineType').create({
    machineCategory_id: defaultMachineCategory.id
  })
  defaultMachine = await Factory.model('App/Models/Machine').create({
    machineCategory_id: defaultMachineCategory.id,
    machineType_id: defaultMachineType.id,
    provider_id: defaultProvider.id
  })
})

test('should destroy machine by id', async ({ client, assert }) => {
  const response = await client.delete(`provider/${defaultProvider.id}/machine/${defaultMachine.id}`).loginVia(defaultUser).end()

  assert.equal(response.status, 204)
  const machineInDatabase =  await Machine.find(defaultMachine.id)
  assert.isNull(machineInDatabase)
})

test('should try to destroy non-existent machine and return status 404', async ({ client, assert }) => {
  const response = await client.delete(`provider/${defaultProvider.id}/machine/131`).loginVia(defaultUser).end()

  assert.equal(response.status, 404)
  const machineInDatabase =  await Machine.find(defaultMachine.id)
  assert.isNotNull(machineInDatabase)
})

test('should try to destroy machine with reports and return status 500', async ({ client, assert }) => {
  let company = await Factory.model('App/Models/Company').create()
  let farm = await Factory.model('App/Models/Farm').create({
    company_id: company.id
  })
  let plot = await Factory.model('App/Models/Plot').create({
    farm_id: farm.id
  })
  let demand = await Factory.model('App/Models/Demand').create({
    company_id: company.id,
    farm_id: farm.id
  })
  await Factory.model('App/Models/Report').createMany(10, {
    demand_id: demand.id,
    plot_id: plot.id,
    machine_id: defaultMachine.id
  })

  const response = await client.delete(`provider/${defaultProvider.id}/machine/${defaultMachine.id}`).loginVia(defaultUser).end()

  assert.equal(response.status, 500)
  assert.equal(response.body.code, 'ER_ROW_IS_REFERENCED')
  assert.equal(response.body.table, 'REPORTS')
  const machineInDatabase =  await Machine.find(defaultMachine.id)
  assert.isNotNull(machineInDatabase)
})

test('should try destroy machine without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.delete(`provider/${defaultProvider.id}/machine/${defaultMachine.id}`).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.delete(`provider/1/machine/1`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.delete(`provider/1/machine/1`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

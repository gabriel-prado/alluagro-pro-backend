'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Provider Machine - GET /provider/id/machine/id')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultProvider = null
let defaultMachineType = null
let defaultMachineCategory = null
let defaultMachine = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultProvider = await Factory.model('App/Models/Provider').create()
  defaultMachineCategory = await Factory.model('App/Models/MachineCategory').create()
  defaultMachineType = await Factory.model('App/Models/MachineType').create({
    machineCategory_id: defaultMachineCategory.id
  })
  defaultMachine = await Factory.model('App/Models/Machine').create({
    machineCategory_id: defaultMachineCategory.id,
    machineType_id: defaultMachineType.id,
    provider_id: defaultProvider.id
  })
})

test('should not show machine for not logged user', async ({ client, assert }) => {
  const response =
    await client
      .get(`provider/${defaultProvider.id}/machine/${defaultMachine.id}`)
      .end()

  assert.equal(response.status, 401)
})

test('should return machine and status 200', async({ client, assert}) => {
  const response =
    await client
      .get(`provider/${defaultProvider.id}/machine/${defaultMachine.id}`)
      .loginVia(defaultUser)
      .end()

  assert.equal(response.status, 200)
  assert.include(response.body, defaultMachine.toJSON())
})

test('should get 404 for a non existent machine', async({ client, assert}) => {
  const response =
    await client
      .get(`provider/${defaultProvider.id}/machine/777`)
      .loginVia(defaultUser)
      .end()

  assert.equal(response.status, 404)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.get(`provider/1/machine/1`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.get(`provider/1/machine/1`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

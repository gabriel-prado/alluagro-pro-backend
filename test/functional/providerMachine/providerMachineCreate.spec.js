'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Provider Machine - POST /provider/id/machine')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultProvider = null
let defaultMachineType = null
let defaultMachineCategory = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultProvider = await Factory.model('App/Models/Provider').create()
  defaultMachineCategory = await Factory.model('App/Models/MachineCategory').create()
  defaultMachineType = await Factory.model('App/Models/MachineType').create({
    machineCategory_id: defaultMachineCategory.id
  })
})

test('should store machine and retrieve 201', async ({ client, assert }) => {
  const machine = await Factory.model('App/Models/Machine').make({
    machineCategory_id: defaultMachineCategory.id,
    machineType_id: defaultMachineType.id,
    provider_id: defaultProvider.id
  })

  let machineJson = machine.toJSON()

  const response = await client.post(`provider/${defaultProvider.id}/machine`).loginVia(defaultUser).send(machineJson).end()

  assert.equal(response.status, 201)
  assert.equal(response.body.brand, machine.brand)
  assert.equal(response.body.provider_id, machine.provider_id)
  assert.equal(response.body.description, machine.description)
})

test('should store invalid machine and retrieve 400', async ({ client, assert }) => {
  const machine = await Factory.model('App/Models/Machine').make({
    machineCategory_id: defaultMachineCategory.id,
    machineType_id: defaultMachineType.id,
    provider_id: defaultProvider.id
  })

  let machineJson = machine.toJSON()
  delete machineJson.machineType_id

  const response = await client.post(`provider/${defaultProvider.id}/machine`).loginVia(defaultUser).send(machineJson).end()

  assert.equal(response.status, 400)
})

test('should store machine without authenticate and retrieve 401', async ({ client, assert }) => {
  const machine = await Factory.model('App/Models/Machine').make({
    machineCategory_id: defaultMachineCategory.id,
    machineType_id: defaultMachineType.id,
    provider_id: defaultProvider.id
  })

  let machineJson = machine.toJSON()

  const response = await client.post(`provider/${defaultProvider.id}/machine`).send(machineJson).end()

  assert.equal(response.status, 401)
})

test('should store invalid machine without harvester field and retrieve 400', async ({ client, assert }) => {
  let category = await Factory.model('App/Models/MachineCategory').create({
    fields: 'HARVESTER'
  })
  let type = await Factory.model('App/Models/MachineType').create({
    machineCategory_id: category.id
  })
  const machine = await Factory.model('App/Models/Machine').make({
    machineCategory_id: category.id,
    machineType_id: type.id,
    provider_id: defaultProvider.id
  })

  let machineJson = machine.toJSON()
  delete machineJson.harvester_brand

  const response = await client.post(`provider/${defaultProvider.id}/machine`).loginVia(defaultUser).send(machineJson).end()

  assert.equal(response.status, 400)
  assert.equal(response.body[0].validation, 'machineFields')
  assert.equal(response.body[0].field, 'harvester_brand')
})

test('should store machine with photos and retrieve 201', async ({ client, assert }) => {
  const machine = await Factory.model('App/Models/Machine').make({
    machineCategory_id: defaultMachineCategory.id,
    machineType_id: defaultMachineType.id,
    provider_id: defaultProvider.id
  })
  const machinePhotos = await Factory.model('App/Models/MachinePhoto').makeMany(3, {
    machine_id: machine.id
  })

  let machineJson = machine.toJSON()
  machineJson.machinePhotos = machinePhotos.map(p => p.toJSON())

  const response = await client.post(`provider/${defaultProvider.id}/machine`).loginVia(defaultUser).send(machineJson).end()

  assert.equal(response.status, 201)
  assert.lengthOf(response.body.machinePhotos, 3)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.post(`provider/${defaultProvider.id}/machine`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.post(`provider/${defaultProvider.id}/machine`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

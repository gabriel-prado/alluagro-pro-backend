'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const Machine = use('App/Models/Machine')
const MachinePhoto = use('App/Models/MachinePhoto')
const { test, trait, beforeEach } = use('Test/Suite')('Machine - PUT /provider/id/machine/id')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultProvider = null
let defaultMachineType = null
let defaultMachineCategory = null
let defaultMachine = null
let defaultMachinePhotos = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultProvider = await Factory.model('App/Models/Provider').create()
  defaultMachineCategory = await Factory.model('App/Models/MachineCategory').create()
  defaultMachineType = await Factory.model('App/Models/MachineType').create({
    machineCategory_id: defaultMachineCategory.id
  })
  defaultMachine = await Factory.model('App/Models/Machine').create({
    machineCategory_id: defaultMachineCategory.id,
    machineType_id: defaultMachineType.id,
    provider_id: defaultProvider.id
  })
  defaultMachinePhotos = await Factory.model('App/Models/MachinePhoto').createMany(2, {
    machine_id: defaultMachine.id
  })
})

test('should update machine and return 200', async({ client, assert }) => {
  let machineJson = defaultMachine.toJSON()
  machineJson.brand = 'machine_new_brand'
  machineJson.year = 2657

  const response =
    await client
      .put(`provider/${defaultProvider.id}/machine/${defaultMachine.id}`)
      .loginVia(defaultUser)
      .send(machineJson)
      .end()

  assert.equal(response.status, 200)
  const machineInDatabase =  await Machine.find(response.body.id)
  assert.isNotNull(machineInDatabase)
  assert.containsAllKeys(response.body, ['id', 'provider_id', 'created_at', 'updated_at'])
  assert.equal(machineInDatabase.provider_id, machineJson.provider_id)
  assert.equal(machineInDatabase.year, machineJson.year)
})

test('should try update machine and return 404', async ({ client, assert }) => {
  const response =
    await client
      .put(`provider/${defaultProvider.id}/machine/453`)
      .loginVia(defaultUser)
      .send(defaultMachine.toJSON())
      .end()

  assert.equal(response.status, 404)
})

test('should not update machine for not logged user', async({ client, assert}) => {
  const response =
    await client
      .put(`provider/${defaultProvider.id}/machine/${defaultMachine.id}`)
      .end()

  assert.equal(response.status, 401)
})

test('should update machine with photos and return 200', async({ client, assert }) => {
  let machineJson = defaultMachine.toJSON()
  machineJson.brand = 'machine_new_brand'
  let machinePhotoJson = defaultMachinePhotos[0].toJSON()
  machinePhotoJson.photo_url = "photo_new_url"
  machineJson.machinePhotos = [
    machinePhotoJson
  ]

  const response =
    await client.put(`provider/${defaultProvider.id}/machine/${defaultMachine.id}`)
    .loginVia(defaultUser)
    .send(machineJson)
    .end()

  assert.equal(response.status, 200)
  const machineInDatabase =  await Machine.find(response.body.id)
  assert.isNotNull(machineInDatabase)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  assert.equal(machineInDatabase.brand, machineJson.brand)
  const photoInDatabase =  await MachinePhoto.find(machinePhotoJson.id)
  assert.isNotNull(photoInDatabase)
  assert.containsAllKeys(response.body.machinePhotos[0], ['id', 'photo_url', 'created_at', 'updated_at'])
  assert.equal(photoInDatabase.photo_url, machinePhotoJson.photo_url)
})

test('should update machine without photos and return 200', async({ client, assert }) => {
  let machineJson = defaultMachine.toJSON()
  machineJson.brand = 'machine_new_brand'
  machineJson.machinePhotos = []

  const response =
    await client.put(`provider/${defaultProvider.id}/machine/${defaultMachine.id}`)
    .loginVia(defaultUser)
    .send(machineJson)
    .end()

  assert.equal(response.status, 200)
  const machineInDatabase =  await Machine.find(response.body.id)
  assert.isNotNull(machineInDatabase)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  assert.equal(machineInDatabase.brand, machineJson.brand)
  const photoInDatabase =  await MachinePhoto.findBy('machine_id', machineJson.id)
  assert.isNull(photoInDatabase)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.put(`provider/1/machine/1`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.put(`provider/1/machine/1`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

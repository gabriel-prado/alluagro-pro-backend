'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Provider Machine - GET /provider/id/machine')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultProvider = null
let defaultMachineType = null
let defaultMachineCategory = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultProvider = await Factory.model('App/Models/Provider').create()
  defaultMachineCategory = await Factory.model('App/Models/MachineCategory').create()
  defaultMachineType = await Factory.model('App/Models/MachineType').create({
    machineCategory_id: defaultMachineCategory.id
  })
})

test('should not list machines for not logged user', async ({ client, assert }) => {
  const response = await client.get(`provider/${defaultProvider.id}/machine`).end()
  assert.equal(response.status, 401)
})

test('should list machines with default pagination', async ({ client, assert }) => {
  const machines = await Factory.model('App/Models/Machine').createMany(6, {
    machineCategory_id: defaultMachineCategory.id,
    machineType_id: defaultMachineType.id,
    provider_id: defaultProvider.id
  })

  const response = await client.get(`provider/${defaultProvider.id}/machine`).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 5)
})

test('should list machines with all', async ({ client, assert }) => {
  const machines = await Factory.model('App/Models/Machine').createMany(6, {
    machineCategory_id: defaultMachineCategory.id,
    machineType_id: defaultMachineType.id,
    provider_id: defaultProvider.id
  })

  const response = await client.get(`provider/${defaultProvider.id}/machine`).query({ all: true }).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 6)
})

test('should list machines using pagination', async ({ client, assert }) => {
  const machines = await Factory.model('App/Models/Machine').createMany(5, {
    machineCategory_id: defaultMachineCategory.id,
    machineType_id: defaultMachineType.id,
    provider_id: defaultProvider.id
  })

  const response = await client.get(`provider/${defaultProvider.id}/machine`).query({ page: 3, perPage: 2 }).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 1)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.get(`provider/1/machine`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.get(`provider/1/machine`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

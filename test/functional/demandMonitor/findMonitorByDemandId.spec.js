'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Monitor - GET /demand/id/monitor')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultMonitor = null
let defaultAdmin = null
let defaultCompany = null
let defaultFarm = null
let defaultDemand = null

beforeEach(async () => {
  await clearDataBase()

  defaultMonitor = await Factory.model('App/Models/User').create({
    role: 'MONITOR'
  })

  defaultAdmin = await Factory.model('App/Models/User').create({
    role: 'ADMIN'
  })

  defaultCompany = await Factory.model('App/Models/Company').create()

  defaultFarm = await Factory.model('App/Models/Farm').create({
    company_id: defaultCompany.id
  })

  defaultDemand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: defaultMonitor.id
  })
})

test('should retrieve existent user', async({ client, assert}) => {
  const resourceUrl = `/demand/${defaultDemand.id}/monitor`

  const response = await client.get(resourceUrl).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.id, defaultMonitor.id)
})

test('should get 404 for a non existent demand', async({ client, assert}) => {
  const resourceUrl = `/demand/777/monitor`

  const response = await client.get(resourceUrl).loginVia(defaultAdmin).end()

  assert.equal(response.status, 404)
})

test('should not get monitor for not logged user', async({ client, assert}) => {
  const resourceUrl = `/demand/${defaultDemand.id}/monitor`

  const response = await client.get(resourceUrl).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.get(`demand/1/monitor`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.get(`demand/1/monitor`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Machine Photo - GET /machine/id/machine-photo')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultProvider = null
let defaultMachineCategory = null
let defaultMachineType = null
let defaultMachine = null
let defaultMachinePhotos = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create()
  defaultProvider = await Factory.model('App/Models/Provider').create()
  defaultMachineCategory = await Factory.model('App/Models/MachineCategory').create({
    fields: 'COMMON'
  })
  defaultMachineType = await Factory.model('App/Models/MachineType').create({
    machineCategory_id: defaultMachineCategory.id
  })
  defaultMachine = await Factory.model('App/Models/Machine').create({
    machineCategory_id: defaultMachineCategory.id,
    machineType_id: defaultMachineType.id,
    provider_id: defaultProvider.id
  })
  defaultMachinePhotos = await Factory.model('App/Models/MachinePhoto').createMany(5, {
    machine_id: defaultMachine.id
  })
})

test('should retrieve photos paginated', async({ client, assert}) => {
  const response = await client.get(`machine/${defaultMachine.id}/machine-photo`).query({ page: 3, perPage: 2 }).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.page, 3)
  assert.lengthOf(response.body.data, 1)
})

test('should retrieve photos with default pagination', async({ client, assert}) => {
  const response = await client.get(`machine/${defaultMachine.id}/machine-photo`).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.page, 1)
  assert.lengthOf(response.body.data, 5)
})

test('should retrieve all photos', async({ client, assert}) => {
  const response = await client.get(`machine/${defaultMachine.id}/machine-photo`).query({ all: true }).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 5)
})

test('should not list photos for not logged user', async({ client, assert}) => {
  const response = await client.get(`machine/${defaultMachine.id}/machine-photo`).end()

  assert.equal(response.status, 401)
})

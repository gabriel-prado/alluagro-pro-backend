'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Company - POST /company')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
})

test('should create a company and retrieve 201', async ({ client, assert }) => {
  const company = await Factory.model('App/Models/Company').make()
  const companyJson = company.toJSON()

  const response = await client.post(`company`).loginVia(defaultUser).send(companyJson).end()

  assert.equal(response.status, 201)
  assert.equal(response.body.name, company.name)
  assert.equal(response.body.cnpj, company.cnpj)
  assert.equal(response.body.phone, company.phone)
})

test('should store invalid company and retrieve 400', async ({ client, assert }) => {
  const company = await Factory.model('App/Models/Company').make()
  let companyJson = company.toJSON()
  delete companyJson.name

  const response = await client.post('company').loginVia(defaultUser).send(companyJson).end()

  assert.equal(response.status, 400)
})

test('should store company without authenticate and retrieve 401', async ({ client, assert }) => {
  const company = await Factory.model('App/Models/Company').make()
  const companyJson = company.toJSON()

  const response = await client.post('company').send(companyJson).end()

  assert.equal(response.status, 401)
})

test('should create a company without address retrieve 201', async ({ client, assert }) => {
  const company = await Factory.model('App/Models/Company').make()
  const json = company.toJSON()
  delete json.zip_code
  delete json.state
  delete json.city
  delete json.district
  delete json.street
  delete json.number
  delete json.complement

  const response = await client.post(`company`).loginVia(defaultUser).send(json).end()

  assert.equal(response.status, 201)
  assert.equal(response.body.name, company.name)
  assert.equal(response.body.cnpj, company.cnpj)
  assert.equal(response.body.phone, company.phone)
})

test('should create a company default photo_url', async ({ client, assert }) => {
  const company = await Factory.model('App/Models/Company').make()
  const json = company.toJSON()
  delete json.photo_url

  const createResponse = await client.post(`company`).loginVia(defaultUser).send(json).end()
  const fetchResponse = await client.get(`company/${createResponse.body.id}`).loginVia(defaultUser).end()

  assert.equal(createResponse.status, 201)
  assert.equal(fetchResponse.body.photo_url, 'assets/img/no-image.png')
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.post('company').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.post('company').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

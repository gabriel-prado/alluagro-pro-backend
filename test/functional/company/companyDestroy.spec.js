'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const Company = use('App/Models/Company')
const { test, trait, beforeEach } = use('Test/Suite')('Company - DELETE /company/id')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultCompany = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultCompany = await Factory.model('App/Models/Company').create()
})

test('should destroy company by id', async ({ client, assert }) => {
  const response = await client.delete(`company/${defaultCompany.id}`).loginVia(defaultUser).end()

  assert.equal(response.status, 204)
  const companyInDatabase =  await Company.find(defaultCompany.id)
  assert.isNull(companyInDatabase)
})

test('should try to destroy non-existent company and return status 404', async ({ client, assert }) => {
  const response = await client.delete(`company/131`).loginVia(defaultUser).end()

  assert.equal(response.status, 404)
  const companyInDatabase =  await Company.find(defaultCompany.id)
  assert.isNotNull(companyInDatabase)
})

test('should try to destroy company referenced and return status 500', async ({ client, assert }) => {
  const monitor = await Factory.model('App/Models/User').create({
    role: 'MONITOR',
    company_id: defaultCompany.id
  })

  const response = await client.delete(`company/${defaultCompany.id}`).loginVia(defaultUser).end()

  assert.equal(response.status, 500)
  assert.equal(response.body.code, 'ER_ROW_IS_REFERENCED')
  assert.equal(response.body.table, 'USERS')
  const companyInDatabase =  await Company.find(defaultCompany.id)
  assert.isNotNull(companyInDatabase)
})

test('should try destroy company without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.delete(`company/${defaultCompany.id}`).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.delete(`company/${defaultCompany.id}`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.delete(`company/${defaultCompany.id}`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Company - GET /company/id')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultCompany = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultCompany = await Factory.model('App/Models/Company').create()
  defaultCompany = defaultCompany.toJSON()
})

test('should get company by id', async ({ client, assert }) => {
  const response = await client.get(`company/${defaultCompany.id}`).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.include(response.body, defaultCompany)
})

test('should not get company by id', async ({ client, assert }) => {
  const response = await client.get(`company/3`).loginVia(defaultUser).end()
  assert.equal(response.status, 404)
})

test('should not get company for not logged user', async ({ client, assert }) => {
  const response = await client.get(`company/${defaultCompany.id}`).end()
  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.get(`company/${defaultCompany.id}`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.get(`company/${defaultCompany.id}`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

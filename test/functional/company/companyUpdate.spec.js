'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Company - PUT /company/id')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultCompany = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultCompany = await Factory.model('App/Models/Company').create()
})

test('should update a company and retrieve 200', async ({ client, assert }) => {
  let companyJson = defaultCompany.toJSON()
  companyJson.name = 'company_new_name'
  companyJson.cnpj = '1234567890'

  const response =
    await client
      .put(`company/${defaultCompany.id}`)
      .loginVia(defaultUser)
      .send(companyJson)
      .end()

  assert.equal(response.status, 200)
  assert.containsAllKeys(response.body, ['id', 'updated_at', 'created_at'])
  assert.equal(response.body.name, companyJson.name)
  assert.equal(response.body.cnpj, companyJson.cnpj)
})

test('should try update invalid company and retrieve 400', async ({ client, assert }) => {
  let companyJson = defaultCompany.toJSON()
  companyJson.type = 'WRONG'

  const response =
    await client
      .put(`company/${defaultCompany.id}`)
      .loginVia(defaultUser)
      .send(companyJson)
      .end()

  assert.equal(response.status, 400)
  assert.equal(response.body[0].field, 'type')
})

test('should try update company without authenticate and retrieve 401', async ({ client, assert }) => {
  const companyJson = defaultCompany.toJSON()

  const response =
    await client
      .put(`company/${defaultCompany.id}`)
      .send(companyJson)
      .end()

  assert.equal(response.status, 401)
})

test('should try to update a nonexistent company and return 404', async ({ client, assert }) => {
  let companyJson = defaultCompany.toJSON()
  companyJson.name = 'company_new_name'

  const response =
    await client
      .put(`company/777`)
      .loginVia(defaultUser)
      .send(companyJson)
      .end()

  assert.equal(response.status, 404)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.put(`company/${defaultCompany.id}`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.put(`company/${defaultCompany.id}`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

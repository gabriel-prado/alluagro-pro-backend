'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Company - GET /company')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
})

test('should deny', async ({ client, assert }) => {
  const response = await client.get('company').end()
  assert.equal(response.status, 401)
})

test('should list companies with default pagination', async ({ client, assert }) => {
  const companies = await Factory.model('App/Models/Company').createMany(6)

  const response = await client.get('company').loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 5)
})

test('should list companies with all', async ({ client, assert }) => {
  const companies = await Factory.model('App/Models/Company').createMany(6)

  const response = await client.get('company').query({ all: true }).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 6)
})

test('should list companies using pagination', async ({ client, assert }) => {
  const companies = await Factory.model('App/Models/Company').createMany(5)

  const response = await client.get('company').query({ page: 3, perPage: 2 }).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 1)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.get(`company`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.get(`company`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const DemandObservation = use('App/Models/DemandObservation')
const { test, trait, beforeEach } = use('Test/Suite')('Demand Observation - POST /demand/id/observation')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultMonitor = null
let defaultCompany = null
let defaultFarm = null
let defaultDemand = null

beforeEach(async () => {
  await clearDataBase()
  defaultMonitor = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  defaultCompany = await Factory.model('App/Models/Company').create()
  defaultFarm = await Factory.model('App/Models/Farm').create({
    company_id: defaultCompany.id
  })
  defaultDemand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: defaultMonitor.id
  })
})

test('should save observation and return 201', async ({ client, assert }) => {
  const demandObservation = await Factory.model('App/Models/DemandObservation').make({
    demand_id: defaultDemand.id
  })

  const response = await client.post(`demand/${defaultDemand.id}/observation`).loginVia(defaultMonitor).send(demandObservation.toJSON()).end()

  assert.equal(response.status, 201)
  const demandObservationInDatabase =  await DemandObservation.find(response.body.id)
  assert.isNotNull(demandObservationInDatabase)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
})

test('should try save observation and return 401', async ({ client, assert }) => {
  const response = await client.post(`demand/${defaultDemand.id}/observation`).end()

  assert.equal(response.status, 401)
})

test('should try save observation and return 400', async ({ client, assert }) => {
  const demandObservation = await Factory.model('App/Models/DemandObservation').make({
    demand_id: defaultDemand.id
  })
  let demandObservationJson = demandObservation.toJSON()
  delete demandObservationJson.date

  const response = await client.post(`demand/${defaultDemand.id}/observation`).loginVia(defaultMonitor).send(demandObservationJson).end()

  assert.equal(response.status, 400)
})

test('should get a forbidden to incorrect monitor', async({ client, assert}) => {
  const monitor = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const demandObservation = await Factory.model('App/Models/DemandObservation').make({
    demand_id: defaultDemand.id
  })
  const demandObservationJson = demandObservation.toJSON()
  const response = await client.post(`demand/${defaultDemand.id}/observation`).loginVia(monitor).send(demandObservationJson).end()

  assert.equal(response.status, 403)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.post(`demand/${defaultDemand.id}/observation`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  const response = await client.post(`demand/${defaultDemand.id}/observation`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

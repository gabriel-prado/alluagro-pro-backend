'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const DemandObservation = use('App/Models/DemandObservation')
const { test, trait, beforeEach } = use('Test/Suite')('Demand Observation - GET /demand/id/observation?date')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultAdmin = null
let defaultMonitor = null
let defaultManager = null
let defaultCompany = null
let defaultFarm = null
let defaultDemand = null
let defaultDemandObservation = null

beforeEach(async () => {
  await clearDataBase()
  defaultAdmin = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultMonitor = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  defaultCompany = await Factory.model('App/Models/Company').create()
  defaultManager = await Factory.model('App/Models/User').create({ role: 'MANAGER', company_id: defaultCompany.id })
  defaultFarm = await Factory.model('App/Models/Farm').create({
    company_id: defaultCompany.id
  })
  defaultDemand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: defaultMonitor.id
  })
  defaultDemandObservation = await Factory.model('App/Models/DemandObservation').createMany(10, {
    demand_id: defaultDemand.id,
    date: '2017-01-04'
  })
})

test('should retrieve observations paginated to admin', async({ client, assert }) => {
  const response = await client.get(`demand/${defaultDemand.id}/observation`).query({ page: 4, perPage: 3, date: '2017-01-04' }).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.page, 4)
  assert.lengthOf(response.body.data, 1)
  assert.equal(response.body.data[0].date, '2017-01-04')
  assert.include(response.body.data[0], defaultDemandObservation[9].toJSON())
})

test('should retrieve observations paginated to manager', async({ client, assert }) => {
  const response = await client.get(`demand/${defaultDemand.id}/observation`).query({ page: 4, perPage: 3, date: '2017-01-04' }).loginVia(defaultManager).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.page, 4)
  assert.lengthOf(response.body.data, 1)
  assert.equal(response.body.data[0].date, '2017-01-04')
  assert.include(response.body.data[0], defaultDemandObservation[9].toJSON())
})

test('should retrieve observations paginated to monitor', async({ client, assert }) => {
  const response = await client.get(`demand/${defaultDemand.id}/observation`).query({ page: 4, perPage: 3, date: '2017-01-04' }).loginVia(defaultMonitor).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.page, 4)
  assert.lengthOf(response.body.data, 1)
  assert.equal(response.body.data[0].date, '2017-01-04')
  assert.include(response.body.data[0], defaultDemandObservation[9].toJSON())
})

test('should retrieve observations with default pagination to monitor', async({ client, assert }) => {
  const response = await client.get(`demand/${defaultDemand.id}/observation`).query({ date: '2017-01-04' }).loginVia(defaultMonitor).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.page, 1)
  assert.lengthOf(response.body.data, 5)
  assert.equal(response.body.data[0].date, '2017-01-04')
  assert.include(response.body.data[0], defaultDemandObservation[0].toJSON())
})

test('should retrieve observations with default pagination to admin', async({ client, assert }) => {
  const response = await client.get(`demand/${defaultDemand.id}/observation`).query({ date: '2017-01-04' }).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.page, 1)
  assert.lengthOf(response.body.data, 5)
  assert.equal(response.body.data[0].date, '2017-01-04')
  assert.include(response.body.data[0], defaultDemandObservation[0].toJSON())
})

test('should retrieve observations with default pagination to manager', async({ client, assert }) => {
  const response = await client.get(`demand/${defaultDemand.id}/observation`).query({ date: '2017-01-04' }).loginVia(defaultManager).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.page, 1)
  assert.lengthOf(response.body.data, 5)
  assert.equal(response.body.data[0].date, '2017-01-04')
  assert.include(response.body.data[0], defaultDemandObservation[0].toJSON())
})

test('should retrieve all observations to admin', async({ client, assert }) => {
  const response = await client.get(`demand/${defaultDemand.id}/observation`).query({ all: true, date: '2017-01-04' }).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 10)
})

test('should retrieve all observations to monitor', async({ client, assert }) => {
  const response = await client.get(`demand/${defaultDemand.id}/observation`).query({ all: true, date: '2017-01-04' }).loginVia(defaultMonitor).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 10)
})

test('should retrieve all observations to manager', async({ client, assert }) => {
  const response = await client.get(`demand/${defaultDemand.id}/observation`).query({ all: true, date: '2017-01-04' }).loginVia(defaultManager).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 10)
})

test('should get a forbidden to incorrect monitor', async({ client, assert}) => {
  let monitor = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.get(`demand/${defaultDemand.id}/observation`).loginVia(monitor).end()

  assert.equal(response.status, 403)
})

test('should not list observations for not logged user', async({ client, assert }) => {
  const response = await client.get(`demand/${defaultDemand.id}/observation`).end()

  assert.equal(response.status, 401)
})

test('should get a forbidden to incorrect manager', async({ client, assert}) => {
  const managerUser = await Factory.model('App/Models/User').create({
    role: 'MANAGER'
  })

  const response = await
    client
      .get(`demand/${defaultDemand.id}/observation`)
      .loginVia(managerUser)
      .end()

  assert.equal(response.status, 403)
})

test('should retrieve all observations to correct manager', async({ client, assert}) => {
  const response = await
    client
      .get(`demand/${defaultDemand.id}/observation`)
      .query({ all: true, date: '2017-01-04' })
      .loginVia(defaultManager)
      .end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 10)
})

'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Provider - GET /provider/id')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
})

test('should get provider by id', async ({ client, assert }) => {
  const provider = await Factory.model('App/Models/Provider').create()

  const response = await client.get(`provider/${provider.id}`).loginVia(defaultUser).end()
  let body = response.body
  body.employees_are_clt = Boolean(response.body.employees_are_clt)

  assert.equal(response.status, 200)
  assert.include(body, provider.toJSON())
})

test('should not get provider by id', async ({ client, assert }) => {
  const response = await client.get(`provider/3`).loginVia(defaultUser).end()
  assert.equal(response.status, 404)
})

test('should not get provider for not logged user', async ({ client, assert }) => {
  const provider = await Factory.model('App/Models/Provider').create()

  const response = await client.get(`provider/${provider.id}`).end()
  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.get(`provider/1`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.get(`provider/1`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

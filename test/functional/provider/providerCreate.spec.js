'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Provider - POST /provider')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
})

test('should create a provider and retrieve 201', async ({ client, assert }) => {
  const provider = await Factory.model('App/Models/Provider').make()
  const providerJson = provider.toJSON()
  delete providerJson.email

  const response = await client.post(`provider`).loginVia(defaultUser).send(providerJson).end()

  assert.equal(response.status, 201)
  assert.equal(response.body.name, provider.name)
  assert.equal(response.body.cnpj, provider.cnpj)
  assert.equal(response.body.photo_url, provider.photo_url)
  assert.equal(response.body.trading_name, provider.trading_name)
  assert.equal(response.body.phone, provider.phone)
})

test('should store invalid provider and retrieve 400', async ({ client, assert }) => {
  const provider = await Factory.model('App/Models/Provider').make()
  let providerJson = provider.toJSON()
  delete providerJson.name

  const response = await client.post('provider').loginVia(defaultUser).send(providerJson).end()

  assert.equal(response.status, 400)
})

test('should store provider without authenticate and retrieve 401', async ({ client, assert }) => {
  const provider = await Factory.model('App/Models/Provider').make()
  const providerJson = provider.toJSON()

  const response = await client.post('provider').send(providerJson).end()

  assert.equal(response.status, 401)
})

test('should create a provider without address retrieve 201', async ({ client, assert }) => {
  const provider = await Factory.model('App/Models/Provider').make()
  const json = provider.toJSON()
  delete json.zip_code
  delete json.state
  delete json.city
  delete json.district
  delete json.street
  delete json.number
  delete json.complement

  const response = await client.post(`provider`).loginVia(defaultUser).send(json).end()

  assert.equal(response.status, 201)
  assert.equal(response.body.name, provider.name)
  assert.equal(response.body.cnpj, provider.cnpj)
  assert.equal(response.body.phone, provider.phone)
})

test('should create a provider default photo_url', async ({ client, assert }) => {
  const provider = await Factory.model('App/Models/Provider').make()
  const json = provider.toJSON()
  delete json.photo_url

  const createResponse = await client.post(`provider`).loginVia(defaultUser).send(json).end()
  const fetchResponse = await client.get(`provider/${createResponse.body.id}`).loginVia(defaultUser).end()

  assert.equal(createResponse.status, 201)
  assert.equal(fetchResponse.body.photo_url, 'assets/img/no-image.png')
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.post(`provider`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.post(`provider`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

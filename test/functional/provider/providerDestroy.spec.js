'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const Provider = use('App/Models/Provider')
const { test, trait, beforeEach } = use('Test/Suite')('Provider - DELETE /provider/id')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultProvider = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultProvider = await Factory.model('App/Models/Provider').create()
})

test('should destroy provider by id', async ({ client, assert }) => {
  const response = await client.delete(`provider/${defaultProvider.id}`).loginVia(defaultUser).end()

  assert.equal(response.status, 204)
  const providerInDatabase =  await Provider.find(defaultProvider.id)
  assert.isNull(providerInDatabase)
})

test('should try to destroy non-existent provider and return status 404', async ({ client, assert }) => {
  const response = await client.delete(`provider/131`).loginVia(defaultUser).end()

  assert.equal(response.status, 404)
  const providerInDatabase =  await Provider.find(defaultProvider.id)
  assert.isNotNull(providerInDatabase)
})

test('should try to destroy provider referenced and return status 500', async ({ client, assert }) => {
  let machineCategory = await Factory.model('App/Models/MachineCategory').create()
  let machineType = await Factory.model('App/Models/MachineType').create({
    machineCategory_id: machineCategory.id
  })
  let machine = await Factory.model('App/Models/Machine').create({
    machineCategory_id: machineCategory.id,
    machineType_id: machineType.id,
    provider_id: defaultProvider.id
  })

  const response = await client.delete(`provider/${defaultProvider.id}`).loginVia(defaultUser).end()

  assert.equal(response.status, 500)
  assert.equal(response.body.code, 'ER_ROW_IS_REFERENCED')
  assert.equal(response.body.table, 'MACHINES')
  const providerInDatabase =  await Provider.find(defaultProvider.id)
  assert.isNotNull(providerInDatabase)
})

test('should try destroy provider without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.delete(`provider/${defaultProvider.id}`).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.delete(`provider/${defaultProvider.id}`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.delete(`provider/${defaultProvider.id}`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

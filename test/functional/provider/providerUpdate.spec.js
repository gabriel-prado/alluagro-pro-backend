'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Provider - PUT /provider/id')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultProvider = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultProvider = await Factory.model('App/Models/Provider').create()
})

test('should update a provider and retrieve 200', async ({ client, assert }) => {
  let providerJson = defaultProvider.toJSON()
  providerJson.name = 'provider_new_name'
  providerJson.cnpj = '1234567890'

  const response =
    await client
      .put(`provider/${defaultProvider.id}`)
      .loginVia(defaultUser)
      .send(providerJson)
      .end()

  assert.equal(response.status, 200)
  assert.containsAllKeys(response.body, ['id', 'updated_at', 'created_at'])
  assert.equal(response.body.name, providerJson.name)
  assert.equal(response.body.cnpj, providerJson.cnpj)
})

test('should try update invalid provider and retrieve 400', async ({ client, assert }) => {
  let providerJson = defaultProvider.toJSON()
  providerJson.type = 'WRONG'

  const response =
    await client
      .put(`provider/${defaultProvider.id}`)
      .loginVia(defaultUser)
      .send(providerJson)
      .end()

  assert.equal(response.status, 400)
  assert.equal(response.body[0].field, 'type')
})

test('should try update provider without authenticate and retrieve 401', async ({ client, assert }) => {
  const providerJson = defaultProvider.toJSON()

  const response =
    await client
      .put(`provider/${defaultProvider.id}`)
      .send(providerJson)
      .end()

  assert.equal(response.status, 401)
})

test('should try to update a nonexistent provider and return 404', async ({ client, assert }) => {
  let providerJson = defaultProvider.toJSON()
  providerJson.name = 'provider_new_name'

  const response =
    await client
      .put(`provider/777`)
      .loginVia(defaultUser)
      .send(providerJson)
      .end()

  assert.equal(response.status, 404)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.put(`provider/1`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.put(`provider/1`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

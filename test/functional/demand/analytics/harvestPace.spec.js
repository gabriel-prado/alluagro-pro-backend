'use strict'

const clearDataBase = require('../../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Analytics - GET /demand/id/analytics/harvest-pace')
const { ioc } = use('@adonisjs/fold')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()

})

test('analytics controller should return the harvest pace', async({ client, assert }) => {
  let expectedBurnup = [
    { name: 'Semana 1', value: 36 },
    { name: 'Semana 2', value: 36 },
    { name: 'Semana 3', value: 36 }
  ]

  let expectedHarvestedHectares = [
    { name: 'Semana 1', value: 36.04 },
    { name: 'Semana 2', value: 65.52 },
    { name: 'Semana 3', value: 65.52 }
  ]

  ioc.fake('Services/Analytics', () => {
    return {
      burnup: () => expectedBurnup,
      harvestedHectares: () => expectedHarvestedHectares
    }
  })

  let user = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  let company = await Factory.model('App/Models/Company').create()
  let farm = await Factory.model('App/Models/Farm').create({
    company_id: company.id
  })
  let demand = await Factory.model('App/Models/Demand').create({
    company_id: company.id,
    farm_id: farm.id
  })

  const response = await client.get('demand/1/analytics/harvest-pace').loginVia(user).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 2)

  assert.equal(response.body[0].name, 'BURN_UP')
  assert.lengthOf(response.body[0].series, 3)
  assert.sameDeepOrderedMembers(response.body[0].series, expectedBurnup)

  assert.equal(response.body[1].name, 'COLHIDO')
  assert.lengthOf(response.body[1].series, 3)
  assert.sameDeepOrderedMembers(response.body[1].series, expectedHarvestedHectares)

  ioc.restore('Services/Analytics')
})

test('analytics controller should return the harvest pace to manager', async({ client, assert }) => {
  let expectedBurnup = [
    { name: 'Semana 1', value: 36 },
    { name: 'Semana 2', value: 36 },
    { name: 'Semana 3', value: 36 }
  ]

  let expectedHarvestedHectares = [
    { name: 'Semana 1', value: 36.04 },
    { name: 'Semana 2', value: 65.52 },
    { name: 'Semana 3', value: 65.52 }
  ]

  ioc.fake('Services/Analytics', () => {
    return {
      burnup: () => expectedBurnup,
      harvestedHectares: () => expectedHarvestedHectares
    }
  })

  let company = await Factory.model('App/Models/Company').create()
  let user = await Factory.model('App/Models/User').create({ role: 'MANAGER', company_id: company.id })
  let farm = await Factory.model('App/Models/Farm').create({
    company_id: company.id
  })
  let demand = await Factory.model('App/Models/Demand').create({
    company_id: company.id,
    farm_id: farm.id
  })

  const response = await client.get('demand/1/analytics/harvest-pace').loginVia(user).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 2)

  assert.equal(response.body[0].name, 'BURN_UP')
  assert.lengthOf(response.body[0].series, 3)
  assert.sameDeepOrderedMembers(response.body[0].series, expectedBurnup)

  assert.equal(response.body[1].name, 'COLHIDO')
  assert.lengthOf(response.body[1].series, 3)
  assert.sameDeepOrderedMembers(response.body[1].series, expectedHarvestedHectares)

  ioc.restore('Services/Analytics')
})

test('analytics controller should return 401 when have not the required role', async({ client, assert }) => {
  const monitor = await Factory.model('App/Models/User').create({ role: 'MONITOR'})
  const response = await client.get('demand/1/analytics/harvest-pace').loginVia(monitor).end()
  assert.equal(response.status, 401)
})

test('analytics controller should get a forbidden to incorrect manager', async({ client, assert }) => {
  let company = await Factory.model('App/Models/Company').create()
  let user = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  let farm = await Factory.model('App/Models/Farm').create({
    company_id: company.id
  })
  let demand = await Factory.model('App/Models/Demand').create({
    company_id: company.id,
    farm_id: farm.id
  })

  const response = await client.get('demand/1/analytics/harvest-pace').loginVia(user).end()

  assert.equal(response.status, 403)
})

test('analytics controller should return 401 when have no logged user', async({ client, assert }) => {
  const response = await client.get('demand/1/analytics/harvest-pace').end()
  assert.equal(response.status, 401)
})

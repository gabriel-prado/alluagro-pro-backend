'use strict'

const clearDataBase = require('../../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Analytics - GET /demand/id/analytics/percentage-harvested')
const { ioc } = use('@adonisjs/fold')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()
})

test('analytics controller should return the percentage harvested', async({ client, assert }) => {
  ioc.fake('Services/Analytics', () => {
    return {
      percentageHarvested: () => {
        return { value: 36.04, totalValue: 200 }
      }
    }
  })

  let company = await Factory.model('App/Models/Company').create()
  let user = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  let farm = await Factory.model('App/Models/Farm').create({
    company_id: company.id
  })
  let demand = await Factory.model('App/Models/Demand').create({
    company_id: company.id,
    farm_id: farm.id
  })

  const response = await client.get('demand/1/analytics/percentage-harvested').loginVia(user).end()

  assert.equal(response.status, 200)
  assert.isObject(response.body)
  assert.equal(response.body.value, 36.04)
  assert.equal(response.body.totalValue, 200)

  ioc.restore('Services/Analytics')
})

test('analytics controller should return the percentage harvested to manager', async({ client, assert }) => {
  ioc.fake('Services/Analytics', () => {
    return {
      percentageHarvested: () => {
        return { value: 36.04, totalValue: 200 }
      }
    }
  })

  let company = await Factory.model('App/Models/Company').create()
  let user = await Factory.model('App/Models/User').create({ role: 'MANAGER', company_id: company.id })
  let farm = await Factory.model('App/Models/Farm').create({
    company_id: company.id
  })
  let demand = await Factory.model('App/Models/Demand').create({
    company_id: company.id,
    farm_id: farm.id
  })

  const response = await client.get('demand/1/analytics/percentage-harvested').loginVia(user).end()

  assert.equal(response.status, 200)
  assert.isObject(response.body)
  assert.equal(response.body.value, 36.04)
  assert.equal(response.body.totalValue, 200)

  ioc.restore('Services/Analytics')
})

test('analytics controller should get a forbidden to incorrect manager', async({ client, assert }) => {
  let company = await Factory.model('App/Models/Company').create()
  let user = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  let farm = await Factory.model('App/Models/Farm').create({
    company_id: company.id
  })
  let demand = await Factory.model('App/Models/Demand').create({
    company_id: company.id,
    farm_id: farm.id
  })

  const response = await client.get('demand/1/analytics/percentage-harvested').loginVia(user).end()

  assert.equal(response.status, 403)
})

test('analytics controller should return 401 when have no logged user', async({ client, assert }) => {
  const response = await client.get('demand/1/analytics/percentage-harvested').end()
  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.get('demand/1/analytics/percentage-harvested').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

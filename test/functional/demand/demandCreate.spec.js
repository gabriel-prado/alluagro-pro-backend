'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const Demand = use('App/Models/Demand')
const DemandMachine = use('App/Models/DemandMachine')
const { test, trait, beforeEach } = use('Test/Suite')('Demand - POST /demand')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultCompany = null
let defaultFarm = null
let defaultProvider = null
let defaultMachineType = null
let defaultMachineCategory = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultCompany = await Factory.model('App/Models/Company').create()
  defaultFarm = await Factory.model('App/Models/Farm').create({
    company_id: defaultCompany.id
  })
  defaultProvider = await Factory.model('App/Models/Provider').create()
  defaultMachineCategory = await Factory.model('App/Models/MachineCategory').create()
  defaultMachineType = await Factory.model('App/Models/MachineType').create({
    machineCategory_id: defaultMachineCategory.id
  })
})

test('should save demand and return 201', async ({ client, assert }) => {
  const demand = await Factory.model('App/Models/Demand').make({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id
  })

  const response = await client.post('/demand').loginVia(defaultUser).send(demand.toJSON()).end()

  assert.equal(response.status, 201)
  const demandInDatabase =  await Demand.find(response.body.id)
  assert.isNotNull(demandInDatabase)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
})

test('should try save demand and return 401', async ({ client, assert }) => {
  const demand = await Factory.model('App/Models/Demand').make({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id
  })

  const response = await client.post('/demand').send(demand.toJSON()).end()

  assert.equal(response.status, 401)
})

test('should try save demand and return 400', async ({ client, assert }) => {
  const demand = await Factory.model('App/Models/Demand').make({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id
  })
  let demandJson = demand.toJSON()
  delete demandJson.end_date

  const response = await client.post('/demand').loginVia(defaultUser).send(demandJson).end()

  assert.equal(response.status, 400)
})

test('should store demand associated with machines and retrieve 201', async ({ client, assert}) => {
  const demand = await Factory.model('App/Models/Demand').make({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id
  })
  const machines = await Factory.model('App/Models/Machine').createMany(3, {
    machineCategory_id: defaultMachineCategory.id,
    machineType_id: defaultMachineType.id,
    provider_id: defaultProvider.id
  })


  let demandJson = demand.toJSON()
  let machinesJson = machines.map(m => {
    m.machine_code = 'code ' + m.id
    return m.toJSON()
  })

  demandJson.machines = machinesJson

  const response = await client.post('/demand').loginVia(defaultUser).send(demandJson).end()

  assert.equal(response.status, 201)
  const demandInDatabase = await Demand.find(response.body.id)
  assert.isNotNull(demandInDatabase)
  const demandMachineInDatabase = await DemandMachine.findBy('machine_code', machinesJson[0].machine_code)
  assert.isNotNull(demandMachineInDatabase)
  assert.containsAllKeys(response.body, ['id', 'machines', 'created_at', 'updated_at'])
  assert.lengthOf(response.body.machines, 3)
})

test('should store demand with monitor and retrieve 201', async ({ client, assert }) => {
  const demand = await Factory.model('App/Models/Demand').make({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id
  })
  const monitor = await Factory.model('App/Models/User').make({
    company_id: defaultCompany.id
  })

  let demandJson = demand.toJSON()
  demandJson.monitor = monitor['$attributes']

  const response = await client.post('/demand').loginVia(defaultUser).send(demandJson).end()

  assert.equal(response.status, 201)
  const demandInDatabase = await Demand.find(response.body.id)
  assert.isNotNull(demandInDatabase)
  assert.containsAllKeys(response.body, ['id', 'monitor', 'created_at', 'updated_at'])
  assert.equal(response.body.monitor.name, monitor.name)
  assert.equal(response.body.monitor.email, monitor.email)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.post('/demand').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.post('/demand').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should save demand without contracts and return 201', async ({ client, assert }) => {
  const demand = await Factory.model('App/Models/Demand').make({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id
  })

  let json = demand.toJSON()
  delete json.contracts

  const response = await client.post('/demand').loginVia(defaultUser).send(json).end()

  assert.equal(response.status, 201)
  const demandInDatabase =  await Demand.find(response.body.id)
  assert.isNotNull(demandInDatabase)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
})

'use strict'

const clearDataBase = require('../../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Machine Request - POST /demand/id/machine-request')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultCompany = null
let defaultFarm = null
let defaultDemand = null

beforeEach(async () => {
  await clearDataBase()
  defaultCompany = await Factory.model('App/Models/Company').create()
  defaultFarm = await Factory.model('App/Models/Farm').create({
    company_id: defaultCompany.id
  })
  defaultDemand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id
  })
})

test('should send request and status 204', async({ client, assert}) => {
  let user = await Factory.model('App/Models/User').create({ role: 'MANAGER', company_id: defaultCompany.id })
  const response = await client.post(`demand/${defaultDemand.id}/machine-request`).loginVia(user).end()

  assert.equal(response.status, 204)
})

test('should get a forbidden to incorrect manager', async({ client, assert }) => {
  let user = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.post(`demand/${defaultDemand.id}/machine-request`).loginVia(user).end()

  assert.equal(response.status, 403)
})

test('should not send request for not logged user', async({ client, assert}) => {
  const response = await client.post(`demand/${defaultDemand.id}/machine-request`).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.post(`demand/${defaultDemand.id}/machine-request`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  const response = await client.post(`demand/${defaultDemand.id}/machine-request`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Demand - GET /demand/id')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultCompany = null
let defaultFarm = null
let defaultDemand = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultCompany = await Factory.model('App/Models/Company').create()
  defaultFarm = await Factory.model('App/Models/Farm').create({
    company_id: defaultCompany.id
  })
  defaultDemand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id
  })
})

test('should retrieve demand and status 200', async({ client, assert}) => {

  const response = await client.get(`demand/${defaultDemand.id}`).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.deepInclude(response.body, defaultDemand.toJSON())
  assert.include(response.body.farm, defaultFarm.toJSON())
  assert.include(response.body.company, defaultCompany.toJSON())
})

test('should retrieve demand and status 200 to Manager', async({ client, assert}) => {
  let user = await Factory.model('App/Models/User').create({ role: 'MANAGER', company_id: defaultCompany.id })

  const response = await client.get(`demand/${defaultDemand.id}`).loginVia(user).end()

  assert.equal(response.status, 200)
  assert.deepInclude(response.body, defaultDemand.toJSON())
  assert.include(response.body.farm, defaultFarm.toJSON())
  assert.include(response.body.company, defaultCompany.toJSON())
})

test('should not show demand for not logged user', async({ client, assert}) => {
  const response = await client.get(`demand/${defaultDemand.id}`).end()

  assert.equal(response.status, 401)
})

test('should try to show demand that does not exist and return status 404', async({ client, assert}) => {
  const response = await client.get(`demand/99`).loginVia(defaultUser).end()

  assert.equal(response.status, 404)
})

test('should retrieve demand and status 200 to MONITOR', async ({ client, assert }) => {
  const user = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const company = await Factory.model('App/Models/Company').create()
  const farm = await Factory.model('App/Models/Farm').create({
    company_id: company.id
  })
  const demand = await Factory.model('App/Models/Demand').create({
    company_id: company.id,
    farm_id: farm.id,
    monitor_id: user.id
  })

  const response = await client.get(`demand/${demand.id}`).loginVia(user).end()

  assert.equal(response.status, 200)
  assert.deepInclude(response.body, demand.toJSON())
  assert.include(response.body.farm, farm.toJSON())
  assert.include(response.body.company, company.toJSON())
})

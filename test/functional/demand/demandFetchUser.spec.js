'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Demand - GET /demand/id/user')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultAdmin = null
let defaultManager = null
let defaultMonitor = null
let defaultCompany = null
let defaultFarm = null
let defaultDemand = null

beforeEach(async () => {
  await clearDataBase()
  defaultAdmin = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultCompany = await Factory.model('App/Models/Company').create()
  defaultManager = await Factory.model('App/Models/User').create({ role: 'MANAGER', company_id: defaultCompany.id })
  defaultFarm = await Factory.model('App/Models/Farm').create({
    company_id: defaultCompany.id
  })
  defaultMonitor = await Factory.model('App/Models/User').create({
    role: 'MONITOR'
  })
  defaultDemand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: defaultMonitor.id
  })
})

test('should get all users by demandId and return 200', async ({ client, assert }) => {
  await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  const response = await client.get(`demand/${defaultDemand.id}/user`).query({ all: true }).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 4)
  assert.equal(response.body.total, 4)
})

test('should get paginate default users by demandId and return 200', async ({ client, assert }) => {
  await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  const response = await client.get(`demand/${defaultDemand.id}/user`).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 4)
  assert.equal(response.body.total, 4)
})

test('should get users of demand by monitor and return 200', async ({ client, assert }) => {
  const response = await client.get(`demand/${defaultDemand.id}/user`).loginVia(defaultMonitor).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 3)
  assert.equal(response.body.total, 3)
})

test('should get a forbidden to incorrect monitor', async ({ client, assert }) => {
  const monitor = await Factory.model('App/Models/User').create({
    role: 'MONITOR'
  })

  const response = await client.get(`demand/${defaultDemand.id}/user`).loginVia(monitor).end()

  assert.equal(response.status, 403)
})

test('should get paginate users by demandId and return 200', async ({ client, assert }) => {
  await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  await Factory.model('App/Models/User').create({ role: 'MANAGER', company_id: defaultCompany.id })

  const response = await client.get(`demand/${defaultDemand.id}/user`).query({ page: 2 , perPage: 3 }).loginVia(defaultAdmin).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 2)
})

test('should get users by demandId and return empty array', async ({ client, assert }) => {
  const response = await client.get(`demand/500/user`).query({ all: true }).loginVia(defaultAdmin).end()

  assert.equal(response.status, 404)
})

test('should not show users for not logged user', async({ client, assert}) => {
  const response = await client.get(`demand/1/user`).end()

  assert.equal(response.status, 401)
})

test('should not get users to incorrect manager', async({ client, assert}) => {
  const manager = await Factory.model('App/Models/User').create({
    role: 'MANAGER'
  })

  const response = await client.get(`demand/${defaultDemand.id}/user`).loginVia(manager).end()

  assert.equal(response.status, 403)
})

test('should get users to correct manager', async({ client, assert}) => {
  const response = await client.get(`demand/${defaultDemand.id}/user`).query({ all: true }).loginVia(defaultManager).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 3)
})

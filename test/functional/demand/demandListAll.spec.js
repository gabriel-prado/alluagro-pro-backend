'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Demand - GET /demand')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultCompany = null
let defaultFarm = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultCompany = await Factory.model('App/Models/Company').create()
  defaultFarm = await Factory.model('App/Models/Farm').create({
    company_id: defaultCompany.id
  })
})

test('should retrieve demands paginated', async({ client, assert}) => {
  const demands = await Factory.model('App/Models/Demand').createMany(13, {
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id
  })

  const response = await client.get('/demand').query({ page: 3, perPage: 5 }).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.page, 3)
  assert.lengthOf(response.body.data, 3)
})

test('should retrieve demands with default pagination', async({ client, assert}) => {
  const demands = await Factory.model('App/Models/Demand').createMany(12, {
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id
  })

  const response = await client.get('/demand').loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.equal(response.body.page, 1)
  assert.lengthOf(response.body.data, 5)
})

test('should retrieve all demands', async({ client, assert}) => {
  const demands = await Factory.model('App/Models/Demand').createMany(10, {
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id
  })

  const response = await client.get('/demand').query({ all: true }).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 10)
})

test('should retrieve all demands to manager', async({ client, assert}) => {
  let user = await Factory.model('App/Models/User').create({ role: 'MANAGER', company_id: defaultCompany.id })
  const demands = await Factory.model('App/Models/Demand').createMany(10, {
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id
  })

  const response = await client.get('/demand').query({ all: true }).loginVia(user).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 10)
})

test('should not list demands for not logged user', async({ client, assert}) => {
  const response = await client.get('/demand').end()

  assert.equal(response.status, 401)
})

test('should retrieve all demands with company and farm', async ({ client, assert }) => {
  const demand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id
  })

  const response = await client.get('/demand').query({ all: true }).loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 1)
  assert.containsAllKeys(response.body.data[0].company, ['id', 'created_at', 'updated_at'])
  assert.containsAllKeys(response.body.data[0].farm, ['id', 'created_at', 'updated_at'])
})

test('should retrieve demands with default pagination, company and farm', async ({ client, assert }) => {
  const demand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id
  })

  const response = await client.get('/demand').loginVia(defaultUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 1)
  assert.containsAllKeys(response.body.data[0].company, ['id', 'created_at', 'updated_at'])
  assert.containsAllKeys(response.body.data[0].farm, ['id', 'created_at', 'updated_at'])
})

test('should retrieve all demands to MONITOR', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const demand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: anotherUser.id
  })
  await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id
  })

  const response = await client.get('/demand').query({ all: true }).loginVia(anotherUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 1)
  assert.equal(response.body.data[0].id, demand.id)
})

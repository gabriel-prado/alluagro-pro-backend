'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const moment = require('moment')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Demand - GET /demand/id/phase')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultMonitor = null
let defaultCompany = null
let defaultFarm = null
let defaultMachine = null
let defaultMachineCategory = null
let defaultMachineType = null
let defaultProvider = null
let defaultPlot = null

beforeEach(async () => {
  await clearDataBase()

  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultMonitor = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  defaultCompany = await Factory.model('App/Models/Company').create()
  defaultFarm = await Factory.model('App/Models/Farm').create({
    company_id: defaultCompany.id
  })

  defaultProvider = await Factory.model('App/Models/Provider').create()
  defaultMachineCategory = await Factory.model('App/Models/MachineCategory').create()
  defaultMachineType = await Factory.model('App/Models/MachineType').create({
    machineCategory_id: defaultMachineCategory.id
  })

  defaultMachine = await Factory.model('App/Models/Machine').create({
    machineCategory_id: defaultMachineCategory.id,
    machineType_id: defaultMachineType.id,
    provider_id: defaultProvider.id
  })
  defaultPlot = await Factory.model('App/Models/Plot').create({
    farm_id: defaultFarm.id
  })
})

test('should retrieve phases weekly with demand start and end date', async({ client, assert}) => {
  let demand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: defaultMonitor.id,
    start_date: '2018-01-06',
    end_date: '2018-03-11'
  })

  const response = await
    client
      .get(`demand/${demand.id}/phase`)
      .query({ type: 'WEEK' })
      .loginVia(defaultUser)
      .end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 11)
  assert.equal(response.body[0].date.start, '2017-12-31')
  assert.equal(response.body[0].label, 'Semana 1')
  assert.equal(response.body[10].date.end, '2018-03-17')
  assert.equal(response.body[10].label, 'Semana 11')
})

test('should retrieve phases weekly with demand start and end date to manager', async({ client, assert}) => {
  let user = await Factory.model('App/Models/User').create({ role: 'MANAGER', company_id: defaultCompany.id })
  let demand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: defaultMonitor.id,
    start_date: '2018-01-06',
    end_date: '2018-03-11'
  })

  const response = await
    client
      .get(`demand/${demand.id}/phase`)
      .query({ type: 'WEEK' })
      .loginVia(user)
      .end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 11)
  assert.equal(response.body[0].date.start, '2017-12-31')
  assert.equal(response.body[0].label, 'Semana 1')
  assert.equal(response.body[10].date.end, '2018-03-17')
  assert.equal(response.body[10].label, 'Semana 11')
})

test('should retrieve phases weekly with reports', async({ client, assert}) => {
  let demand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: defaultMonitor.id,
    start_date: '2018-01-06',
    end_date: '2018-03-11'
  })

  await Factory.model('App/Models/Report').create({
    demand_id: demand.id,
    machine_id: defaultMachine.id,
    plot_id: defaultPlot.id,
    date: '2018-03-19'
  })

  await Factory.model('App/Models/Report').create({
    demand_id: demand.id,
    machine_id: defaultMachine.id,
    plot_id: defaultPlot.id,
    date: '2017-12-29'
  })

  const response = await
    client
      .get(`demand/${demand.id}/phase`)
      .query({ type: 'WEEK' })
      .loginVia(defaultUser)
      .end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 13)
  assert.equal(response.body[0].date.start, '2017-12-24')
  assert.equal(response.body[0].label, 'Semana 1')
  assert.equal(response.body[12].date.end, '2018-03-24')
  assert.equal(response.body[12].label, 'Semana 13')
})

test('should get a bad request and invalid_type error', async({ client, assert}) => {

  let demand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: defaultMonitor.id,
    start_date: '2018-01-06',
    end_date: '2018-03-11'
  })

  const response = await
  client
    .get(`demand/${demand.id}/phase`)
    .query({ type: 'MONTH' })
    .loginVia(defaultUser)
    .end()

  assert.equal(response.status, 400)
  assert.include(response.body, { error: 'INVALID_TYPE' })
})

test('should get a not found to invalid demand id', async({ client, assert}) => {

  const response = await
  client
    .get(`demand/1/phase`)
    .query({ type: 'WEEK' })
    .loginVia(defaultUser)
    .end()

  assert.equal(response.status, 404)
})

test('should get a unauthorized when not logged in', async({ client, assert}) => {

  const response = await
  client
    .get(`demand/1/phase`)
    .query({ type: 'WEEK' })
    .end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.get(`demand/1/phase`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should get a forbidden to incorrect manager', async({ client, assert}) => {
  const managerUser = await Factory.model('App/Models/User').create({
    role: 'MANAGER'
  })
  let demand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: defaultMonitor.id,
    start_date: '2018-01-06',
    end_date: '2018-03-11'
  })

  const response = await
    client
      .get(`demand/${demand.id}/phase`)
      .query({ type: 'WEEK' })
      .loginVia(managerUser)
      .end()

  assert.equal(response.status, 403)
})

test('should retrieve phases to correct manager', async({ client, assert}) => {
  const managerUser = await Factory.model('App/Models/User').create({
    role: 'MANAGER',
    company_id: defaultCompany.id
  })
  let demand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: defaultMonitor.id,
    start_date: '2018-01-06',
    end_date: '2018-03-11'
  })

  const response = await
    client
      .get(`demand/${demand.id}/phase`)
      .query({ type: 'WEEK' })
      .loginVia(managerUser)
      .end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body, 11)
})

'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Demand - GET /demand/id/plot')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultAdminUser = null
let defaultCompany = null
let defaultFarm = null
let defaultPlot = null

beforeEach(async () => {
  await clearDataBase()

  defaultAdminUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })

  defaultCompany = await Factory.model('App/Models/Company').create()

  defaultFarm = await Factory.model('App/Models/Farm').create({
    company_id: defaultCompany.id
  })

  defaultPlot = await Factory.model('App/Models/Plot').create({
    farm_id: defaultFarm.id
  })
})

test('should get plots by demandId and return 200', async ({ client, assert }) => {
  const monitor = await Factory.model('App/Models/User').create({
    role: 'MONITOR'
  })
  const demand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: monitor.id
  })

  const response = await client.get(`demand/${demand.id}/plot`).loginVia(defaultAdminUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 1)
  assert.equal(response.body.total, 1)
  assert.equal(response.body.data[0].id, defaultPlot.id)
})

test('should get plots of demand by monitor and return 200', async ({ client, assert }) => {
  const monitor = await Factory.model('App/Models/User').create({
    role: 'MONITOR'
  })
  const demand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: monitor.id
  })

  const response = await client.get(`demand/${demand.id}/plot`).loginVia(monitor).end()
  
  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 1)
  assert.equal(response.body.total, 1)
  assert.equal(response.body.data[0].id, defaultPlot.id)
})

test('should get a forbidden to incorrect monitor', async ({ client, assert }) => {
  const monitor = await Factory.model('App/Models/User').create({
    role: 'MONITOR'
  })
  const demand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: 1
  })

  const response = await client.get(`demand/${demand.id}/plot`).loginVia(monitor).end()

  assert.equal(response.status, 403)
})

test('should get all plots by demandId and return 200', async ({ client, assert }) => {
  const monitor = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const farm = await Factory.model('App/Models/Farm').create({ company_id: defaultCompany.id })

  await Factory.model('App/Models/Plot').createMany(5, { farm_id: farm.id })

  const demand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: farm.id,
    monitor_id: monitor.id
  })

  const response = await client.get(`demand/${demand.id}/plot`).query({ all: true }).loginVia(defaultAdminUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 5)
})

test('should get plots by demandId and return empty array', async ({ client, assert }) => {
  const response = await client.get(`demand/500/plot`).query({ all: true }).loginVia(defaultAdminUser).end()

  assert.equal(response.status, 404)
})

test('should not show plots for not logged user', async({ client, assert}) => {
  const response = await client.get(`demand/1/plot`).end()

  assert.equal(response.status, 401)
})

test('should not get plots to incorrect manager', async({ client, assert}) => {
  const monitor = await Factory.model('App/Models/User').create({
    role: 'MONITOR'
  })
  const demand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: monitor.id
  })
  const managerUser = await Factory.model('App/Models/User').create({
    role: 'MANAGER'
  })

  const response = await client.get(`demand/${demand.id}/plot`).loginVia(managerUser).end()

  assert.equal(response.status, 403)
})

test('should get plots to correct manager', async({ client, assert}) => {
  await Factory.model('App/Models/Plot').createMany(4, { farm_id: defaultFarm.id })
  const monitor = await Factory.model('App/Models/User').create({
    role: 'MONITOR'
  })
  const demand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: monitor.id
  })
  const managerUser = await Factory.model('App/Models/User').create({
    role: 'MANAGER',
    company_id: defaultCompany.id
  })

  const response = await client.get(`demand/${demand.id}/plot`).query({ all: true }).loginVia(managerUser).end()

  assert.equal(response.status, 200)
  assert.lengthOf(response.body.data, 5)
})

'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const Demand = use('App/Models/Demand')
const User = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('Demand - PUT /demand/id')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultCompany = null
let defaultFarm = null
let defaultMonitor = null
let defaultDemand = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultCompany = await Factory.model('App/Models/Company').create()
  defaultFarm = await Factory.model('App/Models/Farm').create({
    company_id: defaultCompany.id
  })
  defaultMonitor = await Factory.model('App/Models/User').create()
  defaultDemand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: defaultMonitor.id
  })
})

test('should update demand and return 200', async ({ client, assert }) => {
  let demandJson = defaultDemand.toJSON()
  demandJson.name = 'demand_name_test'

  const response = await client.put(`demand/${demandJson.id}`).loginVia(defaultUser).send(demandJson).end()

  assert.equal(response.status, 200)
  const demandInDatabase =  await Demand.findBy('id', response.body.id)
  assert.isNotNull(demandInDatabase)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  assert.equal(demandInDatabase.name, demandJson.name)
})

test('should try update demand and return 400', async ({ client, assert }) => {
  let demandJson = defaultDemand.toJSON()
  demandJson.start_date = 3

  const response = await client.put(`demand/${demandJson.id}`).loginVia(defaultUser).send(demandJson).end()

  assert.equal(response.status, 400)
  assert.equal(response.body[0].field, 'start_date')
  assert.equal(response.body[0].validation, 'dateFormat')
})

test('should update demand with monitor and return 200', async ({ client, assert }) => {
  let demandJson = defaultDemand.toJSON()
  demandJson.name = 'demand_name_test'
  let monitorJson = defaultMonitor.toJSON()
  monitorJson.name = 'user_name_test'
  demandJson.monitor = monitorJson

  const response = await client.put(`demand/${demandJson.id}`).loginVia(defaultUser).send(demandJson).end()

  assert.equal(response.status, 200)
  const demandInDatabase =  await Demand.findBy('id', response.body.id)
  assert.isNotNull(demandInDatabase)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'monitor', 'updated_at'])
  assert.equal(demandInDatabase.name, demandJson.name)
  assert.equal(response.body.monitor.name, monitorJson.name)
})

test('should update monitor without password and return 200', async ({ client, assert }) => {
  let demandJson = defaultDemand.toJSON()
  let monitorJson = defaultMonitor.toJSON()
  delete monitorJson.password
  demandJson.monitor = monitorJson

  const monitorInDatabaseAfter =  await User.findBy('id', monitorJson.id)
  const response = await client.put(`demand/${demandJson.id}`).loginVia(defaultUser).send(demandJson).end()
  const monitorInDatabaseBefore =  await User.findBy('id', monitorJson.id)
  assert.equal(response.status, 200)
  assert.equal(monitorInDatabaseAfter.password, monitorInDatabaseBefore.password)
})

test('should update monitor with password and return 200', async ({ client, assert }) => {
  let demandJson = defaultDemand.toJSON()
  let monitorJson = defaultMonitor.toJSON()
  monitorJson.password = 'new_password'
  demandJson.monitor = monitorJson

  const monitorInDatabaseAfter =  await User.findBy('id', monitorJson.id)
  const response = await client.put(`demand/${demandJson.id}`).loginVia(defaultUser).send(demandJson).end()
  const monitorInDatabaseBefore =  await User.findBy('id', monitorJson.id)

  assert.equal(response.status, 200)
  assert.notEqual(monitorInDatabaseAfter.password, monitorInDatabaseBefore.password)
})

test('should update demand with machines and return 200', async ({ client, assert }) => {
  const machineCategory = await Factory.model('App/Models/MachineCategory').create()
  const machineType = await Factory.model('App/Models/MachineType').create({
    machineCategory_id: machineCategory.id
  })
  const provider = await Factory.model('App/Models/Provider').create()
  const machines = await Factory.model('App/Models/Machine').createMany(2, {
    machineCategory_id: machineCategory.id,
    machineType_id: machineType.id,
    provider_id: provider.id
  })

  let machinesJson = machines.map(m => {
    m.machine_code = 'cod_test'
    return m.toJSON()
  })
  let demandJson = defaultDemand.toJSON()
  demandJson.machines = machinesJson
  demandJson.name = 'demand_name_test'

  const response = await client.put(`demand/${demandJson.id}`).loginVia(defaultUser).send(demandJson).end()

  assert.equal(response.status, 200)
  const demandInDatabase =  await Demand.findBy('id', response.body.id)
  assert.isNotNull(demandInDatabase)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  assert.equal(demandInDatabase.name, demandJson.name)
  assert.lengthOf(response.body.machines, 2)
  assert.containsAllKeys(response.body.machines[0], ['id', 'created_at', 'updated_at', 'pivot'])
})

test('should try update demand and return 401', async ({ client, assert }) => {
  let demandJson = defaultDemand.toJSON()

  const response = await client.put(`demand/${demandJson.id}`).send(demandJson).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.put(`demand/1`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.put(`demand/1`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should update demand removing contracts and return 200', async ({ client, assert }) => {
  let response
  let demandJson = defaultDemand.toJSON()
  demandJson.name = 'demand_name_test'
  demandJson.contracts = [1, 2, 3]

  response = await client.put(`demand/${demandJson.id}`).loginVia(defaultUser).send(demandJson).end()
  assert.deepEqual(response.body.contracts, demandJson.contracts)

  demandJson.contracts = null
  response = await client.put(`demand/${demandJson.id}`).loginVia(defaultUser).send(demandJson).end()
  assert.deepEqual(response.body.contracts, [])

  assert.equal(response.status, 200)
  const demandInDatabase =  await Demand.findBy('id', response.body.id)
  assert.isNotNull(demandInDatabase)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  assert.equal(demandInDatabase.name, demandJson.name)
})

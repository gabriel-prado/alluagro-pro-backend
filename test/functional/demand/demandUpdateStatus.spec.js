'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const Demand = use('App/Models/Demand')
const { test, trait, beforeEach } = use('Test/Suite')('Demand - PUT /demand/id/status')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultCompany = null
let defaultFarm = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultCompany = await Factory.model('App/Models/Company').create()
  defaultFarm = await Factory.model('App/Models/Farm').create({
    company_id: defaultCompany.id
  })
})

test('should update demand status and return 200', async ({ client, assert }) => {
  const demand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    status: 'IN_ANALYSIS'
  })
  let demandJson = demand.toJSON()
  demandJson.status = 'IN_PROGRESS'

  const response = await client.put(`demand/${demand.id}/status`).loginVia(defaultUser).send(demandJson).end()

  assert.equal(response.status, 200)
  const demandInDatabase =  await Demand.findBy('id', response.body.id)
  assert.isNotNull(demandInDatabase)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  assert.equal(demandInDatabase.status, 'IN_PROGRESS')
})

test('should not update demand name and return 200', async ({ client, assert }) => {
  const demand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    name: 'old name'
  })
  let demandJson = demand.toJSON()
  demandJson.name = 'new name'

  const response = await client.put(`demand/${demand.id}/status`).loginVia(defaultUser).send(demandJson).end()

  assert.equal(response.status, 200)
  const demandInDatabase =  await Demand.findBy('id', response.body.id)
  assert.isNotNull(demandInDatabase)
  assert.containsAllKeys(response.body, ['id', 'created_at', 'updated_at'])
  assert.equal(demandInDatabase.name, 'old name')
})

test('should try update demand and return 401', async ({ client, assert }) => {
  const demand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    name: 'old name'
  })

  const response = await client.put(`demand/${demand.id}/status`).send(demand.toJSON()).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.put(`demand/1/status`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.put(`demand/1/status`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

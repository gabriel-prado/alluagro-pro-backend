'use strict'

const clearDataBase = require('../../../utils/clearDataBase')

const Factory = use('Factory')
const Helpers = use('Helpers')
const { test, trait, beforeEach } = use('Test/Suite')('Contract - GET /demand/id/contract')
const { ioc } = use('@adonisjs/fold')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()
})

test('should get contract and return status 200', async({ client, assert }) => {
  ioc.fake('Drive', () => {
    return {
      disk: () => {
        return {
          getObject: () => {
            return {
              ContentType: 'text/plain',
              Body: Buffer.from('Hello world!')
            }
          }
        }
      }
    }
  })

  let company = await Factory.model('App/Models/Company').create()
  let user = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  let farm = await Factory.model('App/Models/Farm').create({
    company_id: company.id
  })
  let demand = await Factory.model('App/Models/Demand').create({
    company_id: company.id,
    farm_id: farm.id
  })

  const response =
    await
      client
      .get('demand/1/contract')
      .query({ contract: 'teste.txt' })
      .loginVia(user)
      .end()

  assert.equal(response.status, 200)
  assert.isObject(response.body)
  assert.equal(response.text, 'Hello world!')

  ioc.restore('Drive')
})

test('analytics controller should return 401 when have no logged user', async({ client, assert }) => {
  const response = await client.get('demand/1/contract').end()
  assert.equal(response.status, 401)
})

test('should try to access the route with monitor role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.get('demand/1/contract').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with manager role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.get('demand/1/contract').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

'use strict'

const clearDataBase = require('../../../utils/clearDataBase')

const Factory = use('Factory')
const Helpers = use('Helpers')
const Drive = use('Drive')
const fs = require('fs')
const { test, trait, beforeEach } = use('Test/Suite')('Contract - POST /demand/id/contract')
const { ioc } = use('@adonisjs/fold')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()
})

test('should save contract and return status 200', async({ client, assert }) => {
  ioc.fake('Drive', () => {
    return {
      disk: () => {
        return {
          put: () => {
            return 'http://aws.com/123'
          }
        }
      }
    }
  })

  let company = await Factory.model('App/Models/Company').create()
  let user = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  let farm = await Factory.model('App/Models/Farm').create({
    company_id: company.id
  })
  let demand = await Factory.model('App/Models/Demand').create({
    company_id: company.id,
    farm_id: farm.id
  })
  await Drive.put('contract.pdf', Buffer.from('Hello world!'))
  let file = await Drive.disk('local').getStream('contract.pdf')

  const response =
    await
      client
      .post('demand/1/contract')
      .attach('contract', file)
      .loginVia(user)
      .end()

  assert.equal(response.status, 200)
  assert.isObject(response.body)
  assert.equal(response.body.url, 'contract.pdf')

  await Drive.disk('local').delete('contract.pdf')
  ioc.restore('Drive')
})

test('should try to save contract from a nonexistent demand and return status 404', async({ client, assert }) => {
  let user = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  await Drive.put('contract.pdf', Buffer.from('Hello world!'))
  let file = await Drive.disk('local').getStream('contract.pdf')

  const response =
    await
      client
      .post('demand/199/contract')
      .attach('contract', file)
      .loginVia(user)
      .end()

  assert.equal(response.status, 404)
  await Drive.disk('local').delete('contract.pdf')
})

test('analytics controller should return 401 when have no logged user', async({ client, assert }) => {
  const response = await client.post('demand/1/contract').end()
  assert.equal(response.status, 401)
})

test('should try to access the route with monitor role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.post('demand/1/contract').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with manager role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.post('demand/1/contract').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

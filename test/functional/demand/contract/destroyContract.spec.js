'use strict'

const clearDataBase = require('../../../utils/clearDataBase')

const Demand = use('App/Models/Demand')
const Factory = use('Factory')
const { test, trait, beforeEach } = use('Test/Suite')('Contract - DELETE /demand/id/contract')
const { ioc } = use('@adonisjs/fold')

trait('Test/ApiClient')
trait('Auth/Client')

beforeEach(async () => {
  await clearDataBase()
})

test('should delete contract and return status 204', async({ client, assert }) => {
  ioc.fake('Drive', () => {
    return {
      disk: () => {
        return {
          delete: () => {
            return true
          }
        }
      }
    }
  })

  let company = await Factory.model('App/Models/Company').create()
  let user = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  let farm = await Factory.model('App/Models/Farm').create({
    company_id: company.id
  })
  let demand = await Factory.model('App/Models/Demand').create({
    company_id: company.id,
    farm_id: farm.id,
    contracts: ["teste.txt"]
  })

  const response =
    await
      client
      .delete('demand/1/contract/0')
      .loginVia(user)
      .end()

  assert.equal(response.status, 204)
  const demandInDatabase =  await Demand.find(demand.id)
  assert.lengthOf(demandInDatabase.toJSON().contracts, 0)

  ioc.restore('Drive')
})

test('should try to delete contract from a nonexistent demand and return status 404', async({ client, assert }) => {
  let user = await Factory.model('App/Models/User').create({ role: 'ADMIN' })

  const response =
    await
      client
      .delete('demand/199/contract/0')
      .loginVia(user)
      .end()

  assert.equal(response.status, 404)
})

test('analytics controller should return 401 when have no logged user', async({ client, assert }) => {
  const response = await client.delete('demand/1/contract/0').end()
  assert.equal(response.status, 401)
})

test('should try to access the route with monitor role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.delete('demand/1/contract/0').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route with manager role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.delete('demand/1/contract/0').loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

'use strict'

const clearDataBase = require('../../utils/clearDataBase')

const Factory = use('Factory')
const Demand = use('App/Models/Demand')
const User = use('App/Models/User')
const { test, trait, beforeEach } = use('Test/Suite')('Demand - DELETE /demand/id')
const { ioc } = use('@adonisjs/fold')

trait('Test/ApiClient')
trait('Auth/Client')

let defaultUser = null
let defaultCompany = null
let defaultFarm = null
let defaultDemand = null

beforeEach(async () => {
  await clearDataBase()
  defaultUser = await Factory.model('App/Models/User').create({ role: 'ADMIN' })
  defaultCompany = await Factory.model('App/Models/Company').create()
  defaultFarm = await Factory.model('App/Models/Farm').create({
    company_id: defaultCompany.id
  })
  defaultDemand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id
  })
})

test('should destroy demand by id', async ({ client, assert }) => {
  ioc.fake('Services/Demand', () => {
    return {
      deleteMessages: () => null,
      deleteContractFolder: () => true
    }
  })

  const response = await client.delete(`demand/${defaultDemand.id}`).loginVia(defaultUser).end()

  assert.equal(response.status, 204)
  const demandInDatabase =  await Demand.find(defaultDemand.id)
  assert.isNull(demandInDatabase)

  ioc.restore('Services/Demand')
})

test('should try to destroy non-existent demand and return status 404', async ({ client, assert }) => {
  const response = await client.delete(`demand/131`).loginVia(defaultUser).end()

  assert.equal(response.status, 404)
  const demandInDatabase =  await Demand.find(defaultDemand.id)
  assert.isNotNull(demandInDatabase)
})

test('should try to destroy demand with reports and return status 500', async ({ client, assert }) => {
  let provider = await Factory.model('App/Models/Provider').create()
  let machineCategory = await Factory.model('App/Models/MachineCategory').create()
  let machineType = await Factory.model('App/Models/MachineType').create({
    machineCategory_id: machineCategory.id
  })
  let machine = await Factory.model('App/Models/Machine').create({
    machineCategory_id: machineCategory.id,
    machineType_id: machineType.id,
    provider_id: provider.id
  })
  let plot = await Factory.model('App/Models/Plot').create({
    farm_id: defaultFarm.id
  })
  await Factory.model('App/Models/Report').createMany(10, {
    machine_id: machine.id,
    plot_id: plot.id,
    demand_id: defaultDemand.id
  })

  const response = await client.delete(`demand/${defaultDemand.id}`).loginVia(defaultUser).end()

  assert.equal(response.status, 500)
  assert.equal(response.body.code, 'ER_ROW_IS_REFERENCED')
  assert.equal(response.body.table, 'REPORTS')
  const demandInDatabase =  await Demand.find(defaultDemand.id)
  assert.isNotNull(demandInDatabase)
})

test('should try destroy demand without authenticate and retrieve 401', async ({ client, assert }) => {
  const response = await client.delete(`demand/${defaultDemand.id}`).end()

  assert.equal(response.status, 401)
})

test('should destroy demand with monitor and return status 204', async ({ client, assert }) => {
  let monitor = await Factory.model('App/Models/User').create({ role: 'MONITOR'})
  let demand = await Factory.model('App/Models/Demand').create({
    company_id: defaultCompany.id,
    farm_id: defaultFarm.id,
    monitor_id: monitor.id
  })

  ioc.fake('Services/Demand', () => {
    return {
      deleteMessages: () => null,
      deleteContractFolder: () => true
    }
  })

  const response = await client.delete(`demand/${demand.id}`).loginVia(defaultUser).end()

  assert.equal(response.status, 204)
  const demandInDatabase =  await Demand.find(demand.id)
  assert.isNull(demandInDatabase)
  const monitorInDatabase =  await User.find(monitor.id)
  assert.isNull(monitorInDatabase)

  ioc.restore('Services/Demand')
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MONITOR' })
  const response = await client.delete(`demand/${defaultDemand.id}`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

test('should try to access the route without the required role and retrieve 401', async ({ client, assert }) => {
  const anotherUser = await Factory.model('App/Models/User').create({ role: 'MANAGER' })
  const response = await client.delete(`demand/${defaultDemand.id}`).loginVia(anotherUser).end()

  assert.equal(response.status, 401)
})

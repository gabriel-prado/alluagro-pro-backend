'use strict'

const clearDataBase = require('./test/utils/clearDataBase')

/*
|--------------------------------------------------------------------------
| Vow file
|--------------------------------------------------------------------------
|
| The vow file is loaded before running your tests. This is the best place
| to hook operations `before` and `after` running the tests.
|
*/
const Database = use('Database')
// Uncomment when want to run migrations
const ace = require('@adonisjs/ace')

module.exports = (cli, runner) => {
  runner.timeout(15000)

  runner.before(async () => {
    use('Adonis/Src/Server').listen(process.env.HOST, process.env.PORT)

    await ace.call('migration:run', null, { silent: true })
  })

  runner.after(async () => {
    use('Adonis/Src/Server').getInstance().close()

    await clearDataBase()
    await ace.call('migration:reset', null, { silent: true })
  })
}
